var $I = {};
var COMPANY_LOGO = `https://dummyimage.com/600x400/000/fff.png&text=HELLO%20WORLD`;
var FULL_NAME = "System";
function getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
        var img = new Image();
        img.setAttribute("crossOrigin", "anonymous");

        img.onload = () => {
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;

            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);

            var dataURL = canvas.toDataURL("image/png");

            resolve(dataURL);
        };

        img.onerror = error => {
            reject(error);
        };

        img.src = url;
    });
}

Date.prototype.toDateInputValue = function () {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
};

let API_KEY =
    "PMAK-62afc68867477909483db628-fe818711e4da3d6d66b9d6c12b3f836ee4";
let csrf_token = $('meta[name="csrf-token"]').attr("content") || '';
$I = {
    ajax: function (type, url, data = {}, success, error, withAsync = false) {
        let messageError = error;
        if (typeof error === 'undefined') {
            messageError = function (err) {
                console.log("ERR: ", err);
                Swal.fire({
                    title: 'Whoops!',
                    text: 'Something went wrong!',
                    icon: 'error'
                })
            }
        }
        if (withAsync) {
            return new Promise((resolve, reject) => {
                $.ajax({
                    type: type,
                    url: url,
                    headers: {
                        'x-api-key': API_KEY,
                        'X-CSRF-TOKEN': csrf_token,
                    },
                    data: data,
                    dataType: "JSON",
                    success: function (result) {
                        resolve(result)
                    },
                    error: messageError
                })
            })
        } else {
            var ajax = $.ajax({
                type: type,
                url: url,
                headers: {
                    'x-api-key': API_KEY,
                    'X-CSRF-TOKEN': csrf_token,
                },
                data: data,
                dataType: "JSON",
                success: success,
                error: messageError
            })
            return ajax;
        }
    },
    showTableDataStatic: function (
        data,
        listColoums,
        targetElement,
        options = {}
    ) {
        var jsonOption = {
            language: {
                search: "",
                searchPlaceholder: "Search Here"
            },
            data: data,
            searching: true,
            lengthChange: false,
            ordering: false,
            order: [],
            columnDefs: [{ width: "5px", targets: [0] }],
            columns: listColoums,
            responsive: true,
            initComplete: function (settings, json) {
                targetElement.wrap(
                    "<div style='overflow:auto; width:100% !important;position:relative;'></div>"
                );
            }
        };

        Object.entries(options).forEach(item => {
            jsonOption[item[0]] = item[1];
        });

        var oTable = targetElement.DataTable(jsonOption);
        return oTable;
    },
    showTable: function (
        url,
        type = "GET",
        params,
        listColoums,
        targetElement,
        options = {}
    ) {
        // console.log("target SHOW TABLE", {
        //     url,
        //     params
        // });
        var jsonOption = {
            processing:
                '<i class="fa fa-spinner fa-spin" style="font-size:24px;color:rgb(75, 183, 245);"></i>',
            language: {
                search: "",
                searchPlaceholder: "Search Here"
            },
            searching: false,
            lengthChange: false,
            ordering: false,
            searchDelay: 500,
            serverSide: true,
            order: [],
            columnDefs: [{ width: "5px", targets: [0] }],
            ajax: {
                url: url,
                headers: {
                    "x-api-key": API_KEY,
                    "X-CSRF-TOKEN": csrf_token
                },
                type: type,
                data: function (data) {
                    return {
                        ...data,
                        ...params,
                        page: (parseInt(data.start / data.length) + 1),
                    }
                },
                beforeSend: function () {
                    $(".dataTables_processing").css("display", "block");
                    $(".dataTables_processing").css("z-index", "1000");
                },
                dataSrc: function (json) {
                    var { data } = json;
                    json.recordsTotal = data.total;
                    json.recordsFiltered = data.total;
                    return data.data;
                }
            },
            columns: listColoums,
            responsive: true,
            initComplete: function (settings, json) {
                targetElement.wrap(
                    "<div style='overflow:auto; width:100% !important;position:relative;'></div>"
                );
            }
        };

        Object.entries(options).forEach(item => {
            jsonOption[item[0]] = item[1];
        });

        var oTable = targetElement.DataTable(jsonOption);
        return oTable;
    },
    generateSelectOption: function (targetElement, type, url, data = {}, value = 'id', text = 'name', withAsync = true, addtionalAttr = []) {
        if (withAsync) {
            return new Promise((resolve, reject) => {
                $I.ajax(type, url, data, function (result) {
                    let { data } = result;
                    data.map((item) => {
                        let showText = "";
                        let addAttr = "";

                        if (typeof text === 'object') {
                            let convertObjectToArray = Object.entries(text);
                            convertObjectToArray.map((txt, index) => {
                                showText += `${txt[0]}: ${item[txt[1]]} ${(convertObjectToArray.length !== (index + 1)) ? " & " : ""}`
                            });
                        } else {
                            showText = item[text];
                        }

                        if (addtionalAttr.length > 0) {
                            addAttr = addtionalAttr.map((attr) => {
                                return `${attr}='${item[attr]}'`;
                            }).join(' ');
                        }

                        targetElement.append(`
                            <option value='${item[value]}' ${addAttr}>${showText}</option>
                        `)
                    });
                    resolve(true);
                })
            })
        } else {
            $I.ajax(type, url, data, function (result) {
                let { data } = result;
                data.map(item => {
                    let showText = "";
                    let addAttr = "";
                    if (typeof text === 'object') {
                        let convertObjectToArray = Object.entries(text);
                        convertObjectToArray.map((txt, index) => {
                            showText += `${txt[0]}: ${item[txt[1]]} ${(convertObjectToArray.length !== (index + 1)) ? " & " : ""}`
                        });
                    } else {
                        showText = item[text];
                    }

                    if (addtionalAttr.length > 0) {
                        addAttr = addtionalAttr.map((attr) => {
                            return `${attr}='${item[attr]}'`;
                        }).join(' ');
                    }

                    targetElement.append(`
                        <option value='${item[value]}' ${addAttr}>${showText}</option>
                    `);
                });
            })
        }
    },
    generateSelectOptionStaticData: function (targetElement, data = [], value = 'id', text = 'name', addtionalAttr = []) {
        data.map(item => {
            let showText = "";
            let addAttr = "";
            if (typeof text === 'object') {
                let convertObjectToArray = Object.entries(text);
                convertObjectToArray.map((txt, index) => {
                    showText += `${txt[0]}: ${item[txt[1]]} ${(convertObjectToArray.length !== (index + 1)) ? " & " : ""}`
                });
            } else {
                showText = item[text];
            }

            if (addtionalAttr.length > 0) {
                addAttr = addtionalAttr.map((attr) => {
                    return `${attr}='${item[attr]}'`;
                }).join(' ');
            }

            targetElement.append(`
                <option value='${item[value]}' ${addAttr}>${showText}</option>
            `);
        });
    },
    generateButtonExport: function (
        title,
        typeExport = ["pdf, excel"],
        targetElement
    ) {
        const buttons = {
            pdf: {
                extend: "pdfHtml5",
                text: "PDF Export",
                title: title,
                exportOptions: {
                    columns: ":not(:last-child)"
                },
                orientation: "landscape",
                customize: function (doc) {
                    var colCount = new Array();
                    $(targetElement)
                        .find("tbody tr:first-child td")
                        .each(function () {
                            if ($(this).attr("colspan")) {
                                for (
                                    var i = 1;
                                    i <= $(this).attr("colspan");
                                    $i++
                                ) {
                                    colCount.push("*");
                                }
                            } else {
                                colCount.push("*");
                            }
                        });
                    doc.content[1].table.widths = colCount;
                    doc["header"] = async function () {
                        return {
                            columns: [
                                {
                                    image: await getBase64ImageFromURL(COMPANY_LOGO),
                                    width: 84
                                }
                            ],
                            margin: 20
                        };
                    };
                    doc["footer"] = function (page, pages) {
                        return {
                            columns: [
                                {
                                    text: [
                                        {
                                            text: new Date().toISOString()
                                        }
                                    ]
                                },
                                {
                                    text: [
                                        {
                                            text: $("#page_no").text()
                                        }
                                    ]
                                },
                                {
                                    text: [
                                        "Printed by: ",
                                        {
                                            text: FULL_NAME
                                        }
                                    ]
                                },
                                {
                                    text: [
                                        {
                                            text: $("#page_no").text()
                                        }
                                    ]
                                },
                                {
                                    text: [
                                        "page ",
                                        {
                                            text: page.toString()
                                        },
                                        " of ",
                                        {
                                            text: pages.toString()
                                        }
                                    ]
                                }
                            ],
                            margin: 20
                        };
                    };
                }
            },
            excel: {
                extend: "excel",
                text: "Excel Export",
                title: title,
                exportOptions: {
                    columns: ":not(:last-child)"
                }
            }
        };

        return typeExport.map(type => buttons[type]);
    },
    generateFilter: function (
        label,
        components,
        targetElement,
        options = {
            url: null,
            type: "GET",
            params: {}
        },
        callback
    ) {
        return new Promise((resolve, reject) => {
            try {
                if (typeof components === "function") {
                    if (options.url && options.type && options.params) {
                        $I.ajax(
                            options.type,
                            options.url,
                            options.params,
                            function (result) {
                                targetElement.append(
                                    `<b>${label}: ${components(result)}</b>`
                                );
                                typeof callback === "function" && callback();

                                resolve(true);
                            }
                        );
                    } else {
                        targetElement.prepend(
                            `<b>${label}: ${components()}</b>`
                        );
                        typeof callback === "function" && callback();

                        resolve(true);
                    }
                } else {
                    reject("COMPONENT IS NOT A FUNCTION");
                }
            } catch (error) {
                reject(error);
            }
        });
    }
};
