<?php

function dd($data = [])
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    die;
}

function dump($data = [])
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
}

function set_value($value, $default = ""){
    if($value){
        return $value;
    }else{
        return $default;
    }
}


function route($controller, $action, $queryString=NULL){		
    if($queryString !== NULL){
        $queryString = "&$queryString";
    }

    $url = "index.php?controller=$controller&action=$action".$queryString;

    return $url;
}

function filter_alphanum($string) {
    $characters = str_split($string);
    $alphaNumeric = array_filter($characters,"ctype_alnum");
    return join($alphaNumeric);
}
