<?php
// file: /core/PDOConnection.php

class PDOConnection
{
	// Production
	// private static $dbhost = "103.147.154.182";
	// private static $dbname = "eventor4_event_organizer";
	// private static $dbuser = "eventor4_production";
	// private static $dbpass = "F4M.UICK^1*i";

	// Local
	private static $dbhost = "sql12.freemysqlhosting.net";
	private static $dbname = "sql12611953";
	private static $dbuser = "sql12611953";
	private static $dbpass = "vF37hLZzpG";

	private static $db_singleton = null;

	public static function getInstance()
	{
		if (self::$db_singleton == null) {
			self::$db_singleton = new PDO(
				"mysql:host=" . self::$dbhost . ";dbname=" . self::$dbname . ";charset=utf8", // connection string
				self::$dbuser,
				self::$dbpass,
				array(
					// options
					PDO::ATTR_EMULATE_PREPARES => false,
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
				)
			);
		}
		return self::$db_singleton;
	}
}
