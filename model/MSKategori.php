<?php
// file: model/Comment.php

require_once(__DIR__."/../core/ValidationException.php");

/**
* Class Comment
*
* Represents a Comment in the blog. A Comment is attached
* to a Post and was written by an specific User (author)
*
* @author lipido <lipido@gmail.com>
*/
class MSKategori {
	private $id;
	
	public function __construct($id=NULL, $content=NULL, User $author=NULL, Post $post=NULL) {
		$this->id = $id;
		$this->content = $content;
		$this->author = $author;
		$this->post = $post;
	}

	public function getId(){
		return $this->id;
	}
}
