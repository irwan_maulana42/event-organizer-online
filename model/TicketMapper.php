<?php

require_once(__DIR__ . "/../core/PDOConnection.php");
require_once(__DIR__ . "/../model/Member.php");

class TicketMapper
{
    private $db;

    public function __construct()
    {
        $this->db = PDOConnection::getInstance();
    }

    public function findAll()
    {
        // $stmt = $this->db->query("SELECT * FROM posts, users WHERE users.username = posts.author");
        // $posts_db = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // $posts = array();

        // foreach ($posts_db as $post) {
        //     $author = new User($post["username"]);
        //     array_push($posts, new Post($post["id"], $post["title"], $post["content"], $author));
        // }

        // return $posts;
    }

    public function getTicketByEmail($email)
    {
        $sql = "SELECT t2.nama_event, t2.nama_tiket, t1.data_pemesan AS email,
                t3.kode_tiket, t2.date, t2.time, t2.lokasi, t2.id as event_id
                FROM transaction_detail t1
                INNER JOIN events t2 ON t1.event_id = t2.id
                INNER JOIN ticket t3 ON t3.transaction_detail_id = t1.id
                WHERE t1.data_pemesan LIKE '%a%'";
                // WHERE json_unquote(json_extract(t1.data_pemesan, '$.email')) = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($result)){
            $result = array_map(function ($row) {
                $data = $row;
                $data['email'] = @json_decode($row['email'], true)['email'];
                return $data;
            }, $result);
        }

        return $result;
    }

    public function getTicketByKode($kode, $event_id)
    {
        $sql = "SELECT t2.nama_event, t2.nama_tiket, t1.data_pemesan,
                t3.kode_tiket, t2.date, t2.time, t2.lokasi, t2.id as event_id
                FROM transaction_detail t1
                INNER JOIN events t2 ON t1.event_id = t2.id
                INNER JOIN ticket t3 ON t3.transaction_detail_id = t1.id
                WHERE t3.kode_tiket = ? AND t3.event_id = ?";
        $stmt = $this->db->prepare($sql);
        $stmt->execute([$kode, $event_id]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function getTiketTerjual($userId = "", $params = [])
    {
        $where = "";
        if($userId !== "" || !empty($params)){
            $where = "WHERE ";
        }

        if($userId !== ""){
            $where .= " t1.user_id = '". $userId ."'";
        }

        if(isset($params['start_date']) && isset($params['end_date'])){
            if($userId !== "") {
                $where .= " AND ";
            }
            $where .= " DATE(t2.created_at) BETWEEN '". $params['start_date'] ."' AND '".$params['end_date']."'";
        }

        if(!empty($params['organizer_id'])){
            $where .= "AND t3.id = '". $params['organizer_id'] ."'";
        }

        if(!empty($params['kategori_id'])){
            $where .= "AND t1.kategori_id = '". $params['kategori_id'] ."'";
        }

        $sql = "SELECT t1.id as event_id, t1.*, t2.id as ticket_id, t2.*
        FROM events t1
        INNER JOIN ticket t2 ON t2.event_id = t1.id
        INNER JOIN `profile` t3 ON t1.user_id = t3.user_id
        $where";

        $stmt = $this->db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getTotalTransaksi($userId = "", $params = [])
    {
        $where = "";
        if($userId !== ""){
            $where = "AND t3.user_id = '". $userId ."'";
        }

        if(isset($params['start_date']) && isset($params['end_date'])){
            $where .= " AND DATE(t1.created_at) BETWEEN '". $params['start_date'] ."' AND '".$params['end_date']."'";
        }

        if(!empty($params['organizer_id'])){
            $where .= "AND t4.id = '". $params['organizer_id'] ."'";
        }

        if(!empty($params['kategori_id'])){
            $where .= "AND t3.kategori_id = '". $params['kategori_id'] ."'";
        }

        // $sql = "SELECT SUM(t1.total + t1.fee) as grand_total
        // FROM transactions t1
        // INNER JOIN transaction_detail t2 ON t1.id = t2.transaction_id
        // INNER JOIN events t3 ON t3.id = t2.event_id
        // WHERE t1.status = 1 $where GROUP BY t1.id";
        $sql = "SELECT 
        (t1.total+ t1.fee) as grand_total
        FROM transactions t1 INNER JOIN transaction_detail t2 ON t1.id = t2.transaction_id 
        INNER JOIN events t3 ON t3.id = t2.event_id 
        INNER JOIN `profile` t4 ON t4.user_id = t3.user_id 
        WHERE t1.status = 1 
        $where
        GROUP BY t1.id";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($result)){
            $result['grand_total'] = array_sum(array_column($result, 'grand_total'));
        }
        return $result;
    }

    public function getTotalTransaksiByEvent($request)
    {
        $sql = "SELECT t1.transaction_id, t2.status
            FROM transaction_detail t1 
            INNER JOIN transactions t2 ON t2.id = t1.transaction_id
            WHERE t1.event_id = '".$request['event_id']."'
            AND t2.status = 1
            GROUP BY t1.transaction_id
            ";

        $stmt = $this->db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(!empty($result)){
            $getTransactionId = implode(',', array_column($result, 'transaction_id'));
            $stmt = $this->db->query("SELECT SUM(total + fee) as total_transaksi FROM transactions WHERE id IN($getTransactionId)");
            $result2 = $stmt->fetch(PDO::FETCH_ASSOC);
            if($result2['total_transaksi'] !== NULL){
                return $result2['total_transaksi'];
            }else{
                return 0;
            }
        }

        return 0;
    }

    public function getStatistikTiketTerjual($userId = "", $params = [])
    {
        $where = "";
        if($userId !== "" || !empty($params)){
            $where = "WHERE ";
        }

        if($userId !== ""){
            $where .= " t1.user_id = '". $userId ."'";
        }

        if(isset($params['start_date']) && isset($params['end_date'])){
            if($userId !== "") {
                $where .= " AND ";
            }
            $where .= " DATE(t1.created_at) BETWEEN '". $params['start_date'] ."' AND '".$params['end_date']."'";
        }

        if(!empty($params['organizer_id'])){
            $where .= "AND t2.id = '". $params['organizer_id'] ."'";
        }

        if(!empty($params['kategori_id'])){
            $where .= "AND t1.kategori_id = '". $params['kategori_id'] ."'";
        }

        $sql = "SELECT sum(t1.sisa_tiket) as sisa_tiket, 
                (sum(t1.jumlah_tiket) - sum(t1.sisa_tiket)) as terjual 
                FROM events t1
                INNER JOIN `profile` t2 ON t1.user_id = t2.user_id
        $where";

        $stmt = $this->db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}
