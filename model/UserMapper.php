<?php
// file: model/UserMapper.php

require_once(__DIR__ . "/../core/PDOConnection.php");

/**
 * Class UserMapper
 *
 * Database interface for User entities
 *
 * @author lipido <lipido@gmail.com>
 */
class UserMapper
{
	private $db;

	public function __construct()
	{
		$this->db = PDOConnection::getInstance();
	}

	public function save($user)
	{
		$stmt = $this->db->prepare("INSERT INTO `users` (`email`, `password`, `created_at`, `updated_at`) values (?,?, now(), now())");
		$stmt->execute([
			$user->getEmail(),
			password_hash($user->getPasswd(), PASSWORD_BCRYPT)
		]);
	}

	public function saveProfile($data)
	{
		try {
			$check = $this->checkProfile($data['user_id']);
			if ((int)$check['count'] < 1) {
				$query = "INSERT INTO `profile` (`user_id`, `banner_image`, `profile_image`, `nama_organizer`, `no_handphone`, `alamat`, `tentang_kami`, `tags`, `created_at`, `updated_at`)
						VALUES (?, NULL, ?, ?, ?, ?, ?, NULL, now(), now())";
				$stmt = $this->db->prepare($query);
				$stmt->execute([
					$data['user_id'],
					@json_encode($data['profile_image']),
					$data['nama_organizer'],
					$data['no_handphone'],
					@$data['alamat'],
					@$data['tentang_kami']
				]);
			} else {
				$this->updateProfile($data);
			}
			return true;
		} catch (Exception $e) {
			error_log($e->getMessage());
			return false;
		}
	}

	public function getProfile($user_id)
	{
		$query = "SELECT * FROM `profile` WHERE `user_id` = ? ";
		$stmt = $this->db->prepare($query);
		$stmt->execute([$user_id]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function checkProfile($user_id)
	{
		$query = "SELECT count(*) as `count` FROM `profile` WHERE `user_id` = ? ";
		$stmt = $this->db->prepare($query);
		$stmt->execute([
			$user_id
		]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function updateProfile($data)
	{
		if (isset($data['profile_image'])) {
			$this->onlyUpdateProfileImage($data, $data['user_id']);
		}

		$query = "UPDATE `profile` 
						SET 
							`banner_image` = NULL,
							`nama_organizer` = ?, 
							`no_handphone` = ?, 
							`alamat` = ?, 
							`tentang_kami` = ?, 
							`updated_at` = now()
						WHERE `user_id` = ?";
		$stmt = $this->db->prepare($query);
		$stmt->execute([
			$data['nama_organizer'],
			$data['no_handphone'],
			@$data['alamat'],
			@$data['tentang_kami'],
			$data['user_id']
		]);

		return true;
	}

	public function onlyUpdateProfileImage($data, $user_id)
	{
		$query = "UPDATE `profile` 
						SET 
							`profile_image` = ?,
							`updated_at` = now()
						WHERE `user_id` = ?";
		$stmt = $this->db->prepare($query);
		$stmt->execute([
			json_encode($data['profile_image']),
			$user_id
		]);
		return true;
	}

	public function usernameExists($username)
	{
		$stmt = $this->db->prepare("SELECT count(username) FROM users where username=?");
		$stmt->execute(array($username));

		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function emailExists($email)
	{
		// print_r($email);
		$stmt = $this->db->prepare("SELECT count(email) as `count` FROM users where email=?");
		$stmt->execute(array($email));

		if ($stmt->fetchColumn() > 0) {
			return true;
		}
	}

	public function isValidUser($email, $passwd)
	{
		$stmt = $this->db->prepare("SELECT * FROM users where email = ?");
		$stmt->execute(array($email));
		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		if (!!$data && password_verify($passwd, $data['password'])) {
			return true;
		}else{
			return false;
		}
	}

	public function getDetailuser($email)
	{
		$stmt = $this->db->prepare("
			SELECT t1.*, t2.banner_image, t2.profile_image, t2.nama_organizer, t2.no_handphone, t2.alamat, t2.tentang_kami, t2.tags
			FROM users t1
			LEFT JOIN profile t2 ON t2. user_id = t1.id
			WHERE t1.email = ?
		");
		$stmt->execute([$email]);
		$data = $stmt->fetch(PDO::FETCH_ASSOC);

		if (!!$data) {
			return $data;
		} else {
			return false;
		}
	}

	public function changePasswordUser($new_password, $email)
	{
		$query = "UPDATE `users` 
						SET 
							`password` = ?,
							`updated_at` = now()
						WHERE `email` = ?";
		$stmt = $this->db->prepare($query);
		$stmt->execute([
			password_hash($new_password, PASSWORD_BCRYPT),
			$email
		]);
		return true;
	}

	public function getAllDataOrganizer()
	{
		$stmt = $this->db->prepare("
			SELECT * FROM profile
		");
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);

		if (!!$data) {
			return $data;
		} else {
			return [];
		}
	}
}
