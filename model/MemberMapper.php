<?php

require_once(__DIR__ . "/../core/PDOConnection.php");
require_once(__DIR__ . "/../model/Member.php");

class MemberMapper
{
    private $db;

    public function __construct()
    {
        $this->db = PDOConnection::getInstance();
    }

    public function getAllProfileOrganizer()
    {
        $stmt = $this->db->query("
            SELECT t1.* FROM `profile` as t1
            INNER JOIN `users` as t2 ON t1.user_id=t2.id
            WHERE t2.is_superadmin = '0'
        ");

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function getAllEventsByUserId($request)
    {
        $stmt = $this->db->query("
            SELECT t1.*,
            t2.nama as nama_kategori
            FROM `events` as t1
            INNER JOIN `ms_kategori` as t2 ON t1.kategori_id=t2.id
            WHERE t1.user_id = '" . $request['user_id'] . "'
        ");

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    public function findAll()
    {
        // $stmt = $this->db->query("SELECT * FROM posts, users WHERE users.username = posts.author");
        // $posts_db = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // $posts = array();

        // foreach ($posts_db as $post) {
        //     $author = new User($post["username"]);
        //     array_push($posts, new Post($post["id"], $post["title"], $post["content"], $author));
        // }

        // return $posts;
    }

    public function getTrxByEvent($request)
    {
        $sql = "SELECT t1.transaction_id, t2.status
        FROM transaction_detail t1 
        INNER JOIN transactions t2 ON t2.id = t1.transaction_id
        WHERE t1.event_id = '" . $request['event_id'] . "'
        GROUP BY t1.transaction_id
        ";

        $stmt = $this->db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($result)) {
            $getTransactionId = implode(',', array_column($result, 'transaction_id'));
            $stmt = $this->db->query("SELECT *, (total + fee) as grand_total FROM `transactions` WHERE `id` IN($getTransactionId)");
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        }

        return [];
    }
}
