<?php
require_once(__DIR__."/../core/PDOConnection.php");

require_once(__DIR__."/../model/MSKategori.php");

class MSKategoriMapper {
	private $db;
	protected $table = "ms_kategori";

	public function __construct() {
		$this->db = PDOConnection::getInstance();
	}

	public function findAll()
	{
		$stmt = $this->db->prepare("SELECT * FROM $this->table ORDER BY id ASC");
		$stmt->execute();
		$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $data;
	}

	public function save(Comment $comment) {
		$stmt = $this->db->prepare("INSERT INTO comments(content, author, post) values (?,?,?)");
		$stmt->execute(array($comment->getContent(), $comment->getAuthor()->getUsername(), $comment->getPost()->getId()));
		return $this->db->lastInsertId();
	}
}
