<?php
require_once(__DIR__ . "/../core/PDOConnection.php");
require_once(__DIR__ . "/../core/Mailer.php");

// require_once(__DIR__."/../model/Comment.php");

class EventMapper
{
	private $db, $email;

	public function __construct()
	{
		$this->db = PDOConnection::getInstance();
		$this->email = Mailer::getInstance();
	}

	public function sendEmail($title, $to = "irwan.maullana248@gmail.com", $subject, $body, $alt = NULL)
	{
		try {
			$this->email->setFrom('irwan.maulana0895@gmail.com', $title);
			$this->email->addAddress($to);
			$this->email->addCC('irwan9764@gmail.com');
			//Content
			$this->email->isHTML(true); //Set email format to HTML
			$this->email->Subject = $subject;
			$this->email->Body    = $body;
			if ($alt !== NULL) {
				$this->email->AltBody = $alt;
			}

			$this->email->send();
			return true;
		} catch (Exception $e) {
			dd($e->getMessage());
		}
	}

	public function postEvent($data = [])
	{
		try {
			$prep = array();
			foreach ($data as $k => $v) {
				$prep[':' . $k] = $v;
			}

			$columns = implode(", ", array_keys($data));
			$values = implode(',', array_keys($prep));

			$sql = "INSERT INTO `events` ($columns) VALUES ($values)";

			$stmt = $this->db->prepare($sql);
			$stmt->execute($prep);
			return true;
		} catch (Exception $e) {
			error_log($e->getMessage());
			return false;
		}
	}

	public function insertData($table, $data)
	{
		try {
			$prep = array();
			foreach ($data as $k => $v) {
				$prep[':' . $k] = $v;
			}

			$columns = implode(", ", array_keys($data));
			$values = implode(',', array_keys($prep));

			$sql = "INSERT INTO `$table` ($columns) VALUES ($values)";

			$stmt = $this->db->prepare($sql);
			$stmt->execute($prep);
			return true;
		} catch (Exception $e) {
			error_log($e->getMessage());
			dd($e->getMessage());
			return false;
		}
	}

	public function getTempTransaction($token)
	{
		$sql = "SELECT * FROM `temp_transaction` WHERE token = ?";
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$token]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function updateTempTransaction($order_id, $token, $data)
	{
		try {
			$sql = "UPDATE 
						`temp_transaction` 
							SET 
								`data` = ?,
								`created_at` = now(),
								`updated_at` = now()
							WHERE 
								`token` = ?";
			$stmt = $this->db->prepare($sql);
			$stmt->execute([$data, $token]);
			return true;
		} catch (Exception $e) {
			error_log($e->getMessage());
			dd($e->getMessage());
			return false;
		}
	}

	public function postTempTransaction($data = [])
	{
		try {
			$prep = array();
			foreach ($data as $k => $v) {
				$prep[':' . $k] = $v;
			}

			$columns = implode(", ", array_keys($data));
			$values = implode(',', array_keys($prep));

			$sql = "INSERT INTO `events` ($columns) VALUES ($values)";

			$stmt = $this->db->prepare($sql);
			$stmt->execute($prep);
			return true;
		} catch (Exception $e) {
			error_log($e->getMessage());
			return false;
		}
	}

	public function getDataEventByUserId($user_id = '', $status = 'aktif', $params = [])
	{
		$where = "CONCAT(`date`, ' ', `time`) >= NOW()";
		if ($status !== 'aktif') $where = "CONCAT(`date`, ' ', `time`) <= NOW()";

		if($user_id !== ""){
			$where.= " AND `t1`.`user_id` = '". $user_id ."'";
		}

		if(isset($params['start_date']) && isset($params['end_date'])){
            $where .= " AND DATE(t1.created_at) BETWEEN '". $params['start_date'] ."' AND '".$params['end_date']."'";
        }

		if(!empty($params['organizer_id'])){
            $where .= " AND t2.id = '". $params['organizer_id'] ."'";
        }

		if(!empty($params['kategori_id'])){
            $where .= " AND t1.kategori_id = '". $params['kategori_id'] ."'";
        }

		$sql = "SELECT * 
		FROM `events` as `t1`
		INNER JOIN `profile` t2 ON t1.user_id = t2.user_id
		WHERE $where";
		
        
		$stmt = $this->db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getDataEvent($status = 'aktif')
	{
		$where = "CONCAT(`t1`.`date`, ' ', `t1`.`time`) >= NOW() 
				  AND t1.sisa_tiket > 0 ";
		if ($status !== 'aktif') $where = "CONCAT(`t1`.`date`, ' ', `t1`.`time`) <= NOW()";

		$stmt = $this->db->prepare("
			SELECT `t1`.*, IF(t2.nama_organizer IS NULL, t1.penyelenggara, t2.nama_organizer) as nama_organize, 
				   `t2`.`banner_image`,`t2`.`profile_image`,`t2`.`nama_organizer`, `t2`.`tentang_kami`
			FROM `events` as `t1`
			LEFT JOIN `profile` as `t2` ON `t2`.`user_id` = `t1`.`user_id`
			WHERE $where
			ORDER BY (t1.jumlah_tiket - t1.sisa_tiket) DESC
		");

		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getDataEventFeatured($status = 'aktif')
	{
		$where = "CONCAT(`t1`.`date`, ' ', `t1`.`time`) >= NOW()";
		if ($status !== 'aktif') $where = "CONCAT(`t1`.`date`, ' ', `t1`.`time`) <= NOW()";

		$stmt = $this->db->prepare("
			SELECT `t1`.*, IF(t2.nama_organizer IS NULL, t1.penyelenggara, t2.nama_organizer) as nama_organize, `t2`.`banner_image`,`t2`.`profile_image`,`t2`.`nama_organizer`, `t2`.`tentang_kami`
			FROM `events` as `t1`
			LEFT JOIN `profile` as `t2` ON `t2`.`user_id` = `t1`.`user_id`
			WHERE $where
		");

		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getDataEventById($url, $status = 'aktif')
	{
		$where = "CONCAT(`t1`.`date`, ' ', `t1`.`time`) >= NOW() AND `t1`.`url` = ?";
		if ($status !== 'aktif') $where = "CONCAT(`t1`.`date`, ' ', `t1`.`time`) <= NOW() AND `t1`.`url` = ?";

		$stmt = $this->db->prepare("
			SELECT `t1`.*, IF(t2.nama_organizer IS NULL, t1.penyelenggara, t2.nama_organizer) as nama_organize, 
				   `t3`.`nama` as nama_kategori,`t2`.`banner_image`,`t2`.`profile_image`,`t2`.`nama_organizer`, 
				   `t2`.`tentang_kami`
			FROM `events` as `t1`
			LEFT JOIN `profile` as `t2` ON `t2`.`user_id` = `t1`.`user_id`
			INNER JOIN `ms_kategori` as `t3` ON `t3`.`id` = `t1`.`kategori_id`

			WHERE $where
		");

		$stmt->execute([$url]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getDataTransactionByInvoice($invoice, $status = 1)
	{
		$sql = "SELECT 
					t4.kode_tiket, t2.id as transaction_detail_id, t2.event_id, t2.data_pemesan, t1.invoice, t1.transaction_time, status,
					t3.nama_event, t3.nama_tiket, t3.date, t3.time, t3.lokasi

					FROM transactions t1 
					INNER JOIN transaction_detail t2 ON t1.id = t2.transaction_id
					INNER JOIN events t3 ON t2.event_id = t3.id
					LEFT JOIN ticket t4 ON t3.id = t4.event_id AND t2.id = t4.transaction_detail_id
					WHERE t1.invoice = ? AND t1.status = ?";
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$invoice, $status]);
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getDataPembayaranSuksesByInvoice($invoice)
	{
		$sql = "SELECT t1.invoice, t3.nama_event, t3.jenis_tiket, t1.metode_pembayaran, t1.virtual_account as va, 
				IF(t1.status = 1, 'Pembayaran Berhasil', '-') as status,
				t3.nama_tiket, t3.harga_tiket, t1.jumlah_tiket,
				FORMAT((t1.jumlah_tiket * t3.harga_tiket), 0, 'de_DE') as total,
				FORMAT(((t1.jumlah_tiket * t3.harga_tiket) + IF(t3.jenis_tiket = 2, 0, 3000)), 0, 'de_DE') as grand_total
				FROM transactions t1
				INNER JOIN transaction_detail t2 ON t1.id = t2.transaction_id 
				INNER JOIN events t3 ON t2.event_id = t3.id
				WHERE t1.status = 1 AND t1.invoice = ?
				LIMIT 1";
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$invoice]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function potongKuotaEvent($eventId, $jumlahTiket)
	{
		try {
			$event = $this->getEventById($eventId);
			if (!empty($event)) {
				$potongKuota = ((int) $event['sisa_tiket'] - (int) $jumlahTiket);

				$sql = "UPDATE `events` 
						SET 
							`sisa_tiket` = ?,
							`updated_at` = now()
						WHERE 
							`id` = ?";
				$stmt = $this->db->prepare($sql);
				$stmt->execute([$potongKuota, $eventId]);
				return true;
			} else {
				return false;
			}
		} catch (Exception $e) {
			error_log($e->getMessage());
			return false;
		}
	}

	public function getEventById($eventId)
	{
		$sql = "SELECT * FROM events WHERE id = ?";
		$stmt = $this->db->prepare($sql);
		$stmt->execute([$eventId]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getCarousel()
	{
		$sql = "SELECT * FROM carousel_banner";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function searchEvents($params = [])
	{
		$where = "";
		if(!empty($params['kategori'])){
			$where .= " AND t1.kategori_id = '". $params['kategori'] . "'";
		}

		if(!empty($params['lokasi'])){
			$where .= " AND t1.lokasi LIKE '%". $params['lokasi'] . "%'";
		}

		if(!empty($params['tanggal'])){
			$where .= " AND t1.date = '". $params['tanggal'] . "'";
		}

		$sql = "SELECT t1.id, t1.harga_tiket, t1.jenis_tiket, t1.nama_event, t2.nama, t1.penyelenggara, t1.url, t1.banner_event, t1.date
		FROM events t1 
		INNER JOIN ms_kategori t2 ON t2.id = t1.kategori_id
		WHERE date(`t1`.`date`) > date(NOW())
		AND t1.sisa_tiket > 0
		AND (t1.nama_event LIKE ? OR t2.nama LIKE ? OR t1.penyelenggara LIKE ?) $where";

		$stmt = $this->db->prepare($sql);
		$stmt->execute(["%".$params['search']."%", "%".$params['search']."%", "%".$params['search']."%"]);
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getDataEventAdmin($userId, $params = [])
	{
		$where = "";
		if(isset($params['start_date']) && isset($params['end_date'])){
            $where .= " WHERE DATE(t1.created_at) BETWEEN '". $params['start_date'] ."' AND '".$params['end_date']."'";
			if($userId !== ""){
				$where.= "AND t1.user_id = '". $userId ."'";
			}
        }else{
			if($userId !== ""){
				$where.= "WHERE t1.user_id = '". $userId ."'";
			}
		}

        if(!empty($params['organizer_id'])){
            $where .= "AND t3.id = '". $params['organizer_id'] ."'";
        }

		if(!empty($params['kategori_id'])){
            $where .= "AND t1.kategori_id = '". $params['kategori_id'] ."'";
        }

		$orderBy = " ORDER BY t1.date DESC ";
		$sql = "SELECT t1.id, t1.harga_tiket, 
				t1.jenis_tiket, t1.nama_event, t2.nama as nama_kategori, 
				t1.penyelenggara, t1.url, t1.banner_event, t1.date,
				t1.time, t1.lokasi, t1.nama_tiket,
				t1.jumlah_tiket, t1.sisa_tiket,
				t3.nama_organizer
		FROM events t1 
		INNER JOIN ms_kategori t2 ON t2.id = t1.kategori_id
		INNER JOIN `profile` t3 ON t3.user_id = t1.user_id
		".$where.$orderBy;
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}
