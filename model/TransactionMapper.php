<?php
require_once(__DIR__ . "/../core/PDOConnection.php");

class TransactionMapper
{
	private $db, $table = 'transactions';

	public function __construct()
	{
		$this->db = PDOConnection::getInstance();
	}

	public function insertData($table, $data)
	{
		try {
			$prep = array();
			foreach ($data as $k => $v) {
				$prep[':' . $k] = $v;
			}

			$columns = implode(", ", array_keys($data));
			$values = implode(',', array_keys($prep));

			$sql = "INSERT INTO `$table` ($columns) VALUES ($values)";

			$stmt = $this->db->prepare($sql);
			$stmt->execute($prep);

			return $this->db->lastInsertId();
		} catch (Exception $e) {
			error_log($e->getMessage());
			dd($e->getMessage());
			return false;
		}
	}

	public function getInvoice($invoice)
	{
		$sql = "SELECT * FROM $this->table WHERE invoice = ?";

		$stmt = $this->db->prepare($sql);
		$stmt->execute([$invoice]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function getPaymentByVA($metode_pembayaran, $payment)
	{
		$sql = "SELECT 
					t1.*,
					t2.data_pemesan
					FROM transactions t1
						INNER JOIN transaction_detail t2 ON t1.id = t2.transaction_id
					WHERE 
						t1.metode_pembayaran = ?
					AND
						t1.virtual_account = ?";

		$stmt = $this->db->prepare($sql);
		$stmt->execute([$metode_pembayaran, $payment]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function postPayment($metode_pembayaran, $payment, $status, $date = NULL)
	{
		if ($date === NULL) {
			$date = date('Y-m-d H:i:s');
		}

		$sql = "UPDATE 
					`transactions` 
						SET 
							`status` = ?,
							`transaction_time` = ?,
							`updated_at` = ?
						WHERE 
							`metode_pembayaran` = ?
						AND
							`virtual_account` = ?
							";

		$stmt = $this->db->prepare($sql);
		$stmt->execute([$status, $date, $date, $metode_pembayaran, $payment]);
		return true;
	}

	public function checkRekeningIfNotExist($userId)
	{
		$sql = "SELECT * FROM `rekening` WHERE `user_id` = ?";

		$stmt = $this->db->prepare($sql);
		$stmt->execute([$userId]);
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	public function updateRekening($data, $userId)
	{
		$sql = "UPDATE 
					`rekening` 
						SET 
							`nama_bank` = ?,
							`pemilik_rekening` = ?,
							`nomor_rekening` = ?,
							`kantor_cabang` = ?,
							`kota` = ?,
							`updated_at` = ?
						WHERE 
							`user_id` = ?";
		$stmt = $this->db->prepare($sql);
		$stmt->execute([
			$data['nama_bank'], 
			$data['pemilik_rekening'], 
			$data['nomor_rekening'], 
			$data['kantor_cabang'], 
			$data['kota'],
			date('Y-m-d H:i:s'),
			$userId
		]);
		return true;
	}

	public function getStatistikTtransaction($userId = "", $params) {
		$where = "";
        // if($userId !== "" || !empty($params)){
        //     $where = "WHERE ";
        // }

        if($userId !== ""){
            $where .= " AND t3.user_id = '". $userId ."'";
        }

        if(isset($params['start_date']) && isset($params['end_date'])){
            $where .= " AND DATE(t1.created_at) BETWEEN '". $params['start_date'] ."' AND '".$params['end_date']."'";
        }

		if(!empty($params['organizer_id'])){
            $where .= "AND t4.id = '". $params['organizer_id'] ."'";
        }

		if(!empty($params['kategori_id'])){
            $where .= "AND t3.kategori_id = '". $params['kategori_id'] ."'";
        }

		$where .= " AND `t1`.`status` = 1";

        $sql = "SELECT t1.invoice,
					MONTH(t1.created_at) as bulan, YEAR(t1.created_at) as tahun, (t1.total + t1.fee) as real_total 
					FROM transactions t1
					INNER JOIN transaction_detail t2 ON t1.id = t2.transaction_id
        			INNER JOIN events t3 ON t3.id = t2.event_id
					INNER JOIN `profile` t4 ON t3.user_id = t4.user_id
					WHERE t1.status = '1'
        $where GROUP BY t1.invoice, t1.created_at, t1.total, t1.fee";

		
// print_r($sql);
// die;

        $stmt = $this->db->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

		$getUniqueMonth = array_unique(array_column($result, 'bulan'));
		sort($getUniqueMonth);
		$data = [];
		$monthName = [
			'1' => 'Januari',
			'2' => 'Februari',
			'3' => 'Maret',
			'4' => 'April',
			'5' => 'Mei',
			'6' => 'Juni',
			'7' => 'Juli',
			'8' => 'Agustus',
			'9' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember',
		];

		foreach ($getUniqueMonth as $value) {
			$filterData = array_filter($result, function($row) use ($value) {
				if($row['bulan'] === $value){
					return true;
				}
			});

			sort($filterData);
			$data[] = [
				'month' => $value,
				'month_name' => $monthName[$value],
				'total' => array_sum(array_column($filterData, 'real_total')),
				'data' => $filterData
			];
		}
        return $data;
	}
}
