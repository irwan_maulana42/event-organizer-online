<?php
require_once(__DIR__ . "/../model/MemberMapper.php");
require_once(__DIR__ . "/../model/Member.php");

require_once(__DIR__ . "/../model/UserMapper.php");
require_once(__DIR__ . "/../model/TicketMapper.php");
require_once(__DIR__ . "/../model/MSPembeliMapper.php");

require_once(__DIR__ . "/../core/ViewManager.php");
require_once(__DIR__ . "/../controller/BaseController.php");

// use Endroid\QrCode\Builder\Builder;
// use Endroid\QrCode\Encoding\Encoding;
// use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
// use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
// use Endroid\QrCode\Label\Font\NotoSans;
// use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
// use Endroid\QrCode\Writer\PngWriter;

class TicketController extends BaseController
{
    private $memberMapper, $ticketMapper, $pembeliMapper, $generatorBarcode;
    private static $generator;
    protected $user, $userMapper;
    public function __construct()
    {
        parent::__construct();

        $this->view->setLayout('notitle');

        $this->memberMapper = new MemberMapper();

        $userMapper = new UserMapper();
        $this->ticketMapper = new TicketMapper();
        $this->pembeliMapper = new MSPembeliMapper();

        if (isset($_SESSION['currentuser'])) {
            $this->view->setVariable('detail_user', $userMapper->getDetailuser($_SESSION['currentuser']));
        }
    }

    public function index()
    {
        if (!isset($_SESSION['currentuser'])) return $this->view->redirect("Users", "login");

        $tikets = $this->ticketMapper->getTicketByEmail($this->detailUsers['email']);
        $this->view->setVariable('tickets', $tikets);
        $this->view->setVariable('title', 'Tiket Saya');
        $this->view->render('member/ticket', 'index');
    }

    public function getTicketByKode()
    {
        $kode_tiket = $_POST['kode_tiket'];
        $eventId = $_POST['event_id'];
        $ticket = $this->ticketMapper->getTicketByKode($kode_tiket, $eventId);

        try {
            if (!empty($ticket)) {

                // $result = Builder::create()
                //     ->writer(new PngWriter())
                //     ->writerOptions([])
                //     ->data($ticket['kode_tiket'])
                //     ->encoding(new Encoding('UTF-8'))
                //     ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
                //     ->size(300)
                //     ->margin(10)
                //     ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
                //     // ->logoPath(__DIR__ . '/assets/symfony.png')
                //     ->labelText($ticket['kode_tiket'])
                //     ->labelFont(new NotoSans(20))
                //     ->labelAlignment(new LabelAlignmentCenter())
                //     ->build();
                // $dataUri = $result->getDataUri();

                // dump(base64_encode($qr));
                echo json_encode([
                    'ticket' => $ticket,
                    'temp_pemesan' => $this->pembeliMapper->getAll(),
                    // 'qr' => $dataUri
                ]);
            }else{
                echo json_encode([
                    'ticket' => NULL,
                    'temp_pemesan' => $this->pembeliMapper->getAll()
                ]);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}
