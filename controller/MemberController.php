<?php
require_once(__DIR__ . "/../model/MemberMapper.php");
require_once(__DIR__ . "/../model/Member.php");
require_once(__DIR__ . "/../model/EventMapper.php");
require_once(__DIR__ . "/../model/TicketMapper.php");
require_once(__DIR__ . "/../model/TransactionMapper.php");

require_once(__DIR__ . "/../model/UserMapper.php");
require_once(__DIR__ . "/../model/MSKategoriMapper.php");

require_once(__DIR__ . "/../core/ViewManager.php");
require_once(__DIR__ . "/../controller/BaseController.php");

class MemberController extends BaseController
{
    private $memberMapper, $eventMapper, $ticketMapper, $MSKategoriMapper, $transactionMapper;
    protected $user, $userMapper;
    public function __construct()
    {
        parent::__construct();
		
        if(!isset($_SESSION['currentuser'])){
            return $this->view->redirect("Users", "login");
        }

        $this->memberMapper = new MemberMapper();
        $this->eventMapper = new EventMapper();
        $this->ticketMapper = new TicketMapper();
        $this->transactionMapper = new transactionMapper();

        $this->MSKategoriMapper = new MSKategoriMapper();


        $this->view->setLayout('notitle');
        $userMapper = new UserMapper();
        $this->userMapper = $userMapper;
        $this->view->setVariable('detail_user', $userMapper->getDetailuser($_SESSION['currentuser']));
    }

    public function index()
    {
        $countAktif = 0;
        $countTiketTerjual = 0;
        $countTotalTransaksi = 0;
        $dataOrganizer = [];
        $dataKategori = [];

        $userId = $this->detailUsers['id'];

        if((int) $this->detailUsers['is_superadmin'] !== 1){
            // $ticketTerjual = $this->ticketMapper->getTiketTerjual($userId);
            // $eventAktif = $this->eventMapper->getDataEventByUserId($userId, 'aktif');
            // $totalTransaksi = $this->ticketMapper->getTotalTransaksi($userId);
    
            // if(!empty($eventAktif)){
            //     $countAktif = count($eventAktif);
            // }
    
            // if(!empty($ticketTerjual)){
            //     $countTiketTerjual = count($ticketTerjual);
            // }
    
            // if(!empty($totalTransaksi)){
            //     $countTotalTransaksi = $totalTransaksi['grand_total'];
            // }
        } else {
        }
        $dataKategori = $this->MSKategoriMapper->findAll();
        $dataOrganizer = $this->userMapper->getAllDataOrganizer();

        $this->view->setVariable('eventAktif', $countAktif);
        $this->view->setVariable('tiketTerjual', $countTiketTerjual);
        $this->view->setVariable('grandTotal', $countTotalTransaksi);

        $this->view->setVariable('dataKategori', base64_encode(json_encode($dataKategori)));
        $this->view->setVariable('dataOrganizer', base64_encode(json_encode($dataOrganizer)));

        $this->view->setVariable('title', 'Member');
        if((int) $this->detailUsers['is_superadmin'] == 1){
            $this->view->render('member', 'superadmin_index');
        }else{
            $this->view->render('member', 'index');
        }
    }

    public function getDataSuperAdmin()
    {
        $params = $_GET;

        $userId = "";
        if($this->detailUsers['is_superadmin'] !== 1){
            $userId = $this->detailUsers['id'];
        }
        $ticketTerjual = $this->ticketMapper->getTiketTerjual($userId, $params);
        $eventAktif = $this->eventMapper->getDataEventByUserId($userId, 'aktif', $params);
        $totalTransaksi = $this->ticketMapper->getTotalTransaksi($userId, $params);

        $dataEvent = $this->eventMapper->getDataEventAdmin($userId, $params);

        $getStatistikTiketTerjual = $this->ticketMapper->getStatistikTiketTerjual($userId, $params);
        $getStatistikTransaksi = $this->transactionMapper->getStatistikTtransaction($userId, $params);

        $dataEvent = array_map(function($row){
            $total_transaksi = $this->ticketMapper->getTotalTransaksiByEvent(['event_id' => $row['id']]);
            $row['total_transaksi'] = "Rp ".number_format($total_transaksi, 0, '.', '.');
            $row['harga_tiket'] = "Rp ".number_format($row['harga_tiket'], 0, '.', '.');

            return $row;
        }, $dataEvent);

        echo json_encode([
            'tiket_terjual' => count($ticketTerjual),
            'event_aktif' => count($eventAktif),
            'total_transaksi' => number_format(@$totalTransaksi['grand_total'], 0, '.', '.'),
            'events' => $dataEvent,
            'statistik_tiket_terjual' => $getStatistikTiketTerjual[0], 
            'statistik_transaksi' => $getStatistikTransaksi
        ]);
    }

    public function change_password()
    {
		$this->view->setVariable('title', 'Ubah Password');
        $this->view->render('member/profile/ubah_password', 'index');
    }

    public function changePasswordUser()
    {
        $new_password = $_POST['password_baru'];

        $result = $this->userMapper->changePasswordUser($new_password, $this->detailUsers['email']);
        if($result){
            session_destroy();
            $this->view->setFlash(['status' => 'success', 'message' => 'Password berhasil diganti. Silahkan login ulang']);
            $this->view->redirect("users", "login", 'change_pass=Password berhasil diganti. Silakan login ulang.');
        }else{
            $this->view->setFlash(['status' => 'danger', 'message' => 'Oops... Something went wrong. Please try again!']);
            $this->view->redirect("Member", "change_password");
        }
    }

    public function list_organizer()
    {
        $this->view->setVariable('title', 'List Organizer');
        $this->view->render('member/organizer', 'index');
    }

    public function getListOrganizer() {
        $request = $_POST;
        $data = $this->memberMapper->getAllProfileOrganizer();
        echo json_encode($data);
    }

    public function getListEventsByUserId() {
        $request = $_GET;
        $data = $this->memberMapper->getAllEventsByUserId($request);
        echo json_encode($data);
    }

    public function getListTrxByEvent() {
        $request = $_GET;
        $data = $this->memberMapper->getTrxByEvent($request);
        echo json_encode($data);
    }
}