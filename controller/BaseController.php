<?php
//file: controller/BaseController.php
require_once(__DIR__ . '/../vendor/autoload.php');

require_once(__DIR__ . "/../core/ViewManager.php");
require_once(__DIR__ . "/../core/I18n.php");

require_once(__DIR__ . "/../model/User.php");
require_once(__DIR__ . "/../model/UserMapper.php");

require_once(__DIR__ . "/../helpers/Helper.php");

use Cloudinary\Configuration\Configuration;
use Cloudinary\Api\Upload\UploadApi;
date_default_timezone_set("Asia/Jakarta");

class BaseController
{

	/**
	 * The view manager instance
	 * @var ViewManager
	 */
	protected $view;
	public $helper, $detailUsers;

	/**
	 * The current user instance
	 * @var User
	 */
	protected $currentUser;

	public function __construct()
	{
		// Configure globally cloduinary
		Configuration::instance([
			'cloud' => [
				'cloud_name' => 'dwqwwil7k',
				'api_key' => '235439449818346',
				'api_secret' => 'X7CATIUK8RFhV45PQDcG3E3jCMo',
			],
			'url' => [
				'secure' => true
			]
		]);

		$this->view = ViewManager::getInstance();
		if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}

		if (isset($_SESSION["currentuser"])) {

			$this->currentUser = new User($_SESSION["currentuser"]);
			//add current user to the view, since some views require it
			$this->view->setVariable("currentusername", $this->currentUser->getUsername());

			$userMapper = new UserMapper();
			$detailUsers = $userMapper->getDetailuser($_SESSION['currentuser']);
			$this->detailUsers = $detailUsers;
			$this->view->setVariable('detail_user', $detailUsers);
		}
	}

	public function checkIfNotUserLogin()
	{
		if (!isset($_SESSION['currentuser'])) return $this->view->redirect("Users", "login");
	}

	public function uploadImage($file, $options = []){
		$api = new UploadApi();
		$upload = $api->upload($file, $options);
		$response = json_encode($upload);
		return json_decode($response);
	}
}
