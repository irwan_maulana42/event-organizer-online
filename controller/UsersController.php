<?php

require_once(__DIR__ . "/../core/ViewManager.php");
require_once(__DIR__ . "/../core/I18n.php");

require_once(__DIR__ . "/../model/User.php");
require_once(__DIR__ . "/../model/UserMapper.php");
require_once(__DIR__ . "/../model/EventMapper.php");


require_once(__DIR__ . "/../controller/BaseController.php");
class UsersController extends BaseController
{
	private $userMapper, $eventMapper;
	protected $user;
	public function __construct()
	{
		parent::__construct();

		$this->userMapper = new UserMapper();
		$this->eventMapper = new EventMapper();
		$this->view->setLayout("notitle");
		$this->user = new User();
	}

	public function login()
	{
		if (isset($_SESSION['currentuser'])) return $this->view->redirect("Member", "index");

		$this->view->setVariable('title', 'Login');
		if (isset($_POST["email"])) {
			if(empty($_POST["email"]) && empty($_POST["password"])){
				$errors = array();
				$errors["general"] = "Email is not valid";
				$this->view->setVariable("errors", $errors);
				$this->view->setFlash(['status' => 'danger', 'message' => 'Email or Password cannot be empty !']);
			}else{
				$this->user->setEmail($_POST["email"]);
				$this->user->setPassword($_POST["password"]);
	
				if ($this->userMapper->isValidUser($this->user->getEmail(), $this->user->getPasswd())) {
					$_SESSION["currentuser"] = $this->user->getEmail();
					$this->view->setFlash(['status' => 'success', 'message' => 'Hi, Welcome Back...']);
					$this->view->redirect("Member", "index");
				} else {
					$errors = array();
					$errors["general"] = "Email is not valid";
					$this->view->setVariable("errors", $errors);
					$this->view->setFlash(['status' => 'danger', 'message' => 'Email or Password Wrong !']);
				}
			}
		}

		$this->view->render("login", "index");
	}

	public function register()
	{
		if (isset($_SESSION['currentuser'])) return $this->view->redirect("Member", "index");

		$this->view->setVariable('title', 'Register');

		if (isset($_POST["email"])) {
			$this->user->setEmail($_POST["email"]);
			$this->user->setPassword($_POST["password"]);
			try {
				$this->user->checkIsValidForRegister();

				if (!$this->userMapper->emailExists($this->user->getEmail())) {
					$this->userMapper->save($this->user);
					// $this->view->setFlash("Email " . $this->user->getEmail() . " successfully added. Please login now");
					$this->eventMapper->sendEmail("Event Organizer Online", $_POST['email'], "Registrasi Berhasil !", "Terima kasih telah melakukan registrasi di webiste Event Organizer Online.");
					return $this->view->redirect("users", "login");
				} else {
					$errors = array();
					$errors["email"] = "Email already exists";
					$this->view->setFlash(['status' => 'danger', 'message' => 'Email already exists !']);
					$this->view->setVariable("errors", $errors);
				}
			} catch (ValidationException $ex) {
				$errors = $ex->getErrors();
				$this->view->setVariable("errors", $errors);
			}
		}
		$this->view->setVariable("user", $this->user);

		$this->view->render("register", "index");
	}

	public function logout()
	{
		session_destroy();
		if ($_POST) {
			return true;
		} else {
			$this->view->redirect("users", "login");
		}
	}

	public function informasi_kamu()
	{
		$this->checkIfNotUserLogin();
		$this->view->setLayout("notitle");
		$this->view->setVariable('title', 'Informasi Dasar');
		$data = $this->userMapper->getProfile($this->detailUsers['id']);
		$this->view->setVariable('profile', $data);
		$this->view->render("member/profile/informasi_kamu", "index");
	}

	public function postInformasi()
	{
		$data = [];
		$this->checkIfNotUserLogin();
		$data = [
			'user_id' => $this->detailUsers['id'],
			'nama_organizer' => $_POST['nama_organizer'],
			'no_handphone' => $_POST['nomor_handphone'],
			'alamat' => @$_POST['alamat'],
			'tentang_kami' => @$_POST['tentang_kami'],
			'details' => $this->detailUsers
		];

		if ($_FILES['profile_image']['error'] === 0) {
			$resUpload = $this->uploadImage($_FILES['profile_image']['tmp_name']);
			$data['profile_image'] = $resUpload;
		}

		$response = $this->userMapper->saveProfile($data);
		if ($response) {
			$this->view->setFlash(['status' => 'success', 'message' => 'Data berhasil diubah !']);
			$this->view->redirect("Users", "informasi_kamu");
		} else {
			$this->view->setFlash(['status' => 'danger', 'message' => 'Data gagal diubah !']);
			$this->view->redirect("Users", "informasi_kamu");
		}
	}

	public function checkingPassword()
	{
		$check = $this->userMapper->isValidUser($this->detailUsers['email'], $_POST['password_lama']);
		echo json_encode($check);
	}
}
