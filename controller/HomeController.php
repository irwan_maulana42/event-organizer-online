<?php
require_once(__DIR__ . "/../core/ViewManager.php");
require_once(__DIR__ . "/../controller/BaseController.php");
require_once(__DIR__ . "/../controller/EventController.php");
require_once(__DIR__ . "/../model/EventMapper.php");
require_once(__DIR__ . "/../model/TransactionMapper.php");
require_once(__DIR__ . "/../model/MSKategoriMapper.php");

date_default_timezone_set("Asia/Jakarta");
class HomeController extends BaseController
{
    protected $eventMapper, $transactionMapper, $eventController, $MSKategoriMapper;

    public function __construct()
    {
        parent::__construct();
        $this->eventMapper = new EventMapper();
        $this->transactionMapper = new TransactionMapper();
        $this->eventController = new EventController();
        $this->MSKategoriMapper = new MSKategoriMapper();

    }

    public function index()
    {
        $data = $this->eventMapper->getDataEvent();
        $carousel = $this->eventMapper->getCarousel();

        $featured = $this->eventMapper->getDataEventFeatured();
        usort($featured, function ($a, $b) {
            return $b['id'] - $a['id'];
        });
        $this->view->setVariable('featured', @$featured[0]);
        $this->view->setVariable('carousel', $carousel);

        $this->view->setVariable('top_selling', array_slice($data, 0, 5));

        $this->view->setVariable('events', $data);
        $this->view->render('home', 'index');
    }

    public function hubungi_kami()
    {
        $this->view->render('owner', 'hubungi_kami');
    }

    public function payment()
    {
        $this->view->render('tools', 'aktivator');
    }

    public function discover()
    {
        $dataKategori = $this->MSKategoriMapper->findAll();
        $this->view->setVariable('kategori', $dataKategori);
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = $this->eventMapper->searchEvents(@$_POST);
            $this->view->setVariable('events', $data);
            $this->view->redirect('Home', 'discover', http_build_query($_POST));
        } else {
            $data = $this->eventMapper->searchEvents(@$_GET);
            $this->view->setVariable('events', $data);
            $this->view->render('discover', 'index');
        }
    }

    public function aktivasiPayment()
    {
        $result = $this->transactionMapper->postPayment($_POST['metode_pembayaran'], $_POST['va'], 1);
        if ($result) {
            $this->eventController->createTicket($_POST['invoice'], $_POST['email_notifikasi']);
        }
        echo json_encode($result);
    }

    public function cekAktivasi()
    {
        $result = $this->transactionMapper->getPaymentByVA($_POST['metode_pembayaran'], $_POST['va']);
        echo json_encode($result);
    }

    public function rekening_kamu()
    {
        $rekening = $this->transactionMapper->checkRekeningIfNotExist($this->detailUsers['id']);
        $this->view->setVariable('rekening', $rekening);
        $this->view->setVariable('title', 'Rekening Kamu');
        $this->view->setLayout('notitle');
        $this->view->render('member/profile/rekening_kamu', 'index');
    }

    public function post_rekening_kamu()
    {
        $result = "";
        $checkrekening = $this->transactionMapper->checkRekeningIfNotExist($this->detailUsers['id']);
        if(!empty($checkrekening)){
            $result = $this->transactionMapper->updateRekening([
                'nama_bank' => $_POST['bank'],
                'pemilik_rekening' => $_POST['pemilik_rekening'],
                'nomor_rekening' => $_POST['nomor_rekening'],
                'kantor_cabang' => $_POST['kantor_cabang'],
                'kota' => $_POST['kota'],
            ], $this->detailUsers['id']);
        }else{
            $result = $this->eventMapper->insertData('rekening', [
                'user_id' => $this->detailUsers['id'],
                'nama_bank' => $_POST['bank'],
                'pemilik_rekening' => $_POST['pemilik_rekening'],
                'nomor_rekening' => $_POST['nomor_rekening'],
                'kantor_cabang' => $_POST['kantor_cabang'],
                'kota' => $_POST['kota'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        if ($result) {
            $this->view->setFlash(['status' => 'success', 'message' => 'Rekening berhasil diubah !']);
            $this->view->redirect("Home", "rekening_kamu");
        } else {
            $this->view->setFlash(['status' => 'danger', 'message' => 'Rekening gagal diubah !']);
            $this->view->redirect("Home", "rekening_kamu");
        }
    }
}
