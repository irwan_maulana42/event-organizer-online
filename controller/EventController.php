<?php
require_once(__DIR__ . "/../core/ViewManager.php");
require_once(__DIR__ . "/../controller/BaseController.php");

require_once(__DIR__ . "/../model/UserMapper.php");
require_once(__DIR__ . "/../model/MSKategoriMapper.php");
require_once(__DIR__ . "/../model/MSPembeliMapper.php");
require_once(__DIR__ . "/../model/EventMapper.php");
require_once(__DIR__ . "/../model/TransactionMapper.php");
date_default_timezone_set("Asia/Jakarta");

use Ramsey\Uuid\Uuid;

class EventController extends BaseController
{
  protected $kategori, $eventMapper, $pembeliMapper, $transactionMapper;

  public function __construct()
  {
    parent::__construct();
    $userMapper = new UserMapper();
    $this->kategori = new MSKategoriMapper();
    $this->eventMapper = new EventMapper();
    $this->pembeliMapper = new MSPembeliMapper();
    $this->transactionMapper = new TransactionMapper();

    if (isset($_SESSION['currentuser'])) {
      $this->view->setVariable('detail_user', $userMapper->getDetailuser($_SESSION['currentuser']));
    }
  }

  public function index()
  {
    $event = @$_GET['event'];
    $getEvent = $this->eventMapper->getDataEventById($event);

    if (!isset($event) || empty($getEvent)) {
      return $this->view->render('errors', '404');
    }
    $this->view->setVariable('event', $getEvent);
    $this->view->render('event', 'index');
  }

  public function softBuy()
  {
    $post = $_POST;
    $post['token'] = Uuid::uuid4()->toString();
    $data = json_encode($post);
    $cookie = isset($_COOKIE['order-id-' . $post['order_id']]);
    if ($cookie) {
      $cookies = $_COOKIE['order-id-' . $post['order_id']];
      $data = json_decode($data, true);
      $data['token'] = json_decode($cookies, true)['token'];
      $result = $this->eventMapper->updateTempTransaction($post['order_id'], $data['token'], json_encode($data));
      setcookie('order-id-' . $post['order_id'], json_encode($data), time() + 60 * 15);
      return $this->view->redirect('Event', 'buy', 'token=' . $data['token']);
    } else {
      $result = $this->eventMapper->insertData('temp_transaction', [
        'order_id' => $post['order_id'],
        'token' => $post['token'],
        'data' => $data,
        'created_at' => date('Y-m-d H:i:s'),
        'expired' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' + 15 minute')),
        'updated_at' => date('Y-m-d H:i:s'),
      ]);
      if ($result) {
        setcookie('order-id-' . $post['order_id'], $data, time() + 60 * 15);
      } else {
        dd('Oops... Something went wrong!');
      }
      return $this->view->redirect('Event', 'buy', 'token=' . $post['token']);
    }
  }

  public function buy()
  {
    $token = @$_GET['token'];
    $temp = $this->eventMapper->getTempTransaction($token);
    $dataPembeli = $this->pembeliMapper->getAll();
    if (!isset($temp) || empty($temp)) {
      return $this->view->render('errors', '404');
    }

    $event = $this->eventMapper->getDataEventById($temp['order_id']);
    // dd($temp);
    $this->view->setVariable('temp', $temp);
    $this->view->setVariable('event', $event);
    $this->view->setVariable('pembeli', $dataPembeli);

    $this->view->render('event/buy', 'index');
  }

  public function event_saya()
  {
    $this->checkIfNotUserLogin();
    $eventAktif = $this->eventMapper->getDataEventByUserId($this->detailUsers['id'], 'aktif');
    $eventTidakAktif = $this->eventMapper->getDataEventByUserId($this->detailUsers['id'], 'tidak_aktif');
    $this->view->setVariable('events', ['event_aktif' => $eventAktif, 'event_tidak_aktif' => $eventTidakAktif]);
    $this->view->setLayout('notitle');
    $this->view->setVariable('title', 'Event Saya');
    $this->view->render('member/events', 'index');
  }

  public function getDetailEventSaya()
  {
    $event = $_POST['url'];
    $status = $_POST['status'];
    $getEvent = $this->eventMapper->getDataEventById($event, $status);
    echo json_encode($getEvent);
  }

  public function create_event()
  {
    $this->checkIfNotUserLogin();
    $this->view->setLayout('notitle');
    $dataPembeli = $this->pembeliMapper->getAll();
    $this->view->setVariable('dataPembeli', $dataPembeli);
    $this->view->setVariable('title', 'Buat Event');
    $this->view->setVariable('dataKategori', $this->kategori->findAll());
    $this->view->render('member/events', 'create');
  }

  public function post_event()
  {
    $this->checkIfNotUserLogin();

    $resUpload = $this->uploadImage($_FILES['banner_event']['tmp_name']);
    $data = [
      'user_id' => $this->detailUsers['id'],
      'nama_event' => $_POST['nama_event'],
      'banner_event' => json_encode($resUpload),
      'url' => filter_alphanum($_POST['nama_event']) . Uuid::uuid4()->toString(),
      'kategori_id' => $_POST['kategori'],
      'date' => $_POST['date'],
      'time' => $_POST['time'],
      'lokasi' => $_POST['lokasi'],
      'penyelenggara' => $_POST['penyelenggara'],
      'jenis_tiket' => $_POST['jenis_tiket'],
      'nama_tiket' => $_POST['nama_tiket'],
      'jumlah_tiket' => $_POST['jumlah_tiket'],
      'sisa_tiket' => $_POST['jumlah_tiket'],
      'harga_tiket' => $_POST['harga_tiket'],
      'deskripsi_tiket' => $_POST['deskripsi_tiket'],
      'deskripsi_event' => base64_encode($_POST['deskripsi_event']),
      'syarat_ketentuan' => base64_encode($_POST['syarat_ketentuan']),
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s'),
      'atur_data_pembeli' => json_encode($_POST['atur_data_pembeli']),
    ];
    $result = $this->eventMapper->postEvent($data);
    if ($result) {
      $this->view->setFlash(['status' => 'success', 'message' => 'Event berhasil dibuat !']);
      $this->view->redirect("Member", "index");
    } else {
      $this->view->setFlash(['status' => 'danger', 'message' => 'Event gagal dibuat !']);
      $this->view->redirect("Member", "index");
    }
  }

  public function sendEmail()
  {
    $to = $_GET['to'];
    $name = $_GET['name'];

    // return $this->eventMapper->sendEmail($to, $name);
  }

  public function checkout()
  {
    $event = $this->eventMapper->getDataEventById($_POST['url']);

    $invoice = "INV/" . date('Ymd') . "/" . rand(1000, 9999) . "/" . chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90));
    $dataPemesan = [];
    $virtual_account = rand(100000, 999999);

    for ($i = 0; $i < $_POST['jumlah_tiket']; $i++) {
      foreach (json_decode($event['atur_data_pembeli']) as $key => $value) {
        $dataPemesan[$i][$value] = $_POST[$value][$i];
      }
    }

    $lastId = $this->transactionMapper->insertData('transactions', [
      'invoice' => $invoice,
      'status' => 0,
      'metode_pembayaran' => $_POST['metode_pembayaran'],
      'jumlah_tiket' => $_POST['jumlah_tiket'],
      'virtual_account' => $virtual_account,
      'email_notifikasi' => $_POST['email_notifikasi'],
      'total' => $_POST['total'],
      'fee' => $_POST['fee'],
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s')
    ]);

    for ($i = 0; $i < $_POST['jumlah_tiket']; $i++) {
      $subResult = $this->transactionMapper->insertData('transaction_detail', [
        'transaction_id' => $lastId,
        'event_id' => $_POST['event_id'],
        'data_pemesan' => json_encode($dataPemesan[$i]),
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s')
      ]);
    }
    $dataEmail = [
      'nama_event' => $event['nama_event'],
      'nama_tiket' => $event['nama_tiket'],
      'harga_tiket' => number_format($event['harga_tiket'], 0, '.', '.'),
      'jumlah_tiket' => $_POST['jumlah_tiket'],
      'total' => number_format(((int) $_POST['jumlah_tiket'] * (int) $event['harga_tiket']), 0, '.', '.'),
      'grand_total' => number_format((((int) $_POST['jumlah_tiket'] * (int) $event['harga_tiket']) + (int) $_POST['fee']), 0, '.', '.'),
      'jenis_tiket' => $event['jenis_tiket'],
      'invoice' => $invoice,
      'status' => 'Menunggu Pembayaran',
      'va' => $virtual_account,
      'metode_pembayaran' => $_POST['metode_pembayaran']
    ];
    $bodyEmail = $this->customEmail('pembayaran', $dataEmail);
    // dd(["EMAIL" => $bodyEmail, "DATA EMAIL" => $dataEmail, 'email_notifikasi' => $_POST['email_notifikasi']]);
    $this->eventMapper->potongKuotaEvent($_POST['event_id'], $_POST['jumlah_tiket']);
    if((int) $event['jenis_tiket'] !== 2){
      $this->eventMapper->sendEmail('Event Organizer Online', $_POST['email_notifikasi'], 'Pembayaran', $bodyEmail);
    }else{
      $result = $this->transactionMapper->postPayment($_POST['metode_pembayaran'], $virtual_account, 1);
      if ($result) {
          $this->createTicket($invoice, $_POST['email_notifikasi']);
      }
    }
    return $this->view->redirect('Event', 'payment', 'invoice=' . $invoice);
  }

  public function payment()
  {
    $invoice = $this->transactionMapper->getInvoice($_GET['invoice']);
    if (empty($invoice)) {
      return $this->view->render('errors', '404');
    }
    $this->view->setVariable('invoice', $invoice);
    $this->view->render('invoice', 'index');
  }

  public function createTicket($invoice, $email_notifikasi)
  {
    try {
      $data = $this->eventMapper->getDataTransactionByInvoice($invoice);
      $getPaymentSuccess = $this->eventMapper->getDataPembayaranSuksesByInvoice($invoice);
      if (!empty($data)) {
        $this->eventMapper->sendEmail('Event Organizer Online', $_POST['email_notifikasi'], 'Pembayaran', $this->customEmail('pembayaran', $getPaymentSuccess, TRUE));
        foreach ($data as $key => $value) {
          $kode_tiket = chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90)) . chr(rand(65, 90)) . rand(100, 999);
          $subResult = $this->transactionMapper->insertData('ticket', [
            'event_id' => $value['event_id'],
            'transaction_detail_id' => $value['transaction_detail_id'],
            'kode_tiket' => $kode_tiket,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]);
          $value['kode_tiket'] = $kode_tiket;
          $bodyEmail = $this->customEmail('pembayaran_success_per_tiket', ['data' => $value, 'master_pemesan' => '']);
          $this->eventMapper->sendEmail('Event Organizer Online', $_POST['email_notifikasi'], 'Pembayaran', $bodyEmail);
        }
      }
      return true;
    } catch (Exception $e) {
      error_log($e->getMessage());
      return false;
    }
  }

  public function customEmailPembayaranSuccess($data)
  {
    $result = $data['data'];
    $temp = $data['master_pemesan'];
    $dataPribadi = json_decode($result['data_pemesan'], true);
    $email = <<<term
                    <p>Hai {$dataPribadi['nama_lengkap']},</p>
                    <p>Terima kasih anda sudah memesan tiket di Event Organizer Online</p>
                    <p>Proses pembayaran Anda telah sukses!</p>
                    <p>Berikut e-voucher kamu dan mohon jaga dengan baik sampai waktu yang ditentukan.</p>
                    <h4>Detail Pemesanan Anda</h4>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <th align="left" style="padding: 3px;width: 25%;">Nama Event</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['nama_event']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;width: 25%;">Nama Tiket</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['nama_tiket']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;width: 25%;">Kode Tiket</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['kode_tiket']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Tanggal</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['date']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Jam</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['time']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Lokasi</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['lokasi']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Invoice</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['invoice']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Status</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">Pembayaran Berhasil</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Waktu Transaksi</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$result['transaction_time']}</td>
                            </tr>
                        </tbody>
                    </table>
                    <p>Good luck! Hope it works.</p>
        term;

    return $email;
  }

  public function customEmailPembayaran($data, $status = FALSE)
  {
    $header = "";
    $fee = ((int) $data['jenis_tiket'] !== 2) ? "3.000" : "0";
    if ($status) {
      $header = "<p>Proses pembayaran Anda telah sukses!</p>
        <p>e-voucher akan dikirimkan ke email masing-masing.</p>";
    } else {
      $header = "<p>Proses pemesanan Anda telah sukses!</p>
                    <p>Namun anda belum dapat menerima e-voucher karena kami belum menerima pembayaran pemesanan tiket anda.</p>";
    }
    $email = <<<term
                    <p>Hai,</p>
                    <p>Terima kasih anda sudah memesan tiket di Event Organizer Online</p>
                    $header
                    <h4>Detail Pemesanan Anda</h4>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <th align="left" style="padding: 3px;width: 25%;">Nama Event</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$data['nama_event']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Invoice</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$data['invoice']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Metode Pembayaran</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$data['metode_pembayaran']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Virtual Account</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$data['va']}</td>
                            </tr>
                            <tr>
                                <th align="left" style="padding: 3px;">Status</th>
                                <td align="left" style="padding: 3px;width: 1%;">:</td>
                                <td align="left" style="padding: 3px;">{$data['status']}</td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                    <table role="presentation" border="1" cellpadding="0" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="padding: 3px;">Ringkasan</th>
                                <th style="padding: 3px;">Harga per Tiket</th>
                                <th style="padding: 3px;">Kuantitas</th>
                                <th style="padding: 3px;">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td align="left" style="padding: 3px;">{$data['nama_tiket']}</td>
                                <td align="center" style="padding: 3px;">Rp. {$data['harga_tiket']}</td>
                                <td align="center" style="padding: 3px;">{$data['jumlah_tiket']}</td>
                                <td align="left" style="padding: 3px;">Rp. {$data['total']}</td>
                            </tr>
                            <tr>
                                <th align="left" colspan="3" style="padding: 3px;">
                                    Fee
                                </th>
                                <td style="padding: 3px;">
                                    Rp. $fee
                                </td>
                            </tr>
                            <tr>
                                <th align="left" colspan="3" style="padding: 3px;">
                                    Grand Total
                                </th>
                                <th style="padding: 3px;">
                                    Rp. {$data['grand_total']}
                                </th>
                            </tr>
                        </tbody>
                    </table>
                    <p>Good luck! Hope it works.</p>
        term;

    return $email;
  }

  public function customEmail($type = NULL, $data = [], $option = NULL)
  {
    $body = "";
    if ($type === 'pembayaran') {
      $body = $this->customEmailPembayaran($data, $option);
    } else if ($type === 'pembayaran_success_per_tiket') {
      $body = $this->customEmailPembayaranSuccess($data);
    }

    $email = <<<term

        <!doctype html>
        <html>
          <head>
            <meta name="viewport" content="width=device-width" />
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Simple Transactional Email</title>
            <style>
              /* -------------------------------------
                  GLOBAL RESETS
              ------------------------------------- */
              
              /*All the styling goes here*/
              
              img {
                border: none;
                -ms-interpolation-mode: bicubic;
                max-width: 100%; 
              }
        
              body {
                background-color: #f6f6f6;
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%; 
              }
        
              table {
                border-collapse: separate;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                width: 100%; }
                table td {
                  font-family: sans-serif;
                  font-size: 14px;
                  vertical-align: top; 
              }
        
              /* -------------------------------------
                  BODY & CONTAINER
              ------------------------------------- */
        
              .body {
                background-color: #f6f6f6;
                width: 100%; 
              }
        
              /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
              .container {
                display: block;
                margin: 0 auto !important;
                /* makes it centered */
                max-width: 580px;
                padding: 10px;
                width: 580px; 
              }
        
              /* This should also be a block element, so that it will fill 100% of the .container */
              .content {
                box-sizing: border-box;
                display: block;
                margin: 0 auto;
                max-width: 580px;
                padding: 10px; 
              }
        
              /* -------------------------------------
                  HEADER, FOOTER, MAIN
              ------------------------------------- */
              .main {
                background: #ffffff;
                border-radius: 3px;
                width: 100%; 
              }
        
              .wrapper {
                box-sizing: border-box;
                padding: 20px; 
              }
        
              .content-block {
                padding-bottom: 10px;
                padding-top: 10px;
              }
        
              .footer {
                clear: both;
                margin-top: 10px;
                text-align: center;
                width: 100%; 
              }
                .footer td,
                .footer p,
                .footer span,
                .footer a {
                  color: #999999;
                  font-size: 12px;
                  text-align: center; 
              }
        
              /* -------------------------------------
                  TYPOGRAPHY
              ------------------------------------- */
              h1,
              h2,
              h3,
              h4 {
                color: #000000;
                font-family: sans-serif;
                font-weight: 400;
                line-height: 1.4;
                margin: 0;
                margin-bottom: 30px; 
              }
        
              h1 {
                font-size: 35px;
                font-weight: 300;
                text-align: center;
                text-transform: capitalize; 
              }
        
              p,
              ul,
              ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                margin-bottom: 15px; 
              }
                p li,
                ul li,
                ol li {
                  list-style-position: inside;
                  margin-left: 5px; 
              }
        
              a {
                color: #3498db;
                text-decoration: underline; 
              }
        
              /* -------------------------------------
                  BUTTONS
              ------------------------------------- */
              .btn {
                box-sizing: border-box;
                width: 100%; }
                .btn > tbody > tr > td {
                  padding-bottom: 15px; }
                .btn table {
                  width: auto; 
              }
                .btn table td {
                  background-color: #ffffff;
                  border-radius: 5px;
                  text-align: center; 
              }
                .btn a {
                  background-color: #ffffff;
                  border: solid 1px #3498db;
                  border-radius: 5px;
                  box-sizing: border-box;
                  color: #3498db;
                  cursor: pointer;
                  display: inline-block;
                  font-size: 14px;
                  font-weight: bold;
                  margin: 0;
                  padding: 12px 25px;
                  text-decoration: none;
                  text-transform: capitalize; 
              }
        
              .btn-primary table td {
                background-color: #3498db; 
              }
        
              .btn-primary a {
                background-color: #3498db;
                border-color: #3498db;
                color: #ffffff; 
              }
        
              /* -------------------------------------
                  OTHER STYLES THAT MIGHT BE USEFUL
              ------------------------------------- */
              .last {
                margin-bottom: 0; 
              }
        
              .first {
                margin-top: 0; 
              }
        
              .align-center {
                text-align: center; 
              }
        
              .align-right {
                text-align: right; 
              }
        
              .align-left {
                text-align: left; 
              }
        
              .clear {
                clear: both; 
              }
        
              .mt0 {
                margin-top: 0; 
              }
        
              .mb0 {
                margin-bottom: 0; 
              }
        
              .preheader {
                color: transparent;
                display: none;
                height: 0;
                max-height: 0;
                max-width: 0;
                opacity: 0;
                overflow: hidden;
                mso-hide: all;
                visibility: hidden;
                width: 0; 
              }
        
              .powered-by a {
                text-decoration: none; 
              }
        
              hr {
                border: 0;
                border-bottom: 1px solid #f6f6f6;
                margin: 20px 0; 
              }
        
              /* -------------------------------------
                  RESPONSIVE AND MOBILE FRIENDLY STYLES
              ------------------------------------- */
              @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                  font-size: 28px !important;
                  margin-bottom: 10px !important; 
                }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                  font-size: 16px !important; 
                }
                table[class=body] .wrapper,
                table[class=body] .article {
                  padding: 10px !important; 
                }
                table[class=body] .content {
                  padding: 0 !important; 
                }
                table[class=body] .container {
                  padding: 0 !important;
                  width: 100% !important; 
                }
                table[class=body] .main {
                  border-left-width: 0 !important;
                  border-radius: 0 !important;
                  border-right-width: 0 !important; 
                }
                table[class=body] .btn table {
                  width: 100% !important; 
                }
                table[class=body] .btn a {
                  width: 100% !important; 
                }
                table[class=body] .img-responsive {
                  height: auto !important;
                  max-width: 100% !important;
                  width: auto !important; 
                }
              }
        
              /* -------------------------------------
                  PRESERVE THESE STYLES IN THE HEAD
              ------------------------------------- */
              @media all {
                .ExternalClass {
                  width: 100%; 
                }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                  line-height: 100%; 
                }
                .apple-link a {
                  color: inherit !important;
                  font-family: inherit !important;
                  font-size: inherit !important;
                  font-weight: inherit !important;
                  line-height: inherit !important;
                  text-decoration: none !important; 
                }
                #MessageViewBody a {
                  color: inherit;
                  text-decoration: none;
                  font-size: inherit;
                  font-family: inherit;
                  font-weight: inherit;
                  line-height: inherit;
                }
                .btn-primary table td:hover {
                  background-color: #34495e !important; 
                }
                .btn-primary a:hover {
                  background-color: #34495e !important;
                  border-color: #34495e !important; 
                } 
              }
        
            </style>
          </head>
          <body class="">
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
              <tr>
                <td>&nbsp;</td>
                <td class="container">
                  <div class="content">
        
                    <!-- START CENTERED WHITE CONTAINER -->
                    <table role="presentation" class="main">
        
                      <!-- START MAIN CONTENT AREA -->
                      <tr>
                        <td class="wrapper">
                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                              <td>
                                $body
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
        
                    <!-- END MAIN CONTENT AREA -->
                    </table>
                    <!-- END CENTERED WHITE CONTAINER -->
        
                    <!-- START FOOTER -->
                    <div class="footer">
                      <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td class="content-block">
                            <br> Don't like these emails? <a href="http://i.imgur.com/CScmqnj.gif">Unsubscribe</a>.
                          </td>
                        </tr>
                        <tr>
                          <td class="content-block powered-by">
                            Powered by <a href="http://htmlemail.io">HTMLemail</a>.
                          </td>
                        </tr>
                      </table>
                    </div>
                    <!-- END FOOTER -->
        
                  </div>
                </td>
                <td>&nbsp;</td>
              </tr>
            </table>
          </body>
        </html>

        term;

    return $email;
  }
}
