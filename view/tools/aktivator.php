<?php
$view = ViewManager::getInstance();

?>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Simulasi Pembayaran
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="metode_pembayaran">Metode Pembayaran</label>
                        <select name="metode_pembayaran" id="metode_pembayaran" class="form-control">
                            <option value="bca">BCA</option>
                            <option value="ovo">OVO</option>
                            <option value="gopay">Gopay</option>
                            <option value="bank_transfer">Bank Transfer</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="va">Virtual Account</label>
                        <input type="text" name="va" id="va" class="form-control">
                    </div>
                    <div class="form-group">
                        <button id="cekPembayaran" type="button" class="btn btn-primary">Cek</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalPembayaran" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Cek Pembayaran</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body" id="isiBody"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeInvoice">Close</button>
                <button type="button" class="btn btn-primary" id="bayarInvoice">Bayar</button>
            </div>
        </div>
    </div>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script>
    var customer = {};
    $('#cekPembayaran').on('click', function() {
        $('#closeInvoice').removeAttr('disabled');
        var va = $('#va').val();
        if (va.trim() === "") return;

        $.ajax({
            url: "<?= route('Home', 'cekAktivasi'); ?>",
            method: 'POST',
            dataType: 'json',
            data: {
                va: va,
                metode_pembayaran: $('#metode_pembayaran option:selected').val(),
            },
            success: function(result) {
                var body = $('#isiBody');
                body.empty();
                if (result) {

                    customer = {
                        email_notifikasi: result.email_notifikasi,
                        invoice: result.invoice
                    }

                    var status = "";
                    var dataPemesan = JSON.parse(result.data_pemesan);

                    console.log({
                        result,
                        dataPemesan
                    });

                    if (result.status == 0) {
                        status = "Menunggu Pembayaran";
                        $('#bayarInvoice').removeAttr('disabled');
                    } else if (result.status == 1) {
                        status = "Pembayaran berhasil";
                        $('#bayarInvoice').attr('disabled', 'disabled');
                    } else {
                        status = "Pembayaran Expired";
                        $('#bayarInvoice').attr('disabled', 'disabled');
                    }

                    var transaction_time = "-";
                    if (result.transaction_time !== null) {
                        transaction_time = result.transaction_time;
                    }

                    var total = parseFloat(result.total) + parseFloat(result.fee);

                    var pemesan = '';

                    var table = `
                    <table class="table table-bordered table-sm">
                        <tr>
                            <td>Invoice</td>
                            <td style="width: 1%">:</td>
                            <td>${result.invoice}</td>
                        </tr>
                        <tr>
                            <td>Virtual Account</td>
                            <td style="width: 1%">:</td>
                            <td>${result.virtual_account}</td>
                        </tr>
                        <tr>
                            <td>Jumlah Tiket</td>
                            <td style="width: 1%">:</td>
                            <td>${result.jumlah_tiket}</td>
                        </tr>
                        <tr>
                            <td>Metode Pembayaran</td>
                            <td style="width: 1%">:</td>
                            <td>${result.metode_pembayaran}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td style="width: 1%">:</td>
                            <td>${status}</td>
                        </tr>
                        <tr>
                            <td>Jumlah yang harus dibayar</td>
                            <td style="width: 1%">:</td>
                            <td>${formatter.format(total)}</td>
                        </tr>
                        <tr>
                            <td>Waktu Transaksi</td>
                            <td style="width: 1%">:</td>
                            <td>${transaction_time}</td>
                        </tr>
                        <tr>
                            <td>Email Notifikasi</td>
                            <td style="width: 1%">:</td>
                            <td>${result.email_notifikasi}</td>
                        </tr>
                    </table>
                    `;

                    body.append(table);
                    $('#modalPembayaran').modal('show');
                } else {
                    alert('Virtual Account tidak ditemukan');
                }
            },
            error: function(err) {
                console.log(err);
            }
        });
    });

    $(document).ready(function() {
        console.log("TEST YA");
    });

    $('#bayarInvoice').on('click', function() {
        var va = $('#va').val();
        $.ajax({
            url: "<?= route('Home', 'aktivasiPayment'); ?>",
            method: 'POST',
            dataType: 'json',
            data: {
                va: va,
                metode_pembayaran: $('#metode_pembayaran option:selected').val(),
                email_notifikasi: customer.email_notifikasi,
                invoice: customer.invoice,
            },
            beforeSend: function() {
                // console.log({
                //     va: va,
                //     metode_pembayaran: $('#metode_pembayaran option:selected').val(),
                //     email_notifikasi: customer.email_notifikasi,
                //     invoice: customer.invoice,
                // })
                $('#bayarInvoice').attr('disabled', 'disabled');
                $('#closeInvoice').attr('disabled', 'disabled');
            },
            success: function(result, textStatus, xhr) {
                console.log("BAYAR INVOICE", {
                    result,
                    textStatus,
                    xhr
                });
                if (result) {
                    $('#modalPembayaran').modal('hide');
                    alert("Pembayaran Sukses");
                } else {
                    alert("Oops.. Something wen wrong. Please try again !");
                }
            },
            error: function(err) {
                console.log("ERROR", err.message);
            }
        });
    });

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
        maximumFractionDigits: 0,
        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
</script>
<?php $view->moveToDefaultFragment(); ?>