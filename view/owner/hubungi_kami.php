<?php
$view = ViewManager::getInstance();

?>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Hubungi Kami
                </div>
                <div class="card-body">
                    <form method="get">
                        <div class="form-group">
                            <label for="nama_lengkap">Nama Lengkap</label>
                            <input type="text" class="form-control" id="nama_lengkap" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-group">
                            <label for="email_address">Email address</label>
                            <input type="email" class="form-control" id="email_address" placeholder="name@example.com">
                        </div>
                        <div class="form-group">
                            <label for="keluhan">Pertanyaan</label>
                            <select class="form-control" id="keluhan">
                                <option value="pelaporan">Pelaporan</option>
                                <option value="pembelian_tiket">Pembelian Tiket</option>
                                <option value="penjualan_tiket">Penjualan Tiket</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pesan">Pesan</label>
                            <textarea class="form-control" id="pesan" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Kirim</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script>

</script>
<?php $view->moveToDefaultFragment(); ?>