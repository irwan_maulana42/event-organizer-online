<?php
$view = ViewManager::getInstance();
$invoice = $view->getVariable('invoice');

$method = "";

if ($invoice['metode_pembayaran'] === 'bca') {
    $method = "Virtual Account BCA";
} else if ($invoice['metode_pembayaran'] === 'ovo') {
    $method = "OVO";
} else if ($invoice['metode_pembayaran'] === 'gopay') {
    $method = "Gopay";
} else {
    $method = "Bank Transfer";
}

$total = ((float) $invoice['total'] + (float) $invoice['fee']);
?>
<div class="container-fluid">
    <div class="card m-3">
        <div class="card-body">
            <h3>Terima kasih atas pesanan kamu</h3>
            <br>
            <div class="row">
                <div class="col-md-7">
                    <?php if ((int) $invoice['status'] === 0) : ?>
                        <p>Kamu melakukan pembayaran via <b><?= $method; ?></b>. Silakan menyelesaikan pembayaran dalam jangka waktu berikut.</p>
                        <div class="alert alert-warning my-5" role="alert">
                            <input type="hidden" name="timer" id="timer" value="<?= $invoice['created_at']; ?>">
                            <h4 class="alert-heading" id="timerCount"></h4>
                            <hr>
                            <p>Jika melebihi batas waktu yang ditentukan, pemesanan tiket akan dibatalkan.</p>
                        </div>
                    <?php endif; ?>
                    <table class="table table-bordered">
                        <tr>
                            <th>Nomor Virtual Account</th>
                            <td><?= $invoice['virtual_account']; ?></td>
                        </tr>
                        <tr>
                            <th>Akun Pemilik Bank</th>
                            <td>PT Event Tiket Maju Mundur</td>
                        </tr>
                        <tr>
                            <th>Jumlah Transfer</th>
                            <td>IDR <?= number_format($total, 0, '.', ','); ?></td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <?php
                                if ($invoice['status'] == 0) {
                                    echo "Menunggu Pembayaran";
                                } else if ($invoice['status'] == 1) {
                                    echo "Pembayaran Berhasil";
                                } else {
                                    echo "Pembayaran Expired";
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Transaction Time</th>
                            <td><?= (empty($invoice['transaction_time'])) ? '-' : $invoice['transaction_time']; ?></td>
                        </tr>
                        <tr>
                            <th>Invoice Code</th>
                            <td><?= $_GET['invoice']; ?></td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-5"></div>
            </div>
        </div>
    </div>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script>
    var isExpired = false;

    function diffDates(end, start = '') {
        var now = moment();
        var end = moment(end);

        var duration = moment.duration(end.diff(now));

        //Get Days and subtract from duration
        var days = duration.asDays();
        duration.subtract(moment.duration(days, 'days'));

        //Get hours and subtract from duration
        var hours = duration.hours();
        duration.subtract(moment.duration(hours, 'hours'));

        //Get Minutes and subtract from duration
        var minutes = duration.minutes();
        duration.subtract(moment.duration(minutes, 'minutes'));

        //Get seconds
        var seconds = duration.seconds();
        // console.log({
        //     now: now.format('YYYY-MM-DD HH:mm:ss'),
        //     end: end.format('YYYY-MM-DD HH:mm:ss'),
        //     duration
        // });
        // return `${Math.round(days)}d, ${hours}h ${minutes}m ${seconds}s`;
        return `${hours}:${minutes}:${seconds}`;
    }

    var interval = setInterval(() => {
        if (!isExpired) {
            var data = $('#timer').val();
            var getTimer = diffDates(moment(data).add(3, 'hours'));
            if (getTimer === "0:0:0") isExpired = true;
            $('#timerCount').html(getTimer);
        } else {
            window.location.href = "<?= route('home', 'index'); ?>"
            window.clearInterval(interval);
        }
    }, 1000);
</script>
<?php $view->moveToDefaultFragment(); ?>