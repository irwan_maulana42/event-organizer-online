<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404</title>
    <style>
        .full-screen-size-fixed-container {
            height: 100vh;
            width: 100%;
            position: fixed;
            padding-bottom: 80px;
        }
    </style>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css">
    <!------ Include the above in your HEAD tag ---------->

</head>

<body>
    <div class="full-screen-size-fixed-container d-flex justify-content-center align-items-center">
        <div class="p-3 text-center shadow">
            <h1 class="display-1 font-weight-bold ">404</h1>
            <h4 class="text-muted">Page Not Found</h4>
            <p class="">Oops... webpage not found please try to refresh.</p>
            <a class="btn btn-dark" href="index.php" role="button"><i class="fas fa-house-user mr-2"></i> Back to Dashboard</a>
        </div>
    </div>
</body>

</html>