<?php
$view = ViewManager::getInstance();
$events = $view->getVariable('events');
$featured = $view->getVariable('featured');
$carousel = $view->getVariable('carousel');
$top_selling = $view->getVariable('top_selling');


?>
<section class="mb-3">
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php
            if (!empty($carousel)) :
                foreach ($carousel as $keyCarousel => $valueCarousel) : ?>
                    <li data-target="#carouselExampleCaptions" data-slide-to="<?= $keyCarousel; ?>" <?= ($keyCarousel === 0) ? 'class="active"' : '' ?>></li>
            <?php
                endforeach;
            endif; ?>
        </ol>
        <div class="carousel-inner">
            <?php
            if (!empty($carousel)) :
                foreach ($carousel as $keyCarousel => $valueCarousel) : ?>
                    <div class="carousel-item <?= ($keyCarousel === 0) ? 'active' : '' ?>">
                        <img src="<?= $valueCarousel['image_url']; ?>" style="height: 60vh; margin-left: auto;width: 100%;object-fit: cover;margin-right: auto;">
                        <div class="carousel-caption d-none d-md-block" style="background: rgb(88 88 88 / 75%);margin: 0.5em;">
                            <h5><?= $valueCarousel['title']; ?></h5>
                            <p><?= $valueCarousel['desc']; ?></p>
                        </div>
                    </div>
            <?php
                endforeach;
            endif; ?>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<div class="container mb-3">
    <div class="row">
        <div class="col-md-6">
            <h4>Featured Event</h4>
            <div class="card">
                <?php
                $imageFeatured = json_decode(@$featured['banner_event'], true);
                ?>
                <img src="<?= @$imageFeatured['url']; ?>" class="card-img-top" alt="No Image" style="height: 350px;object-fit:cover;">
                <div class="card-body">
                    <h5 class="card-title"><?= @$featured['nama_event']; ?></h5>
                    <p class="card-text"><?= @$featured['deskripsi_tiket']; ?></p>
                    <a href="<?= route('Event', 'index', 'event=' . $featured['url']) ?>" class="btn btn-primary">BELI TIKET</a>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <h4>Top Selling</h4>
            <div class="">
                <div class="">
                    <ul class="list-unstyled mb-0">
                        <?php foreach ($top_selling as $event) :
                            $image = json_decode($event['banner_event'], true);
                        ?>
                            <li class="media my-2" style="border: 1px solid #ddd; padding: 0.75em;border-radius: 0.25em;">
                                <img src="<?= $image['url']; ?>" style="width: 100px;object-fit: cover;height: 100px;" class="mr-3" alt="No Image" style="width: 100px;">
                                <div class="media-body">
                                    <div class="d-flex justify-content-between">
                                        <div>
                                            <h6 class="mt-0 mb-1"><?= $event['nama_event']; ?></h6>
                                            <div>
                                                <table style="font-size: 14px !important;">
                                                    <tr>
                                                        <td><a href="javascript:void(0)"><i class="far fa-user mx-1 text-primary"></i><span style="font-weight: 600;"><?= $event['nama_organize']; ?></span></a></td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="far fa-calendar-alt mx-1 text-primary"></i><?= date('d F Y', strtotime($event['date'])); ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-map-marker-alt mx-1 text-primary"></i><?= $event['lokasi'] ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="d-flex align-items-center">
                                            <a href="<?= route('Event', 'index', 'event=' . $event['url']) ?>" class="btn btn-primary">BELI TIKET</a>
                                        </div>
                                    </div>
                                </div>

                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="mt-5">
        <h4>Discover</h4>
        <div class="row">
            <?php
            if (!empty($events)) :
                foreach ($events as $event) :
                    $bannerImage = json_decode($event['banner_event'], true);
            ?>
                    <div class="col-md-3 my-2">
                        <div class="card">
                            <img src="<?= $bannerImage['url']; ?>" class="card-img-top" alt="Image">
                            <div class="card-body">
                                <small class="mb-2 mt-1 text-muted"><?= $event['penyelenggara'] ?></small>
                                <h5 class="card-title"><?= $event['nama_event']; ?></h5>
                                <h6 class="card-subtitle mb-2 text-muted"><?= date('d M Y', strtotime($event['date'])) ?></h6>
                                <p><?= ($event['jenis_tiket'] == 1) ? "Rp." . number_format($event['harga_tiket'], 0, ',', '.') : 'Gratis'; ?></p>
                                <a href="<?= route('Event', 'index', 'event=' . $event['url']) ?>" class="btn btn-primary btn-block mt-1">BELI TIKET</a>
                            </div>
                        </div>
                    </div>
                <?php
                endforeach;
            else : ?>
                <div class="col-md-12 my-2">
                    <div class="card">
                        <div class="card-body">
                            <p class="text-center m-0 font-weight-bold">Data tidak ditemukan !</p>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>