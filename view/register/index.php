<!-- <div class="container">
    <div class="row justify-content-center mt-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Daftar
                </div>
                <div class="card-body">
                    <form method="POST" action="index.php?controller=users&action=register">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" name="email" id="exampleInputEmail1" required>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" class="form-control" id="exampleInputPassword1" name="password" required>
                        </div>
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                            <label class="form-check-label" for="exampleCheck1">Dengan mendaftar, saya menyetujui syarat dan ketentuan serta kebijakan privasi.</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Daftar</button>
                    </form>
                    <small>Sudah punya akun? <a href="index.php?controller=users&action=register">Masuk</a></small>
                </div>
            </div>
        </div>
    </div>
</div> -->

<?php
$view = ViewManager::getInstance();
$view->moveToFragment("css"); ?>
<style>
    .card {
        border: 1px solid #ddd;
    }

    .card-login {
        margin-top: 130px;
        padding: 18px;
        max-width: 30rem;
    }

    .card-header {
        color: #fff;
        /*background: #ff0000;*/
        font-family: sans-serif;
        font-size: 20px;
        font-weight: 600 !important;
        margin-top: 10px;
        border-bottom: 0;
    }

    .input-group-prepend span {
        width: 50px;
        background-color: #eb7900;
        color: #fff;
        border: 0 !important;
    }

    input:focus {
        outline: 0 0 0 0 !important;
        box-shadow: 0 0 0 0 !important;
    }

    .login_btn {
        width: 130px;
    }

    .login_btn:hover {
        color: #fff;
        background-color: #6c19ff;
        border-color: #6c19ff;
    }

    .btn-outline-danger {
        color: #fff;
        font-size: 18px;
        background-color: #6c19ff;
        background-image: none;
        border-color: #6c19ff;
    }

    .form-control {
        display: block;
        width: 100%;
        height: calc(2.25rem + 2px);
        padding: 0.375rem 0.75rem;
        font-size: 1.2rem;
        line-height: 1.6;
        color: #28a745;
        background-color: transparent;
        background-clip: padding-box;
        border: 1px solid #28a745;
        border-radius: 0;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    }

    .input-group-text {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-align: center;
        align-items: center;
        padding: 0.375rem 0.75rem;
        margin-bottom: 0;
        font-size: 1.5rem;
        font-weight: 700;
        line-height: 1.6;
        color: #eb7900;
        text-align: center;
        white-space: nowrap;
        background-color: #eb7900;
        border: 1px solid #ced4da;
        border-radius: 0;
    }
</style>
<?php $view->moveToDefaultFragment(); ?>
<div class="container">
    <div class="card card-login mx-auto text-center bg-dark" style='margin-top: 10px !important;'>
        <div class="card-header mx-auto bg-dark">
            <a href="index.php">
                <img src="https://img.icons8.com/color-glass/100/000000/two-tickets.png" class="" style="height: 100px;width: 100px;" alt="">
            </a>
            <br>
            <span class="lead text-white" style="font-size: 2em;">EVENT ORGANIZER</span>
            <br>
            <span class="logo_title mt-5"> Register Member </span>
        </div>
        <div class="card-body">
            <form class="" method="POST" action="index.php?controller=Users&action=register">
                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input type="email" name="email" class="form-control bg-white" placeholder="Email" required>
                </div>

                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input type="password" name="password" class="form-control bg-white" placeholder="Password" required>
                </div>
                <div class="form-group form-check text-white text-left">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                    <label class="form-check-label" for="exampleCheck1">Dengan mendaftar, saya menyetujui syarat dan ketentuan serta kebijakan privasi.</label>
                </div>

                <div class="form-group d-flex justify-content-end">
                    <input type="submit" name="btn" value="Register" class="btn btn-outline-danger login_btn">
                </div>
                <div class="form-group text-white mt-4 d-flex justify-content-center">
                    <h6>Sudah punya akun? <a href="index.php?controller=Users&action=login" class="text-danger">Login</a></h6>
                </div>
            </form>
        </div>
    </div>
</div>