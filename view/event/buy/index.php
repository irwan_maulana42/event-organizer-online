<?php
$view = ViewManager::getInstance();
$event = $view->getVariable('event');
$temp = $view->getVariable('temp');
$user = $view->getVariable('detail_user');
$pembeli = $view->getVariable('pembeli');

$dataPemesan = json_decode($temp['data'], true);
$fee = ((int) $event['jenis_tiket'] !== 2) ? 3000 : 0;
$subTotal = ((float) $dataPemesan['harga_tiket'] * (float) $dataPemesan['jumlah_tiket']);
$total = $fee + $subTotal;

?>
<div class="container my-5">
    <form method="POST" action="<?= route('Event', 'checkout'); ?>">
        <input type="hidden" name="metode_pembayaran" value="gopay" id="method_payment">
        <input type="hidden" name="jumlah_tiket" value="<?= $dataPemesan['jumlah_tiket']; ?>">
        <input type="hidden" name="total" value="<?= $subTotal; ?>">
        <input type="hidden" name="event_id" value="<?= $event['id']; ?>">
        <input type="hidden" name="fee" value="<?= $fee; ?>">
        <input type="hidden" name="url" value="<?= $event['url']; ?>">
        <input type="hidden" name="timer" value="<?= $temp['expired']; ?>" id="timer">

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-3">
                    <div class="card-body">
                        <h3 class="card-title"><?= $event['nama_event']; ?></h3>
                        <div>
                            <span class="badge badge-light"><?= $event['nama_kategori']; ?></span>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <h6 style="font-weight: bold;">Diselenggarakan oleh</h6>
                                <div class="media" style="align-items: center;">
                                    <?php
                                    $profileImage = "https://img.icons8.com/bubbles/100/000000/user.png";
                                    if (isset($event['profile_image'])) {
                                        $profileImage = json_decode($event['profile_image'], true)['url'];
                                    }
                                    ?>
                                    <img src="<?= $profileImage ?>" style="height: 75px;width: 75px; object-fit: cover;border-radius:.50em;" />
                                    <div class="media-body">
                                        <h5 class="ml-2"><?= $event['nama_organize']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h6 style="font-weight: bold;">Tanggal & Waktu</h6>
                                <div style="display: flex; align-items:center" class="m-2">
                                    <i class="far fa-calendar m-1" style="font-size: 20px;"></i>
                                    <?= $event['date']; ?>
                                </div>
                                <div style="display: flex; align-items:center" class="m-2">
                                    <i class="far fa-clock m-1" style="font-size: 20px;"></i>
                                    <?= $event['time']; ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h6 style="font-weight: bold;">Lokasi</h6>
                                <div style="display: flex; align-items:center" class="m-2">
                                    <i class="fas fa-map-marker-alt m-1" style="font-size: 20px;"></i>
                                    <?= $event['lokasi']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="alert alert-warning my-5" role="alert">
                    <h4 class="alert-heading" id="timerCount"></h4>
                    <hr>
                    <p>Selesaikan pembayaran kamu segera!</p>
                </div>

                <div class="alert alert-danger my-5" role="alert">
                    Setelah transaksi pembayaran kamu berhasil, kamu akan mendapatkan info terkait URL Streaming di e-voucher (dikirim via email).
                </div>

                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <?php
                            $email = "";
                            if (!empty($user)) $email = $user['email'];
                            ?>
                            <label for="">Email Notifikasi Pembayaran</label>
                            <input type="email" class="form-control" name="email_notifikasi" id="email_notifikasi" placeholder="Email Notifikasi Pembayaran" value="<?= $email; ?>" <?= (!empty($user)) ? "readonly" : "" ?> required>
                        </div>
                    </div>
                </div>

                <div class="card my-5">
                    <div class="card-body">
                        <h4>Informasi Tambahan</h4>
                        <div class="accordion" id="accordionExample">
                            <?php
                            $i = 0;
                            for ($i = 0; $i < $dataPemesan['jumlah_tiket']; $i++) : ?>
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse<?= $i; ?>">
                                                PEMESAN <?= $i + 1 ?> | TICKET: <?= $event['nama_tiket']; ?>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapse<?= $i; ?>" class="collapse" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <?php foreach (json_decode($event['atur_data_pembeli'], true) as $key => $valueDataPembeli) : ?>
                                                <?php
                                                $findData = [];
                                                if ($valueDataPembeli === 'jenis_kelamin') {
                                                    $string = "";
                                                    $options = "";
                                                    $search = array_search($valueDataPembeli, array_column($pembeli, 'name'));
                                                    $findData = $pembeli[$search];
                                                    foreach (json_decode($findData['options'], true) as $option) {
                                                        $options .= "<option value='" . $option['value'] . "'>" . $option['text'] . "</option>";
                                                    }
                                                    $string .= "
                                                    <div class='form-group'>
                                                        <label for='" . $findData['name'] . "'>" . $findData['label'] . "</label>
                                                        <select class='form-control' name='" . $findData['name'] . "[]' id='" . $findData['name'] . "'>" . $options . "</select>
                                                    </div>";

                                                    echo $string;
                                                } else {
                                                    $string = "";
                                                    $search = array_search($valueDataPembeli, array_column($pembeli, 'name'));
                                                    $findData = $pembeli[$search];
                                                    $string .= "<div class='form-group'>
                                                                    <label for='" . $findData['name'] . "'>" . $findData['label'] . "</label>
                                                                    <input type='" . $findData['type'] . "' class='form-control' name='" . $findData['name'] . "[]' id='" . $findData['name'] . "' placeholder='" . $findData['label'] . "' required>
                                                                </div>";
                                                    echo $string;
                                                }
                                                ?>
                                            <?php endforeach; ?>

                                        </div>
                                    </div>
                                </div>

                            <?php endfor; ?>
                        </div>
                    </div>
                </div>

                <div class="card my-5" style="display: <?= ((int) $event['jenis_tiket'] !== 1) ? "none" : "block" ?>;">
                    <div class="card-body">
                        <h4>Metode Pembayaran</h4>
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="pills-gopay-tab" data-toggle="pill" href="#pills-gopay" onclick="changePayment('gopay')" role="tab" aria-controls="pills-gopay" aria-selected="true">
                                    <strong>GoPay</strong>
                                </a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-bca-tab" data-toggle="pill" href="#pills-bca" role="tab" onclick="changePayment('bca')" aria-controls="pills-bca" aria-selected="false">
                                    <strong>BCA</strong>
                                </a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-ovo-tab" data-toggle="pill" href="#pills-ovo" role="tab" onclick="changePayment('ovo')" aria-controls="pills-ovo" aria-selected="false">
                                    <strong>OVO</strong>
                                </a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="pills-banktransfer-tab" data-toggle="pill" href="#pills-banktransfer" role="tab" onclick="changePayment('bank_transfer')" aria-controls="pills-banktransfer" aria-selected="false">
                                    <strong>Bank Transfer</strong>
                                </a>
                            </li>
                        </ul>
                        <hr>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-gopay" role="tabpanel" aria-labelledby="pills-gopay-tab">
                                <h5>GoPay</h5>
                                <p>Layanan pembayaran uang elektronik melalui aplikasi Gojek.</p>
                            </div>
                            <div class="tab-pane fade" id="pills-bca" role="tabpanel" aria-labelledby="pills-bca-tab">
                                <h5>Virtual Account BCA</h5>
                                <p>Layanan pembayaran transfer dari rekening bank BCA melalui ATM, mobile, atau internet banking.</p>
                            </div>
                            <div class="tab-pane fade" id="pills-ovo" role="tabpanel" aria-labelledby="pills-ovo-tab">
                                <h5>OVO</h5>
                                <p>Layanan pembayaran uang elektronik melalui aplikasi OVO.</p>
                            </div>
                            <div class="tab-pane fade" id="pills-banktransfer" role="tabpanel" aria-labelledby="pills-banktransfer-tab">
                                <h5>Bank Transfer (Virtual Account)</h5>
                                <p>Layanan pembayaran transfer dari rekening lokal bank apapun melalui ATM Bersama / Prima / Alto.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card my-5">
                    <div class="card-body">
                        <h4>Pembelian Kamu</h4>
                        <table style="width: 100%;" class="table table-borderless">
                            <tr>
                                <td class="font-weight-light h5"><?= $event['nama_tiket']; ?> x <?= $dataPemesan['jumlah_tiket']; ?></td>
                                <td class="text-right h5">IDR <?= number_format($subTotal, 0, '.', ','); ?></td>
                            </tr>
                            <tr>
                                <td class="font-weight-normal h5">Convenience fee</td>
                                <td class="text-right h5">IDR <?= number_format($fee, 0, '.', ','); ?></td>
                            </tr>
                            <tr style="border: 1px solid;border-left: 0;border-right: 0;">
                                <td class="font-weight-bold h5">Total Pembayaran</td>
                                <td class="text-right text-danger font-weight-bold h5">IDR <?= number_format($total, 0, '.', ','); ?></td>
                            </tr>
                        </table>
                        <p>Pastikan untuk cek kembali tiket yang akan kamu beli</p>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" required>
                            <label class="form-check-label" for="defaultCheck1">
                                Syarat & ketentuan berlaku.
                            </label>
                        </div>

                        <button class="btn btn-primary btn-lg my-2" type="submit">Bayar</button>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script>
    var isExpired = false;

    function diffDates(end, start = '') {
        var now = moment();
        var end = moment(end);

        var duration = moment.duration(end.diff(now));

        //Get Days and subtract from duration
        var days = duration.asDays();
        duration.subtract(moment.duration(days, 'days'));

        //Get hours and subtract from duration
        var hours = duration.hours();
        duration.subtract(moment.duration(hours, 'hours'));

        //Get Minutes and subtract from duration
        var minutes = duration.minutes();
        duration.subtract(moment.duration(minutes, 'minutes'));

        //Get seconds
        var seconds = duration.seconds();
        // console.log({
        //     now: now.format('YYYY-MM-DD HH:mm:ss'),
        //     end: end.format('YYYY-MM-DD HH:mm:ss'),
        //     duration
        // });
        // return `${Math.round(days)}d, ${hours}h ${minutes}m ${seconds}s`;
        return `${minutes}:${seconds}`;
    }

    var interval = setInterval(() => {
        if (!isExpired) {
            var data = $('#timer').val();
            var getTimer = diffDates(data);
            if (getTimer === "0:0") isExpired = true;
            $('#timerCount').html(getTimer);
        } else {
            window.location.href = "<?= route('event', 'index', 'event=' . $event['url']); ?>"
            window.clearInterval(interval);
        }
    }, 1000);

    var payment = $('#method_payment');

    function changePayment(value) {
        payment.val(value);
    }
</script>
<?php $view->moveToDefaultFragment(); ?>