<?php
$view = ViewManager::getInstance();

$event = $view->getVariable('event');
// dump($event);
?>
<form action="<?= route('Event', 'softBuy'); ?>" method="post" id="formSubmit">
    <input type="hidden" name="order_id" value="<?= $event['url']; ?>">
    <div class="container mb-5">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-3">
                    <?php
                    $bannerImage = json_decode($event['banner_event'], true)['url'];
                    ?>
                    <img style="height: 50vh;object-fit: cover;" src="<?= $bannerImage ?>" class="card-img-top">
                    <div class="card-body">
                        <h3 class="card-title"><?= $event['nama_event']; ?></h3>
                        <div>
                            <span class="badge badge-light"><?= $event['nama_kategori'] ?></span>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <h6 style="font-weight: bold;">Diselenggarakan oleh</h6>
                                <div class="media" style="align-items: center;">
                                    <?php
                                    $profileImage = "https://img.icons8.com/bubbles/100/000000/user.png";
                                    if (!empty($event['profile_image'])) {
                                        $profileImage = json_decode($event['profile_image'], true)['url'];
                                    }
                                    ?>
                                    <img src="<?= $profileImage ?>" style="height: 75px;width: 75px; object-fit: cover;border-radius:.50em;" />
                                    <div class="media-body">
                                        <h5 class="mx-2"><?= $event['nama_organize']; ?></h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h6 style="font-weight: bold;">Tanggal & Waktu</h6>
                                <div style="display: flex; align-items:center" class="m-2">
                                    <i class="far fa-calendar m-1" style="font-size: 20px;"></i>
                                    <?= date('d F Y', strtotime($event['date'])); ?>
                                </div>
                                <div style="display: flex; align-items:center" class="m-2">
                                    <i class="far fa-clock m-1" style="font-size: 20px;"></i>
                                    <?= date('H:i', strtotime($event['time'])); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h6 style="font-weight: bold;">Lokasi</h6>
                                <div style="display: flex; align-items:center" class="m-2">
                                    <i class="fas fa-map-marker-alt m-1" style="font-size: 20px;"></i>
                                    <?= $event['lokasi'] ?>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-3">
                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                    <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Deskripsi Event</a>
                                    <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Kategori Tiket</a>
                                </div>
                            </div>
                            <div class="col-9">
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <div>
                                            <?= base64_decode($event['deskripsi_event']); ?>
                                        </div>
                                        <div>
                                            <h5>SYARAT & KETENTUAN</h5>
                                            <?= base64_decode($event['syarat_ketentuan']); ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                        <div class="ticket">
                                            <div class="stub">
                                                <div class="" style="height: 100%;">
                                                    <div style="position: absolute;top: 25%;padding-right:10px;">
                                                        <div class="top">
                                                            <input type="hidden" name="harga_tiket" id="harga_tiket" value="<?= $event['harga_tiket']; ?>">
                                                            <?php if ($event['jenis_tiket'] !== 2) : ?>
                                                                <span class="num" style="font-size: 26px;font-weight: 600;margin:auto;">
                                                                    IDR <?= number_format($event['harga_tiket'], 0, '.', ','); ?>
                                                                </span>
                                                            <?php else : ?>
                                                                <span class="num" style="font-size: 26px;font-weight: 600;margin:auto;">
                                                                    Gratis
                                                                </span>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div style="margin-top: 5px;">
                                                            <div class="row justify-content-center">
                                                                <div class="col-md-10 col-sm-10 col-lg-10 col-xs-10">
                                                                    <div class="input-group input-group-sm">
                                                                        <div class="input-group-prepend">
                                                                            <button class="input-group-text" id="decrementTicket" type="button"><i class="fas fa-minus"></i></button>
                                                                        </div>
                                                                        <input type="number" class="form-control text-center" id="ticket" name="jumlah_tiket" placeholder="Jumlah" min="0" value="0" readonly>
                                                                        <div class="input-group-append">
                                                                            <button class="input-group-text" id="incrementTicket" type="button"><i class="fas fa-plus"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="check">
                                                <div class="big" id="titleTiket">
                                                    <?= $event['nama_tiket']; ?>
                                                </div>
                                                <div class="info">
                                                    <section>
                                                        <div class="title">Sisa Tiket</div>
                                                        <input type="hidden" id="sisa_tiket" value="<?= $event['sisa_tiket']; ?>">
                                                        <div><?= $event['sisa_tiket']; ?></div>
                                                    </section>
                                                    <!-- <section>
                                                    <div class="title"></div>
                                                    <div>Ampersand</div>
                                                </section>
                                                <section>
                                                    <div class="title">Invite Number</div>
                                                    <div>31415926</div>
                                                </section> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>

    <div style="bottom: 0;
    background-color: #ddd;
    height: 15vh;
    position: fixed;
    width: 100%;">
        <div class="container h-100">
            <div class="row align-items-center h-100">
                <div class="col-md-2">
                    <img src="https://img.icons8.com/officel/100/000000/combi-ticket.png" />
                </div>
                <div class="col-md-6">
                    <h4><?= $event['nama_event'] ?></h4>
                    <p class="m-0" id="chooseTicket">Kamu belum memilih tiket.</p>
                </div>
                <div class="col-md-2">
                    <div class="text-center">
                        <p class="m-0" style="font-size:24px;font-weight:700">Sub-Total</p>
                        <p style="font-size:22px; font-weight:400;" id="total">IDR 0</p>
                    </div>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-lg btn-warning" id="submitTiket" type="button">BELI TIKET</button>
                </div>
            </div>
        </div>
    </div>
</form>


<?php $view->moveToFragment("custom-js"); ?>
<script>
    $('#submitTiket').on('click', function(e) {
        e.preventDefault();
        if (ticket < 1) return alert('Harap isi jumlah tiket yang ingin dibeli.');
        $('#formSubmit').submit();
    });

    var ticket = parseInt($('#ticket').val());
    var hargaTiket = parseInt($('#harga_tiket').val());

    $(document).ready(function() {
        console.log("TEST YA")
    });

    $('#incrementTicket').on('click', function() {
        if (ticket >= 5) return alert("Maksimal tiket yang dapat dipilih 5");
        if (parseInt($('#sisa_tiket').val()) <= parseInt(ticket)) return alert("Tidak boleh melebihi batas sisa tiket");
        ticket++;
        $('#ticket').val(ticket);
        showTiketPass();
        cekTotal();
    });

    $('#decrementTicket').on('click', function() {
        if (ticket === 0) return;
        ticket--;
        $('#ticket').val(ticket);
        showTiketPass();
        cekTotal();
    });

    function showTiketPass() {
        var titleTiket = $('#titleTiket').text();
        if (ticket === 0) {
            $('#chooseTicket').html('Kamu belum memilih tiket.');
        } else {
            $('#chooseTicket').html(`${titleTiket} ${ticket} Tiket`);
        }
    }

    function cekTotal() {
        var total = (ticket * hargaTiket);
        $('#total').html(formatter.format(total));
    }

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
        maximumFractionDigits: 0,
        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });
</script>
<?php $view->moveToDefaultFragment(); ?>