<?php
$view = ViewManager::getInstance();
$eventAktif = $view->getVariable('eventAktif');
$tiketTerjual = $view->getVariable('tiketTerjual');
$totalTransaksi = $view->getVariable('grandTotal');

$dataKategori = $view->getVariable('dataKategori');
$dataOrganizer = $view->getVariable('dataOrganizer');

?>
<h3>Dashboard</h3>
<hr>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Date</label>
                <input type="text" class="form-control" id="date">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Nama Organizer</label>
                <select name="nama_organizer" class="form-control">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Kategori</label>
                <select name="kategori" class="form-control">
                    <option value=""></option>
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <button class="btn btn-primary" onclick="load_data()">Filter</button>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Event Aktif</div>
                <div class="card-body">
                    <h3 id="event_aktif"></h3> Event
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Total Tiket Terjual</div>
                <div class="card-body">
                    <h3 id="tiket_terjual"></h3> Tiket
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Total Transaksi</div>
                <div class="card-body">
                    <h3 id="total_transaksi">Rp </h3> Event
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Statistik Tiket Terjual</div>
                <div style="height: 280px !important;" id="checkMyChartTiketTerjual">
                    <canvas id="chartCount" style="width: 100%;"></canvas>
                </div>
                <div class="card-footer">
                    <button class="btn btn-sm btn-primary" type="button" id="download-pdf-tiket-terjual">
                        Download PDF
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Statistik Transaksi</div>
                <div style="height: 400px !important;" id="checkMyChartTransaksi">
                    <canvas id="myChart" style="width: 100%;height: 400px !important;"></canvas>
                </div>
                <div class="card-footer">
                    <button class="btn btn-sm btn-primary" type="button" id="download-pdf-statistik-transaksi">
                        Download PDF
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered table-sm" id="tableEvents">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Nama Organizer</th>
                                <th>Nama Event</th>
                                <th>Penyelenggara</th>
                                <th>Kategori</th>
                                <th>Jenis Tiket</th>
                                <th>Tanggal</th>
                                <th>Waktu</th>
                                <th>Lokasi</th>
                                <th>Status</th>
                                <th>Nama Tiket</th>
                                <th>Jumlah Tiket</th>
                                <th>Sisa Tiket</th>
                                <th>Harga Tiket</th>
                                <th>Total Transaksi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalDetailTrx" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Detail Transaksi Tiket Terjual</h5>
            </div>
            <div class="modal-body" id="isiBody">
                <table class="table table-sm table-borderless" id="tableDetailEvent" style="width: 100%;"></table>
                <hr>
                <table class="table" id="tableDetailTrx" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Invoice</th>
                            <th>Status</th>
                            <th>Email Notifikasi</th>
                            <th>Jumlah Tiket</th>
                            <th>Metode Pembayaran</th>
                            <th>VA</th>
                            <th>Waktu Transaksi</th>
                            <th>Total</th>
                            <th>Fee</th>
                            <th>Grand Total</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeInvoice">Close</button>
                <!-- <button type="button" class="btn btn-primary" id="bayarInvoice">Bayar</button> -->
            </div>
        </div>
    </div>
</div>
<?php $view->moveToFragment("custom-js"); ?>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script>
    var chartStatistikTransaksi,
        chartStatistikTiketTerjual;

    var params = {
        start_date: moment().startOf('year').format('YYYY-MM-DD'),
        end_date: moment().format('YYYY-MM-DD'),
        organizer_id: '',
        kategori_id: '',
    }

    var dataOrganizer = JSON.parse(atob("<?= $dataOrganizer ?>")) || [];
    var dataKategori = JSON.parse(atob("<?= $dataKategori ?>")) || [];

    var dataStatistikTransaksi = [];
    var dataStatistikTiket = [];

    $(document).ready(function() {
        $('[name="nama_organizer"]').select2({
            placeholder: {
                id: '', // the value of the option
                text: 'Select',
            },
            allowClear: true,
            data: dataOrganizer.map((item) => {
                return {
                    id: item.id,
                    text: item.nama_organizer,
                }
            })
        }).change(function() {
            params.organizer_id = $(this).val();
        });

        $('[name="kategori"]').select2({
            placeholder: {
                id: '',
                text: 'Select',
            },
            allowClear: true,
            data: dataKategori.map((item) => {
                return {
                    id: item.id,
                    text: item.nama,
                }
            })

        }).change(function() {
            params.kategori_id = $(this).val();
        });

        $("#date").flatpickr({
            defaultDate: [new Date(), (moment().startOf('year').unix() * 1000)],
            mode: "range",
            dateFormat: "d M Y",
            onChange: function(selectedDates, dateStr, instance) {
                if (selectedDates.length > 0) {
                    params.start_date = moment(selectedDates[0]).format('YYYY-MM-DD');
                    params.end_date = moment(selectedDates[1]).format('YYYY-MM-DD');
                } else {
                    params.start_date = moment().startOf('year').format('YYYY-MM-DD'),
                        params.end_date = moment().endOf('year').format('YYYY-MM-DD');
                }
            }
        })
    })

    function load_chart_bar(data = []) {
        let options = {
            type: 'bar',
            data: {
                labels: data.map((item) => item.month_name),
                datasets: [{
                    label: 'Total',
                    data: data.map((item) => item.total),
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.8)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                },
                maintainAspectRatio: true,
                responsive: true,
            }
        };

        if (typeof chartStatistikTransaksi !== 'undefined') {
            chartStatistikTransaksi.destroy();
            $('#checkMyChartTransaksi').empty();
            $('#checkMyChartTransaksi').html('<canvas id="myChart" style="width: 100%;height: 400px !important;"></canvas>');
            chartStatistikTransaksi = new Chart(document.getElementById('myChart').getContext('2d'), options);
        } else {
            chartStatistikTransaksi = new Chart(document.getElementById('myChart').getContext('2d'), options);
        }
    }

    function load_chart_circle(data = {}) {
        let options = {
            type: 'doughnut',
            data: {
                labels: [
                    'Terjual',
                    'Sisa',
                ],
                datasets: [{
                    label: 'Statistik Tiket Terjual',
                    data: [data?.terjual || 0, data?.sisa_tiket || 0],
                    backgroundColor: [
                        'rgb(255, 99, 132)',
                        'rgb(54, 162, 235)',
                    ],
                    hoverOffset: 4
                }]
            },
            options: {
                maintainAspectRatio: true,
                responsive: true,
            }
        };
        if (typeof chartStatistikTiketTerjual !== 'undefined') {
            chartStatistikTiketTerjual.destroy();
            $('#checkMyChartTiketTerjual').empty();
            $('#checkMyChartTiketTerjual').html('<canvas id="chartCount" style="width: 100%;height: 280px !important;"></canvas>');
            chartStatistikTiketTerjual = new Chart(document.getElementById('chartCount').getContext('2d'), options);
        } else {
            chartStatistikTiketTerjual = new Chart(document.getElementById('chartCount').getContext('2d'), options);
        }
    }


    async function load_data() {
        console.log("PARAMS: ", params)
        let data = await $I.ajax('GET', "<?= route('Member', 'getDataSuperAdmin') ?>", params).catch(err => err);

        console.log("DATA: ", data);

        $('#event_aktif').html(data.event_aktif);
        $('#tiket_terjual').html(data.tiket_terjual);
        $('#total_transaksi').html(`Rp ${data.total_transaksi}`);

        var targetElement = $('#tableEvents');

        load_chart_circle(data.statistik_tiket_terjual);
        load_chart_bar(data.statistik_transaksi);

        var columns = [{
            data: 'id',
            render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: 'nama_organizer',
        }, {
            data: 'nama_event',
        }, {
            data: 'penyelenggara',
        }, {
            data: 'nama_kategori',
        }, {
            data: 'jenis_tiket',
            render: function(data, type, row, meta) {
                return (data === 1) ? "Berbayar" : "Gratis";
            }
        }, {
            data: 'date',
            render: function(data, type, row, meta) {
                return moment(data).format('DD MMM YYYY');
            }
        }, {
            data: 'time',
        }, {
            data: 'lokasi',
        }, {
            data: 'date',
            render: function(data, type, row, meta) {
                let check = (moment(data).isAfter(moment(), 'day'));
                return `<span class="badge badge-${check ? "success" : "danger"}">${ check ? "Event Aktif" : "Event Berakhir"}</span>`;
            }
        }, {
            data: 'nama_tiket',
        }, {
            data: 'jumlah_tiket',
        }, {
            data: 'sisa_tiket',
            // render: function(data, type, row, meta) {
            //     let check = (moment(data).isAfter(moment(), 'day'));
            //     return `<span class="badge badge-${check ? "success" : "danger"}">${ check ? "Event Aktif" : "Event Berakhir"}</span>`;
            // }
        }, {
            data: 'harga_tiket',
        }, {
            data: 'total_transaksi',
        }, {
            data: 'id',
            render: function(data, type, row, meta) {
                let encodeData = btoa(JSON.stringify(row));
                return `<button class="btn btn-primary btn-sm" onclick="showDetailTransaksi('${data}', '${encodeData}')" title="Detail Transaksi"><i class="fas fa-wallet"></i></button>`;
            }
        }];
        targetElement.removeClass("hidden");
        targetElement.dataTable().fnDestroy();
        var table = $I.showTableDataStatic(data?.events, columns, targetElement, {
            dom: '<"toolbar"<"filter-organizer">B>frtip',
            buttons: $I.generateButtonExport("Export", ['pdf', 'excel'], targetElement),
        });
    }

    load_data();

    async function showDetailTransaksi(id, detail = "") {
        $('#modalDetailTrx').modal('show');
        let detailEvent = JSON.parse(atob(detail));
        console.log("SHOW ID: ", {
            id,
            detailEvent
        });

        $('#tableDetailEvent').empty();
        $('#tableDetailEvent').html(`
            <tr>
                <th style="width: 30%;">Nama Organizer</th>
                <td style="width: 3%;">:</td>
                <td>${detailEvent.nama_organizer}</td>
            </tr>
            <tr>
                <th style="width: 30%;">Nama Event</th>
                <td style="width: 3%;">:</td>
                <td>${detailEvent.nama_event}</td>
            </tr>
            <tr>
                <th style="width: 30%;">Nama Kategori</th>
                <td style="width: 3%;">:</td>
                <td>${detailEvent.nama_kategori}</td>
            </tr>
            <tr>
                <th style="width: 30%;">Jenis Tiket</th>
                <td style="width: 3%;">:</td>
                <td>${(detailEvent.jenis_tiket === 1) ? "Berbayar": "Gratis"}</td>
            </tr>
            <tr>
                <th style="width: 30%;">Tanggal</th>
                <td style="width: 3%;">:</td>
                <td>${moment(detailEvent.date).format('DD MMMM YYYY')}</td>
            </tr>
            <tr>
                <th style="width: 30%;">Waktu</th>
                <td style="width: 3%;">:</td>
                <td>${detailEvent.time}</td>
            </tr>
            <tr>
                <th style="width: 30%;">Lokasi</th>
                <td style="width: 3%;">:</td>
                <td>${detailEvent.lokasi}</td>
            </tr>
            <tr>
                <th style="width: 30%;">Total Transaksi</th>
                <td style="width: 3%;">:</td>
                <td>${detailEvent.total_transaksi}</td>
            </tr>
        `)
        let data = await $I.ajax('GET', "<?= route('Member', 'getListTrxByEvent') ?>", {
            event_id: id
        }).catch(err => err);

        console.log('data', data);
        // return false;

        var targetElement = $('#tableDetailTrx');
        var columns = [{
            data: 'id',
            render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: 'invoice',
        }, {
            data: 'status',
            render: function(data, type, row, meta) {
                let check = "";
                if (data === 1) {
                    check = `<span class="badge badge-success">Transaction Successfully</span>`;
                } else if (data === 0) {
                    check = `<span class="badge badge-warning">Transaction Pending</span>`;
                } else if (data === 2) {
                    check = `<span class="badge badge-danger">Transaction Expired</span>`;
                }
                return check;
            }
        }, {
            data: 'email_notifikasi',
            // render: function(data, type, row, meta) {
            //     return moment(data).format('DD MMM YYYY');
            // }
        }, {
            data: 'jumlah_tiket',
        }, {
            data: 'metode_pembayaran',
        }, {
            data: 'virtual_account',
        }, {
            data: 'transaction_time',
            render: function(data, type, row, meta) {
                return moment(data).format('DD MMM YYYY HH:mm:ss');
            }
        }, {
            data: 'total',
        }, {
            data: 'fee',
        }, {
            data: 'grand_total',
        }];
        targetElement.removeClass("hidden");
        targetElement.dataTable().fnDestroy();
        var table = $I.showTableDataStatic(data, columns, targetElement, {
            dom: '<"toolbar"<"filter-organizer">B>frtip',
            buttons: $I.generateButtonExport("Export Detail", ['pdf', 'excel'], targetElement),
        });
    }

    $('#download-pdf-tiket-terjual').on('click', function() {
        //donwload pdf from original canvas
        var canvas = document.querySelector('#chartCount');
        // var ctx = canvas.getContext("2d");
        // ctx.fillStyle = "white";
        // ctx.fillRect(0, 0, canvas.width, canvas.height);

        //creates image
        var canvasImg = canvas.toDataURL("image/png", 1.0);

        //creates PDF from img
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        doc.text(15, 15, "Statistik Tiket Terjual");
        doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150);
        doc.save('canvas.pdf');
    });

    $('#download-pdf-statistik-transaksi').on('click', function() {
        //donwload pdf from original canvas
        var canvas = document.querySelector('#myChart');

        //creates image
        var canvasImg = canvas.toDataURL("image/png", 1.0);

        //creates PDF from img
        var doc = new jsPDF('landscape');
        doc.setFontSize(20);
        doc.text(15, 15, "Statistik Transaksi");
        doc.addImage(canvasImg, 'JPEG', 10, 10, 280, 150);
        doc.save('canvas.pdf');
    });

</script>
<?php $view->moveToDefaultFragment(); ?>