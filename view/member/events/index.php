<?php
$view = ViewManager::getInstance();

$events = $view->getVariable('events');
$eventAktif = $events['event_aktif'];
$eventTidakAktif = $events['event_tidak_aktif'];

?>

<h3>Event Saya</h3>
<hr>
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <a class="nav-link active" id="home-tab" onclick="changeStatus('aktif')" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Event Aktif</a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" id="profile-tab" onclick="changeStatus('tidak_aktif')" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Event Lalu</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <?php
        if (count($eventAktif) > 0) : ?>
            <div class="row mt-2">
                <?php foreach ($eventAktif as $valueAktif) : ?>
                    <div class="col-md-4 mb-2">
                        <a href="javascript:void(0)" onclick="showModalEvent('<?= $valueAktif['url'] ?>')" style="text-decoration: none !important;">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="row justify-content-center">
                                        <i class="fas fa-calendar-day text-center" style="color: #4183d7;font-size: 2em;"></i>
                                    </div>
                                    <h5 class="text-center my-2"><?= $valueAktif['nama_event']; ?></h5>
                                    <table class="table table-borderless table-sm text-left">
                                        <tr>
                                            <th>Tanggal</th>
                                            <td style="width: 1%;">:</td>
                                            <td><?= date('d F Y', strtotime($valueAktif['date'])); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Jam</th>
                                            <td>:</td>
                                            <td><?= $valueAktif['time'] ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php else : ?>
            <div class="d-flex justify-content-center mt-3">
                <h3 class="text-center">Data Tidak Ada.</h3>
            </div>
        <?php endif; ?>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <?php if (count($eventTidakAktif) > 0) : ?>
            <div class="row mt-2">
                <?php foreach ($eventTidakAktif as $valueTidakAktif) : ?>
                    <div class="col-md-4 mb-2">
                        <a href="javascript:void(0)" onclick="showModalEvent('<?= $valueTidakAktif['url']; ?>')" style="text-decoration: none !important;">
                            <div class="card">
                                <div class="card-body text-center">
                                    <div class="row justify-content-center">
                                        <i class="fas fa-calendar-day text-center" style="color: #4183d7;font-size: 2em;"></i>
                                    </div>
                                    <h5 class="text-center my-2"><?= $valueTidakAktif['nama_event']; ?></h5>
                                    <table class="table table-borderless table-sm text-left">
                                        <tr>
                                            <th>Tanggal</th>
                                            <td style="width: 1%;">:</td>
                                            <td><?= date('d F Y', strtotime($valueTidakAktif['date'])); ?></td>
                                        </tr>
                                        <tr>
                                            <th>Jam</th>
                                            <td>:</td>
                                            <td><?= $valueTidakAktif['time'] ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php
                endforeach; ?>
            </div>

        <?php else : ?>
            <div class="d-flex justify-content-center mt-3">
                <h3 class="text-center">Data Tidak Ada.</h3>
            </div>
        <?php endif; ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="showModalEvent" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Detail Event</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="detailEvent"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script>
    var statusEvent = 'aktif';

    function changeStatus(status) {
        statusEvent = status;
    }

    async function showModalEvent(url) {
        var data = await getDetailEvent(url).catch(err => err);
        console.log("GET DETAIL ", data);
        var {
            banner_event,
            profile_image,
            nama_event,
            nama_kategori,
            profile_image,
            nama_organize,
            date,
            time,
            lokasi,
            deskripsi_event,
            syarat_ketentuan,
            harga_tiket,
            sisa_tiket,
            nama_tiket
        } = data;

        if (data) {
            var bannerEvent = JSON.parse(banner_event);
            var profileImage = "https://img.icons8.com/bubbles/100/000000/user.png";
            if (profile_image !== null) {
                var profile = JSON.parse(profile_image);
                profileImage = profile.url;
            }

            $('#detailEvent').html(`
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card mt-3">
                            <img style="height: 50vh;object-fit: cover;" src="${bannerEvent.url}" class="card-img-top">
                            <div class="card-body">
                                <h3 class="card-title">${nama_event}</h3>
                                <div>
                                    <span class="badge badge-light">${nama_kategori}</span>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h6 style="font-weight: bold;">Diselenggarakan oleh</h6>
                                        <div class="media" style="align-items: center;">
                                            <img src="${profileImage}" style="height: 75px;width: 75px; object-fit: cover;border-radius:.50em;" />
                                            <div class="media-body">
                                                <h5 class="mx-2">${nama_organize}</h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h6 style="font-weight: bold;">Tanggal & Waktu</h6>
                                        <div style="display: flex; align-items:center" class="m-2">
                                            <i class="far fa-calendar m-1" style="font-size: 20px;"></i>
                                            ${moment(date).format('DD MMMM YYYY')}
                                        </div>
                                        <div style="display: flex; align-items:center" class="m-2">
                                            <i class="far fa-clock m-1" style="font-size: 20px;"></i>
                                            ${time}
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <h6 style="font-weight: bold;">Lokasi</h6>
                                        <div style="display: flex; align-items:center" class="m-2">
                                            <i class="fas fa-map-marker-alt m-1" style="font-size: 20px;"></i>
                                            ${lokasi}
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Deskripsi Event</a>
                                            <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Kategori Tiket</a>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                <div>
                                                    ${atob(deskripsi_event)}
                                                </div>
                                                <div>
                                                    <h5>SYARAT & KETENTUAN</h5>
                                                    ${atob(syarat_ketentuan)}
                                                </div>
                                            </div>
                                            <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                <div class="ticket">
                                                    <div class="stub">
                                                        <div class="" style="height: 100%;">
                                                            <div style="display: grid;align-items: center;justify-content: center;height: 100%;">
                                                                <div class="top">
                                                                    <span class="num" style="font-size: 32px;font-weight: 600;margin:auto;">
                                                                        ${formatter.format(harga_tiket)}
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="check">
                                                        <div class="big" id="titleTiket">
                                                            ${nama_tiket}
                                                        </div>
                                                        <div class="info">
                                                            <section>
                                                                <div class="title">Sisa Tiket</div>
                                                                <div>${sisa_tiket}</div>
                                                            </section>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `);
            $('#showModalEvent').modal('show');
        } else {
            alert('Data Tidak Ada.');
        }
    }

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
        maximumFractionDigits: 0,
        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });

    function getDetailEvent(url) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: '<?= route('Event', 'getDetailEventSaya') ?>',
                method: 'POST',
                dataType: 'json',
                data: {
                    url,
                    status: statusEvent
                },
                success: function(result) {
                    resolve(result);
                },
                error: function(err) {
                    console.log(err);
                    reject(err);
                }
            });
        });
    }
</script>
<?php $view->moveToDefaultFragment(); ?>