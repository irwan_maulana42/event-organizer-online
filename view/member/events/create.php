<?php
$view = ViewManager::getInstance();
$view->moveToFragment("css"); ?>
<style>
    .form-input {
        width: 100%;
        padding: 20px;
        background: #fff;
        box-shadow: -3px -3px 7px rgba(94, 104, 121, 0.377),
            3px 3px 7px rgba(94, 104, 121, 0.377);
    }

    .form-input input {
        display: none;

    }

    .form-input label {
        display: block;
        width: 45%;
        height: 45px;
        margin-left: 25%;
        line-height: 50px;
        text-align: center;
        background: #1172c2;

        color: #fff;
        font-size: 15px;
        font-family: "Open Sans", sans-serif;
        text-transform: Uppercase;
        font-weight: 600;
        border-radius: 5px;
        cursor: pointer;
    }

    .form-input img {
        width: 100%;
        display: none;

        margin-bottom: 30px;
    }
</style>
<?php
$view->moveToDefaultFragment();
$dataKategori = $view->getVariable('dataKategori');
$dataPembeli = $view->getVariable('dataPembeli');
$detailUser = $view->getVariable('detail_user');
?>
<h3>Buat Event</h3>
<hr>
<div class="row justify-content-center">
    <div class="col-md-12">
        <form action="index.php?controller=Event&action=post_event" method="POST" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-center">
                        <div class="form-input">
                            <div class="preview">
                                <img id="file-ip-1-preview" style="width: auto;height:400px;margin: auto;">
                            </div>
                            <br>
                            <label for="file-ip-1">Upload Image</label>
                            <input type="file" id="file-ip-1" accept="image/*" name="banner_event" onchange="showPreview(event);" required>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="namaEvent">Nama Event</label>
                        <input type="text" name="nama_event" id="" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="namaEvent">Pilih kategori</label>
                        <select name="kategori" id="" class="form-control" required>
                            <?php foreach ($dataKategori as $kategori) : ?>
                                <option value="<?= $kategori['id']; ?>"><?= $kategori['nama']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Diselenggarakan Oleh</label>
                                <div class="d-flex align-items-center">
                                    <?php
                                    $profileImage = "https://img.icons8.com/bubbles/100/000000/user.png";
                                    if (!empty($detailUser['profile_image']) && $detailUser['profile_image'] !== 'null') {
                                        $profileImage = json_decode($detailUser['profile_image'], true)['url'];
                                    }
                                    ?>
                                    <img src="<?= $profileImage; ?>" style="height: 80px;width: 80px; object-fit: cover;border-radius:.50em;" class="img-responsive m-1" />
                                    <input type="text" class="form-control form-control-sm" name="penyelenggara" value="<?= $detailUser['nama_organizer'] ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Tanggal & Waktu</label>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">
                                            <i class="fas fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="date" class="form-control" name="date" required>
                                </div>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">
                                            <i class="fas fa-clock"></i>
                                        </span>
                                    </div>
                                    <input type="time" class="form-control" name="time" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Lokasi</label>
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">
                                            <i class="fas fa-map-marked"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="lokasi" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item w-50 text-center" role="presentation">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Kategori Tiket</a>
                        </li>
                        <li class="nav-item w-50 text-center" role="presentation">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Deskripsi Event</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="form-group">
                                <label for="">Jenis Tiket</label>
                                <select name="jenis_tiket" onchange="changeJenisTiket(event)" class="form-control">
                                    <option value="1">Tiket Berbayar</option>
                                    <option value="2">Tiket Gratis</option>
                                </select>
                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Nama Tiket</label>
                                                <input type="text" name="nama_tiket" id="" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Jumlah Tiket</label>
                                                <input type="number" name="jumlah_tiket" id="" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="">Harga Tiket</label>
                                                <input type="number" name="harga_tiket" id="harga_tiket" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Deskripsi Tiket</label>
                                                <textarea name="deskripsi_tiket" id="" cols="30" rows="10" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <h5 for="">Atur Data Pembeli</h5>
                                <?php  foreach ($dataPembeli as $pembeli): ?>
                                    <div class="custom-control custom-checkbox mr-sm-2">
                                        <input name="atur_data_pembeli[]" type="checkbox" class="custom-control-input" value="<?= $pembeli['name'] ?>" id="<?= $pembeli['name'] ?>" <?= $pembeli['attributes']. " ". $pembeli['event_listener'] ?>>
                                        <label class="custom-control-label" for="<?= $pembeli['name'] ?>"><?= $pembeli['label'] ?></label>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <div class="m-2">
                                <textarea name="deskripsi_event" id="editor_deskripsi" cols="30" rows="10" required>This is some sample content.</textarea>
                                <hr>
                                <h3>Syarat & Ketentuan</h3>
                                <textarea name="syarat_ketentuan" id="editor_syarat" cols="30" rows="10" required>This is some sample content.</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success btn-lg btn-block">BUAT EVENT</button>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
<?php $view->moveToFragment("custom-js"); ?>
<script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
<script>
    function showPreview(event) {
        if (event.target.files.length > 0) {
            var src = URL.createObjectURL(event.target.files[0]);
            var preview = document.getElementById("file-ip-1-preview");
            preview.src = src;
            preview.style.display = "block";
        }
    }

    ClassicEditor
        .create(document.querySelector('#editor_deskripsi'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });

    ClassicEditor
        .create(document.querySelector('#editor_syarat'))
        .then(editor => {
            console.log(editor);
        })
        .catch(error => {
            console.error(error);
        });

    function changeJenisTiket(event){
        var value = event.target.value
        console.log("JENIS TIKET", value);
        if(parseInt(value) === 2){
            console.log("DISABLED HARGA TIKET")
            $('#harga_tiket').attr('readonly', 'readonly').val(0);
        }else{
            $('#harga_tiket').removeAttr('readonly').val('');
        }
    }
</script>
<?php $view->moveToDefaultFragment(); ?>