<?php
$view = ViewManager::getInstance();
// $eventAktif = $view->getVariable('eventAktif');
// $tiketTerjual = $view->getVariable('tiketTerjual');
// $totalTransaksi = $view->getVariable('grandTotal');

?>
<h3>Data Organizer</h3>
<hr>
<div class="container">
    <table class="table" id="myTable" style="width: 100%;">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Organizer</th>
                <th>No HP</th>
                <th>Alamat</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
    <!-- <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">List Organizer</div>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div> -->
</div>

<!-- Modal -->
<div class="modal fade" id="modalDetail" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Detail Tiket Organizer</h5>
            </div>
            <div class="modal-body" id="isiBody">
                <table class="table" id="tableDetailOrgz" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Event</th>
                            <th>Penyelenggara</th>
                            <th>Kategori</th>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Lokasi</th>
                            <th>Jenis Tiket</th>
                            <th>Nama Tiket</th>
                            <th>Harga Tiket</th>
                            <th>Total Tiket</th>
                            <th>Sisa Tiket</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeInvoice">Close</button>
                <!-- <button type="button" class="btn btn-primary" id="bayarInvoice">Bayar</button> -->
            </div>
        </div>
    </div>
</div>
<?php $view->moveToFragment("custom-js"); ?>
<script>
    var params = {};

    async function load_data() {
        let data = await $I.ajax('GET', "<?= route('Member', 'getListOrganizer') ?>").catch(err => err);

        var targetElement = $('#myTable');
        var columns = [{
            data: 'id',
            render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: 'nama_organizer',
        }, {
            data: 'no_handphone',
        }, {
            data: 'alamat',
        }, {
            data: 'id',
            render: function(data, type, row, meta) {
                var html = '';
                html += `
                    <a class="btn btn-light" href="javascript:void(0)" onclick="showDetailTable('${row.user_id}')">Detail</a>
                `;
                return html;
            }
        }];
        targetElement.removeClass("hidden");
        targetElement.dataTable().fnDestroy();
        var table = $I.showTableDataStatic(data, columns, targetElement, {
            dom: '<"toolbar"<"filter-organizer">B>frtip',
            buttons: $I.generateButtonExport("Export Data Organizer", ['pdf', 'excel'], targetElement),
        });
    }

    load_data();

    async function showDetailTable(user_id) {
        $('#modalDetail').modal('show');
        let data = await $I.ajax('GET', "<?= route('Member', 'getListEventsByUserId') ?>", {
            user_id
        }).catch(err => err);

        var targetElement = $('#tableDetailOrgz');
        var columns = [{
            data: 'id',
            render: function(data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }
        }, {
            data: 'nama_event',
        }, {
            data: 'penyelenggara',
        }, {
            data: 'nama_kategori',
        }, {
            data: 'date',
            render: function(data, type, row, meta) {
                return moment(data).format('DD MMM YYYY');
            }
        }, {
            data: 'time',
        }, {
            data: 'lokasi',
        }, {
            data: 'jenis_tiket',
        }, {
            data: 'nama_tiket',
        }, {
            data: 'harga_tiket',
        }, {
            data: 'jumlah_tiket',
        }, {
            data: 'sisa_tiket',
        }, {
            data: 'date',
            render: function(data, type, row, meta) {
                let check = (moment(data).isAfter(moment(), 'day'));
                return `<span class="badge badge-${check ? "success" : "danger"}">${ check ? "Event Aktif" : "Event Berakhir"}</span>`;
            }
        }];
        targetElement.removeClass("hidden");
        targetElement.dataTable().fnDestroy();
        var table = $I.showTableDataStatic(data, columns, targetElement, {
            dom: '<"toolbar"<"filter-organizer">B>frtip',
            buttons: $I.generateButtonExport("Export Detail Event", ['pdf', 'excel'], targetElement),
        });
    }
</script>
<?php $view->moveToDefaultFragment(); ?>