<?php
$view = ViewManager::getInstance();
$tickets = $view->getVariable('tickets');
?>

<h3>Tiket Saya</h3>
<hr>
<div class="row mt-2">
    <?php
    if (!empty($tickets)) :
        foreach ($tickets as $ticket) :
    ?>
            <div class="col-md-4 mb-2">
                <a href="javascript:void(0)" onclick="showDetailTiket('<?= $ticket['kode_tiket'] ?>', '<?= $ticket['event_id']; ?>')">
                    <div class="card">
                        <div class="card-body text-center">
                            <div class="row justify-content-center">
                                <i class="fas fa-ticket-alt text-center" style="color: #4183d7;font-size: 3em;"></i>
                            </div>
                            <!-- <h5 class="text-center my-2"><?= $ticket['nama_event'] ?></h5> -->
                            <table class="table table-sm table-borderless text-left">
                                <tr>
                                    <th>Nama Event</th>
                                    <td style="width: 1%;">:</td>
                                    <td><?= $ticket['nama_event']; ?></td>
                                </tr>
                                <tr>
                                    <th>Nama Tiket</th>
                                    <td style="width: 1%;">:</td>
                                    <td><?= $ticket['nama_tiket']; ?></td>
                                </tr>
                                <tr>
                                    <th>Tanggal</th>
                                    <td style="width: 1%;">:</td>
                                    <td><?= date('d F Y', strtotime($ticket['date'])); ?></td>
                                </tr>
                                <tr>
                                    <th>Kode Tiket</th>
                                    <td style="width: 1%;">:</td>
                                    <td><?= $ticket['kode_tiket']; ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </a>
            </div>
    <?php
        endforeach;
    else :
        echo "<h3 class='text-center'>Data Tidak Ada !</h3>";
    endif;
    ?>
</div>
<!-- Modal -->
<div class="modal fade" id="showDetailTiket" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Detail Tiket</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div style="display: grid;" class="justify-content-center">
                    <div id="showQR"></div>
                    <h4 class="text-center" id="kodeTiket"></h4>
                    <!-- <img src="https://img.icons8.com/ios/100/000000/loading.png" id="showQR" class="img-responsive img-thumbnail" style="width: 200px;"> -->
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-bordered table-sm" id="showTicket">

                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-bordered table-sm" id="showPemesan">
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php $view->moveToFragment("custom-js"); ?>
<script src="https://cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>

<script>
    // new QRCode(document.getElementById("showQR"), "https://webisora.com");
    $(document).ready(function() {
        console.log("TETS ");
    });

    async function showDetailTiket(kode_tiket, event_id) {
        // $('#showQR').attr('src', 'https://img.icons8.com/ios/100/000000/loading.png');
        var data = await getDataDetailTiket(kode_tiket, event_id).catch(err => err);
        console.log("DATA", data);

        var {
            ticket,
            qr,
            temp_pemesan
        } = data;

        var dataPemesan = JSON.parse(ticket.data_pemesan);

        if (ticket !== null) {
            $('#showQR').empty();
            var qrCode = new QRCode(document.getElementById('showQR'), {
                text: ticket.kode_tiket,
                width: 256,
                height: 256,
                // colorDark: "#5868bf",
                // colorLight: "#ffffff",
                correctLevel: QRCode.CorrectLevel.H
            });
            $('#kodeTiket').html(ticket.kode_tiket);
            // $('#showQR').attr('src', qr);
            $('#showTicket').html(`
                <tr>
                    <th style="width: 30%;">Kode Tiket</th>
                    <td>${ticket.kode_tiket}</td>
                </tr>
                <tr>
                    <th>Nama Event</th>
                    <td>${ticket.nama_event}</td>
                </tr>
                <tr>
                    <th>Nama Tiket</th>
                    <td>${ticket.nama_tiket}</td>
                </tr>
                <tr>
                    <th>Tanggal</th>
                    <td>${moment(ticket.date).format('DD MMMM YYYY')}</td>
                </tr>
                <tr>
                    <th>Waktu</th>
                    <td>${ticket.time}</td>
                </tr>
                <tr>
                    <th>Lokasi</th>
                    <td>${ticket.lokasi}</td>
                </tr>
            `);

            var templatePemesan = `
                <tr>
                    <th colspan="2" class="text-center">Data Pemesan</th>
                </tr>`;

            var object = Object.keys(dataPemesan);
            object.forEach((item) => {
                var findLabel = temp_pemesan.find((find) => find.name === item);
                templatePemesan += `
                <tr>
                    <th style="width: 30%">${findLabel.label}</th>
                    <td>${dataPemesan[item]}</td>
                </tr>
                `
            });
            console.log("RESS", {
                object,
                templatePemesan
            });
            $('#showPemesan').html(templatePemesan);
            $('#showDetailTiket').modal('show');
        } else {
            alert('Data Tidak Ada !');
        }
    }

    function getDataDetailTiket(kode_tiket, event_id) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: "<?= route('Ticket', 'getTicketByKode') ?>",
                method: 'POST',
                data: {
                    kode_tiket: kode_tiket,
                    event_id: event_id
                },
                dataType: 'json',
                success: function(result) {
                    resolve(result);
                },
                error: function(err) {
                    console.log(err);
                    reject(err);
                }
            })
        });
    }
</script>
<?php $view->moveToDefaultFragment(); ?>