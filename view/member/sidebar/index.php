<?php
require_once(__DIR__ . "/../../../core/ViewManager.php");

$view = ViewManager::getInstance();
$detail = $view->getVariable('detail_user');
// echo "<pre>";
// print_r($detail);
// echo "<pre>";
// die;
?>
<a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
</a>
<nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
        <div class="sidebar-brand">
            <a href="index.php">EVENT ORGANIZER</a>
            <div id="close-sidebar">
                <i class="fas fa-times"></i>
            </div>
        </div>
        <div class="sidebar-header">
            <div class="user-pic" style="width: 75px;height: 75px;">
                <?php
                $image = "https://img.icons8.com/color/100/000000/user-male-circle--v1.png";
                if (!empty($detail['profile_image']) && $detail['profile_image'] !== 'null') {
                    $image = json_decode($detail['profile_image'], true)['url'];
                }
                ?>
                <img class="img-responsive img-rounded" src="<?= $image; ?>" alt="User picture">
            </div>
            <div class="user-info">
                <span class="user-name"><?= (!empty($detail['nama_organizer'])) ? $detail['nama_organizer'] : "-"; ?></span>
                <span class="user-role"><?= $detail['email']; ?></span>
                <span class="user-status">
                    <!-- <i class="fa fa-circle"></i> -->
                    <span><?= $detail['is_superadmin'] ? 'Superadmin' : 'Member' ?></span>
                </span>
            </div>
        </div>
        <!-- sidebar-header  -->
        <div class="sidebar-search">
            <div>
                <a href="index.php?controller=Event&action=create_event" class="btn btn-success btn-block">BUAT EVENT</a>
                <!-- <div class="input-group">
                    <input type="text" class="form-control search-menu" placeholder="Search...">
                    <div class="input-group-append">
                        <span class="input-group-text">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </span>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- sidebar-search  -->
        <div class="sidebar-menu">
            <ul>
                <li class="header-menu">
                    <span>Dashboard</span>
                </li>
                <li>
                    <a href="index.php?controller=Member&action=index">
                        <i class="fa fa-tachometer-alt"></i>
                        <span>Home</span>
                        <!-- <span class="badge badge-pill badge-warning">New</span> -->
                    </a>
                </li>
                <?php if($detail['is_superadmin']): ?>
                <li>
                    <a href="index.php?controller=Member&action=list_organizer">
                        <i class="fas fa-calendar-week"></i>
                        <span>Data Organizer</span>
                    </a>
                </li>
                <?php endif; ?>
                <li>
                    <a href="index.php?controller=Event&action=event_saya">
                        <i class="fas fa-calendar-week"></i>
                        <span>Event Saya</span>
                    </a>
                </li>
                <li>
                    <a href="index.php?controller=Ticket&action=index">
                        <i class="fas fa-ticket-alt"></i>
                        <span>Tiket Saya</span>
                    </a>
                </li>
                <li class="sidebar-dropdown">
                    <a href="#">
                        <!-- <i class="fa fa-shopping-cart"></i> -->
                        <i class="fa fa-folder"></i>
                        <span>Profil Kamu</span>
                        <!-- <span class="badge badge-pill badge-danger">3</span> -->
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a href="<?= route('Users', 'informasi_kamu'); ?>">Informasi Kamu</a>
                            </li>
                            <li>
                                <a href="<?= route('Member', 'change_password'); ?>">Ubah Password</a>
                            </li>
                            <li>
                                <a href="<?= route('Home', 'rekening_kamu'); ?>">Rekening Kamu</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
        <!-- <a href="#">
            <i class="fa fa-bell"></i>
            <span class="badge badge-pill badge-warning notification">3</span>
        </a>
        <a href="#">
            <i class="fa fa-envelope"></i>
            <span class="badge badge-pill badge-success notification">7</span>
        </a>
        <a href="#">
            <i class="fa fa-cog"></i>
            <span class="badge-sonar"></span>
        </a> -->
        <a href="javascript:void(0)" id="logout">
            <i class="fa fa-power-off"></i>
        </a>
    </div>
</nav>