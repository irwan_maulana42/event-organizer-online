<?php
$view = ViewManager::getInstance();
$view->moveToFragment("css"); ?>
<style>
    .error {
        color: #a94442;
    }
</style>
<?php
$view->moveToDefaultFragment();
$rekening = $view->getVariable('rekening');
?>

<h3>Rekening Kamu</h3>
<hr>
<div class="card">
    <div class="card-body">
        <form method="POST" action="<?= route('Home', 'post_rekening_kamu'); ?>" id="formPassword">
            <div class="form-group">
                <label for="">Bank</label>
                <select name="bank" id="bank" class="form-control form-control-sm">
                    <option value="">Pilih</option>
                    <option value="bca" <?= (@$rekening['nama_bank'] === 'bca') ? "selected" : "";  ?>>BCA</option>
                    <option value="mandiri" <?= (@$rekening['nama_bank'] === 'mandiri') ? "selected" : "";  ?>>Mandiri</option>
                    <option value="bri" <?= (@$rekening['nama_bank'] === 'bri') ? "selected" : "";  ?>>BRI</option>
                    <option value="cimb_niaga" <?= (@$rekening['nama_bank'] === 'cimb_niaga') ? "selected" : "";  ?>>Cimb Niaga</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Nama Pemilik Rekening</label>
                <input type="text" name="pemilik_rekening" id="pemilik_rekening" class="form-control form-control-sm" value="<?= @$rekening['pemilik_rekening'] ?>" required>
            </div>
            <div class="form-group">
                <label for="">Nomor Rekening</label>
                <input type="number" name="nomor_rekening" id="nomor_rekening" class="form-control form-control-sm" value="<?= @$rekening['nomor_rekening'] ?>" required>
            </div>
            <div class="form-group">
                <label for="">Kantor Cabang</label>
                <input type="text" name="kantor_cabang" id="kantor_cabang" class="form-control form-control-sm" value="<?= @$rekening['kantor_cabang'] ?>" required>
                <small>Kantor cabang saat kamu membuka rekening. Contoh: Kantor Cabang Kaliurang.</small>
            </div>
            <div class="form-group">
                <label for="">Kota</label>
                <input type="text" name="kota" id="kota" class="form-control form-control-sm" value="<?= @$rekening['kota'] ?>" required>
                <small>Kota tempat kantor cabang saat kamu membuka rekening. Contoh: Yogyakarta.</small>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Simpan Rekening</button>
            </div>
        </form>
    </div>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script>
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "This has to be different...");

    $('#formPassword').validate({
        rules: {
            bank: {
                required: true,
            },
            nama_pemilik_rekening: {
                required: true,
            },
            nomor_rekening: {
                required: true,
            },
            kantor_cabang: {
                required: true,
            },
            kota: {
                required: true,
            },
        },
    }, {
        submitHandler: function(form) {
            form.submit();
        }
    });
</script>
<?php $view->moveToDefaultFragment(); ?>