<?php
$view = ViewManager::getInstance();
$view->moveToFragment("css"); ?>
<style>
    .error {
        color: #a94442;
    }
</style>
<?php
$view->moveToDefaultFragment();
$profile = $view->getVariable('profile');
?>

<h3>Ubah Password</h3>
<hr>
<div class="card">
    <div class="card-body">
        <form method="POST" action="<?= route('Member', 'changePasswordUser'); ?>" id="formPassword">
            <div class="form-group">
                <label for="">Password Lama</label>
                <input type="password" name="password_lama" id="password_lama" class="form-control" value="" required>
            </div>
            <div class="form-group">
                <label for="">Password Baru</label>
                <input type="password" name="password_baru" id="password_baru" class="form-control" value="" required>
            </div>
            <div class="form-group">
                <label for="">Konfirmasi Password Baru</label>
                <input type="password" name="konfirmasi_password" id="konfirmasi_password" class="form-control" value="" required>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg">Simpan Perubahan</button>
            </div>
        </form>
    </div>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script>
    jQuery.validator.addMethod("notEqual", function(value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, "This has to be different...");

    $('#formPassword').validate({
        rules: {
            password_lama: {
                required: true,
                minlength: 5,
                remote: {
                    url: '<?= route('Users', 'checkingPassword') ?>',
                    type: 'POST',
                    dataType: 'json',
                }
            },
            password_baru: {
                required: true,
                minlength: 5,
                notEqual: '#password_lama'
            },
            konfirmasi_password: {
                required: true,
                minlength: 5,
                equalTo: '#password_baru'
            },
        },
        messages: {
            password_lama: {
                remote: 'Password doesn\'t same !'
            }
        }
    }, {
        submitHandler: function(form) {
            form.submit();
        }
    })
</script>
<?php $view->moveToDefaultFragment(); ?>