<?php
$view = ViewManager::getInstance();
$view->moveToFragment("css"); ?>
<style>
    .form-input {
        width: 100%;
        padding: 20px;
        background: #fff;
        /* box-shadow: -3px -3px 7px rgba(94, 104, 121, 0.377),
            3px 3px 7px rgba(94, 104, 121, 0.377); */
    }

    .form-input input {
        display: none;

    }

    .form-input label {
        display: block;
        width: 45%;
        height: 45px;
        margin-left: 25%;
        line-height: 50px;
        text-align: center;
        background: #1172c2;

        color: #fff;
        font-size: 15px;
        font-family: "Open Sans", sans-serif;
        text-transform: Uppercase;
        font-weight: 600;
        border-radius: 5px;
        cursor: pointer;
    }

    .form-input img {
        width: 100%;
        display: none;

        margin-bottom: 30px;
        display: flex;
        border-radius: 50%;
        border: 1px solid #ddd;
        width: 150px;
        height: 150px;
        margin-left: auto;
        margin-right: auto;
    }
</style>
<?php
$view->moveToDefaultFragment();
$profile = $view->getVariable('profile');
?>

<h3>Informasi Dasar</h3>
<hr>
<div class="card">
    <div class="card-body">
        <form method="POST" action="index.php?controller=Users&action=postInformasi" enctype="multipart/form-data">
            <div class="form-input">
                <div class="preview">
                    <?php 
                    $showImage = "https://img.icons8.com/color/100/000000/user-male-circle--v1.png";
                    if(!empty($profile['profile_image']) && $profile['profile_image'] !== 'null'){
                        $showImage = json_decode($profile['profile_image'], true)['url'];
                    }
                    ?>
                    <img id="file-ip-1-preview" style="object-fit: cover;" src="<?= $showImage ?>">
                </div>
                <label for="file-ip-1">Upload Image</label>
                <input type="file" id="file-ip-1" accept="image/*" name="profile_image" onchange="showPreview(event);">
            </div>

            <div class="form-group">
                <label for="">Nama Organizer</label>
                <input type="text" name="nama_organizer" id="" class="form-control" value="<?= set_value(@$profile['nama_organizer']) ?>" required>
            </div>

            <div class="form-group">
                <label for="">Nomor Handphone</label>
                <input type="tel" name="nomor_handphone" id="" class="form-control" value="<?= set_value(@$profile['no_handphone']) ?>" required>
            </div>

            <div class="form-group">
                <label for="">Alamat</label>
                <textarea name="alamat" id="" cols="30" rows="10" class="form-control"><?= set_value(@$profile['alamat']) ?></textarea>
            </div>

            <div class="form-group">
                <label for="">Tentang Kami</label>
                <textarea name="tentang_kami" id="" cols="30" rows="10" class="form-control"><?= set_value(@$profile['tentang_kami']) ?></textarea>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg">Simpan Perubahan</button>
            </div>
        </form>
    </div>
</div>

<?php $view->moveToFragment("custom-js"); ?>
<script>
    function showPreview(event) {
        if (event.target.files.length > 0) {
            var src = URL.createObjectURL(event.target.files[0]);
            var preview = document.getElementById("file-ip-1-preview");
            preview.src = src;
            preview.style.display = "block";
        }
    }
</script>
<?php $view->moveToDefaultFragment(); ?>