<?php
//file: view/layouts/default.php

$view = ViewManager::getInstance();
$currentuser = $view->getVariable("currentusername");

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title><?= $view->getVariable("title", "Home") ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
	<!-- enable ji18n() javascript function to translate inside your scripts -->
	<script src="index.php?controller=language&amp;action=i18njs"></script>
	<?= $view->getFragment("css"); ?>
	<?= $view->getFragment("javascript"); ?>
</head>

<body>
	<?php
	$flash = $view->popFlash();
	include(__DIR__ . "/navbar.php");
	?>
	<main>
		<div>
			<?php if (!empty($flash)) :
				$statusHeader = "";
				if ($flash['status'] === 'danger') {
					$statusHeader = "Oops...";
				} else if ($flash['status'] === 'success') {
					$statusHeader = "Success";
				}
			?>
				<div class="alert alert-<?= $flash['status'] ?> alert-dismissible" role="alert">
					<h4 class="alert-heading"><?= $statusHeader ?></h4>
					<p><?= $flash['message'] ?></p>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php endif; ?>
		</div>
		<?= $view->getFragment(ViewManager::DEFAULT_FRAGMENT); ?>
	</main>

	<!-- <footer>
		<?php
		//include(__DIR__."/language_select_element.php");
		?>
	</footer> -->

	<!-- Optional JavaScript; choose one of the two! -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/js/all.min.js" integrity="sha512-Tn2m0TIpgVyTzzvmxLNuqbSJH3JP8jm+Cy3hvHrW7ndTDcJ1w5mBiksqDBb8GpE2ksktFvDB/ykZ0mDpsZj20w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

	<?= $view->getFragment("custom-js"); ?>

	<script>
		$('#logout').on('click', function() {
			swal({
					title: "Apakah Anda yakin  ingin keluar?",
					// text: "Once deleted, you will not be able to recover this imaginary file!",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
				.then((willDelete) => {
					if (willDelete) {
						$.ajax({
							url: 'index.php?controller=Users&action=logout',
							method: 'POST',
							success: function(result) {
								window.location.reload();
							},
							error: function(err) {
								console.log(err);
								swal(err.message, {
									icon: "error",
								});
							}
						});
					}
				});
		});
	</script>
</body>

</html>