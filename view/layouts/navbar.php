<?php
$view = ViewManager::getInstance();
?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.php">
        <img src="https://img.icons8.com/color-glass/100/000000/two-tickets.png" width="30" height="30" class="d-inline-block align-top" alt="">
        <span class="lead">EVENT ORGANIZER</span>
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <form class="form-inline pl-5" method="POST" action="<?= route('Home', 'discover'); ?>">
            <div class="input-group" style="width: 25em !important;">
                <?php
                $search = "";
                if (isset($_GET['search'])) {
                    $search = $_GET['search'];
                }
                ?>
                <input type="text" name="search" class="form-control" placeholder="Search..." value="<?= $search; ?>">
                <div class="input-group-append">
                    <button class="btn btn-light" type="submit" id="button-addon2"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </form>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Buat Event
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="index.php?controller=Event&action=create_event">BUAT EVENT</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="index.php?controller=Home&action=payment">Simulasi Pembayaran</a>
                </div>
            </li>
            <?php if (!isset($_SESSION['currentuser'])) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?controller=Users&amp;action=login">
                        <i class="fas fa-sign-in-alt"></i>
                    </a>
                </li>
            <?php else : ?>
                <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="nav-link dropdown-toggle" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a href="index.php?controller=Member&action=index" class="dropdown-item">
                            <i class="fas fa-home"></i>
                            Dashboard
                        </a>
                        <a href="index.php?controller=Member&action=index" class="dropdown-item">
                            <i class="fas fa-user-alt"></i>
                            Profile
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="javascript:void(0)" id="logout" class="dropdown-item">
                            <i class="fas fa-sign-out-alt"></i>
                            Keluar
                        </a>
                    </div>
                </li>
            <?php endif; ?>
            <li class="nav-item dropdown">
                <a class="nav-link" href="<?= route('Home', 'hubungi_kami') ?>">
                    Hubungi Kami
                </a>
            </li>
        </ul>
    </div>
</nav>