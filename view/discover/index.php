<?php
$view = ViewManager::getInstance();
$events = $view->getVariable('events');
$kategori = $view->getVariable('kategori');

?>
<div class="container-fluid mt-5">
    <div class="row justify-content-end">
        <div class="col-md-2">
            <form method="POST" action="http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>">
                <input type="hidden" name="search" value="<?= $_GET['search'] ?>">
                <h4>Filter</h4>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <h6>Kategori</h6>
                        <select name="kategori" id="" class="form-control form-control-sm">
                            <option value="">Selected</option>
                            <?php foreach ($kategori as $dt) :
                                $selected = ($dt['id'] == $_GET['kategori']) ? "selected" : "";
                            ?>
                                <option value="<?= $dt['id'] ?>" <?= $selected ?>><?= $dt['nama'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </li>
                    <li class="list-group-item">
                        <h6>Lokasi</h6>
                        <input type="text" class="form-control form-control-sm" name="lokasi" value="<?= @$_GET['lokasi'] ?>">
                    </li>
                    <li class="list-group-item">
                        <h6>Tanggal</h6>
                        <input type="text" id="date" class="form-control form-control-sm" name="tanggal" value="<?= @$_GET['tanggal'] ?>">
                    </li>
                </ul>
                <button class="btn btn-primary btn-sm">Filter</button>
            </form>
        </div>
        <div class="col-md-10">
            <div class="row">
                <?php
                if (!empty($events)) :
                    foreach ($events as $event) :
                        $bannerImage = json_decode($event['banner_event'], true);
                ?>
                        <div class="col-md-3 my-2">
                            <div class="card">
                                <img src="<?= $bannerImage['url']; ?>" class="card-img-top" alt="Image">
                                <div class="card-body">
                                    <small class="mb-2 mt-1 text-muted"><?= $event['penyelenggara'] ?></small>
                                    <h5 class="card-title"><?= $event['nama_event']; ?></h5>
                                    <h6 class="card-subtitle mb-2 text-muted"><?= date('d M Y', strtotime($event['date'])) ?></h6>
                                    <p><?= ($event['jenis_tiket'] == 1) ? "Rp." . number_format($event['harga_tiket'], 0, ',', '.') : 'Gratis'; ?></p>
                                    <a href="<?= route('Event', 'index', 'event=' . $event['url']) ?>" class="btn btn-primary btn-block mt-1">BELI TIKET</a>
                                </div>
                            </div>
                        </div>
                    <?php
                    endforeach;
                else : ?>
                    <div class="col-md-12 my-2">
                        <div class="card">
                            <div class="card-body">
                                <p class="text-center m-0 font-weight-bold">Data tidak ditemukan !</p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php $view->moveToFragment("custom-js"); ?>
<script>
    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'IDR',
        maximumFractionDigits: 0,
        // These options are needed to round to whole numbers if that's what you want.
        //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
        //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
    });

    $("#date").flatpickr({
        dateFormat: "Y-m-d",
        // onChange: function(selectedDates, dateStr, instance) {
        //     if (selectedDates.length > 0) {
        //         params.start_date = moment(selectedDates[0]).format('YYYY-MM-DD');
        //         params.end_date = moment(selectedDates[1]).format('YYYY-MM-DD');
        //     } else {
        //         params.start_date = moment().startOf('year').format('YYYY-MM-DD'),
        //             params.end_date = moment().endOf('year').format('YYYY-MM-DD');
        //     }
        // }
    })
</script>
<?php $view->moveToDefaultFragment(); ?>