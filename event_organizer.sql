-- Adminer 4.8.1 MySQL 5.7.33 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `carousel_banner`;
CREATE TABLE `carousel_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` text,
  `title` text,
  `desc` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `carousel_banner` (`id`, `image_url`, `title`, `desc`, `created_at`, `updated_at`) VALUES
(1,	'https://s3-ap-southeast-1.amazonaws.com/loket-production-sg/images/banner/20190416130753_5cb57139ab345.jpg',	'Seruput Massal Bareng MoneySmart Indonesia',	'Rencana buka bisnis masih wacana? Atau...yang udah punya bisnis, revenue nya masih segitu-gitu aja? Yang udah kerja kantoran, apa iya gaji bulanan masih kurang?\r\n\r\n',	'2021-11-19 16:43:46',	'2021-11-19 16:43:46'),
(2,	'https://s3-ap-southeast-1.amazonaws.com/loket-production-sg/images/banner/20211115195051_619257ab5f897.jpg',	'Datang ke YES! 2017, Yuk! Event Ini Cocok Buat Calon Pengusaha Muda!',	'Setelah tiga tahun berturut-turut, Ikatan Keluarga Mahasiswa Manajemen Fakultas Ekonomika dan Bisnis Universitas Gadjah Mada (IKAMMA FEB UGM) kembali menyelenggarakan acara bertajuk Young Entrepreneurs Show (YES!) di tahun 2017 ini.',	'2021-11-19 16:43:46',	'2021-11-19 16:43:46');

DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` longtext,
  `user_id` int(11) NOT NULL,
  `banner_event` json NOT NULL,
  `nama_event` varchar(255) NOT NULL,
  `penyelenggara` varchar(255) NOT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `lokasi` text NOT NULL,
  `jenis_tiket` tinyint(4) NOT NULL COMMENT '1: tiket berbayar 2: tiket gratis',
  `nama_tiket` varchar(255) NOT NULL,
  `harga_tiket` float NOT NULL,
  `jumlah_tiket` int(11) NOT NULL,
  `sisa_tiket` int(11) DEFAULT '0',
  `deskripsi_tiket` longtext NOT NULL,
  `deskripsi_event` longtext NOT NULL,
  `syarat_ketentuan` longtext NOT NULL,
  `atur_data_pembeli` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `events_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `events` (`id`, `url`, `user_id`, `banner_event`, `nama_event`, `penyelenggara`, `kategori_id`, `date`, `time`, `lokasi`, `jenis_tiket`, `nama_tiket`, `harga_tiket`, `jumlah_tiket`, `sisa_tiket`, `deskripsi_tiket`, `deskripsi_event`, `syarat_ketentuan`, `atur_data_pembeli`, `created_at`, `updated_at`) VALUES
(23,	'EventMusicEvent1cf617fc-2c0f-49ae-9102-537382a767d6',	21,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670597189/fmgfuic3xxcyh3l3zadl.webp\", \"etag\": \"7727ca3d216131b6abd83afb13950254\", \"tags\": [], \"type\": \"upload\", \"bytes\": 30708, \"pages\": 1, \"width\": 600, \"folder\": \"\", \"format\": \"webp\", \"height\": 320, \"api_key\": \"235439449818346\", \"version\": 1670597189, \"asset_id\": \"aec333f03fa087eaf89d60eb25c06821\", \"public_id\": \"fmgfuic3xxcyh3l3zadl\", \"signature\": \"550022d16a9a6ea54e3fa9b3886348671c1c297a\", \"created_at\": \"2022-12-09T14:46:29Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670597189/fmgfuic3xxcyh3l3zadl.webp\", \"version_id\": \"44ae66282bb29d42bb1f3b230b3f14a0\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"phpC07\", \"original_extension\": \"tmp\"}',	'Event Music Event',	'organizer 1',	2,	'2022-12-31',	'21:46:00',	'Jl. Panjang',	1,	'Tiket Masuk',	250000,	100,	99,	'must follow the seats that have been provided by the organizer\r\n\r\nwajib mengikuti tempat duduk yang telah disediakan oleh penyelenggara\r\n\r\n \r\n\r\nTicket verification is carried out at the venue with the following schedule  16:30 - 18:45 WIB\r\n\r\nVerifikasi tiket dilakukan di venue dengan jadwal 16:30 – 18:45 WIB\r\n\r\n \r\n\r\nWe are NOT responsible for the lost of this e-voucher\r\n\r\nKami tidak bertanggung jawab atas kehilangan e-voucher ini\r\n\r\n \r\n\r\nNO WEAPON & NO DRUGS\r\n\r\nDILARANG MEMBAWA SENJATA ATAU OBAT-OBATAN\r\n\r\n',	'PHA+PHN0cm9uZz5OZXcgWWVhcidzIEV2ZSBDZWxlYnJhdGlvbjogInJlU09VTHV0aW9uIjwvc3Ryb25nPjwvcD48cD5DZWxlYnJhdGUgdGhlIHllYXIgdGhhdCBoYXMgYmVlbiBhbmQgc3RlcCBwb3dlcmZ1bGx5IGludG8gMjAyMywgbGV04oCZcyByaW5nIGluIHRoZSBuZXcgeWVhciB0b2dldGhlciE8L3A+PHA+V2UgcHJvdWRseSBwcmVzZW50ICJyZVNPVUx1dGlvbiIsIGEgd2VsbG5lc3MtdGhlbWVkIE5ZRSBjZWxlYnJhdGlvbiBmZWF0dXJpbmcgb25lIHN0b3AgZW50ZXJ0YWlubWVudCBhbmQgd2VsbG5lc3MgZmVzdGl2YWwuIEVuam95IHNwZWNpYWwgcGVyZm9ybWFuY2VzIGJ5IEluZG9uZXNpYSByZW5vd25lZCBzaW5nZXIgVFVMVVMsIHN0YW5kLXVwIGNvbWVkaWFuIERPRElUIE1VTFlBTlRPLCBtZW50YWxpc3QgUk9NWSBSQUZBRUwgYW5kIFVSWSBSQUZBRUwgYXQgSklFWFBPIENvbnZlbnRpb24gQ2VudGVyICZhbXA7IFRoZWF0cmUsIG9uZSBvZiB0aGUgZ3JhbmRlc3QgdGhlYXRyZSBpbiBJbmRvbmVzaWEgJmFtcDsgQXNpYSBQYWNpZmljLjwvcD48cD5Kb2luIHVzIGZvciBhIG5pZ2h0IG9mIGxpdmUgbXVzaWMgJmFtcDsgZW50ZXJ0YWlubWVudCwgZ2FtZXMmbmJzcDsgJmFtcDsgZG9vcnByaXplcywgYWxsIHlvdSBjYW4gZWF0IGRpbm5lcisgZnJlZSBmbG93IGRyaW5rcywgYW5kJm5ic3A7IGV4cGVyaWVuY2luZyB3ZWxsbmVzcyBmZXN0aXZhbCB0aHJvdWdoIGEgcGxlbnR5IG9mIGZ1biAmYW1wOyBoZWFsaW5nIGFjdGl2aXRpZXMgYXMgd2VsbCBhcyBwb3AtdXAgZXhoaWJpdGlvbiBmcm9tIHdlbGwta25vd24gYnJhbmRzIGluIEluZG9uZXNpYS48L3A+PHA+PGJyPiZuYnNwOzwvcD4=',	'PHA+WWVhciBFbmQgUGFydHkgd2l0aCBBbCBNY2theXMgRWFydGgsIFdpbmQgJmFtcDsgRmlyZSBFeHBlcmllbmNlPC9wPjxwPkhhcmdhIHRpa2V0IHN1ZGFoIHRlcm1hc3VrIHBhamFrIGRhbiBhZG1pbiBmZWU8L3A+PHA+VGlrZXQgeWFuZyBzdWRhaCBkaWJlbGkgdGlkYWsgZGFwYXQgZGl0dWthci8gZGlrZW1iYWxpa2FuPC9wPjxwPkUtdGlrZXQgYWthbiBkaWtpcmlta2FuIG1lbGFsdWkmbmJzcDsgZW1haWwmbmJzcDsgc2V0ZWxhaCBtZWxha3VrYW4gcGVtYmF5YXJhbiBwZXNhbmFuLjwvcD4=',	'[\"nama_lengkap\", \"email\", \"no_handphone\", \"no_identitas\"]',	'2022-12-09 14:46:31',	'2022-12-09 14:55:45'),
(24,	'EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb',	20,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670597240/u2qnij6mvnjkxptwaite.jpg\", \"etag\": \"240e6336c8a2ad7ee3c5674d64b1f0fb\", \"tags\": [], \"type\": \"upload\", \"bytes\": 147681, \"width\": 1000, \"folder\": \"\", \"format\": \"jpg\", \"height\": 780, \"api_key\": \"235439449818346\", \"version\": 1670597240, \"asset_id\": \"3053bcfa7415cd277e12991cd0b2a6fc\", \"public_id\": \"u2qnij6mvnjkxptwaite\", \"signature\": \"f6e535d400a159e99737257e7e533c8ab6a49122\", \"created_at\": \"2022-12-09T14:47:20Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670597240/u2qnij6mvnjkxptwaite.jpg\", \"version_id\": \"b184b0a90f2d593f95c2385755850336\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"phpD004\", \"original_extension\": \"tmp\"}',	'Event Sport Bike',	'organizer 2',	19,	'2022-12-30',	'12:00:00',	'Jl. Panjang',	1,	'Tiket Sport 1',	100000,	100,	95,	'Year End Party with Al Mckays Earth, Wind & Fire Experience\r\n\r\nHarga tiket sudah termasuk pajak dan admin fee\r\n\r\nTiket yang sudah dibeli tidak dapat ditukar/ dikembalikan\r\n\r\nE-tiket akan dikirimkan melalui  email  setelah melakukan pembayaran pesanan.',	'PHA+PHN0cm9uZz5OZXcgWWVhcidzIEV2ZSBDZWxlYnJhdGlvbjogInJlU09VTHV0aW9uIjwvc3Ryb25nPjwvcD48cD5DZWxlYnJhdGUgdGhlIHllYXIgdGhhdCBoYXMgYmVlbiBhbmQgc3RlcCBwb3dlcmZ1bGx5IGludG8gMjAyMywgbGV04oCZcyByaW5nIGluIHRoZSBuZXcgeWVhciB0b2dldGhlciE8L3A+PHA+V2UgcHJvdWRseSBwcmVzZW50ICJyZVNPVUx1dGlvbiIsIGEgd2VsbG5lc3MtdGhlbWVkIE5ZRSBjZWxlYnJhdGlvbiBmZWF0dXJpbmcgb25lIHN0b3AgZW50ZXJ0YWlubWVudCBhbmQgd2VsbG5lc3MgZmVzdGl2YWwuIEVuam95IHNwZWNpYWwgcGVyZm9ybWFuY2VzIGJ5IEluZG9uZXNpYSByZW5vd25lZCBzaW5nZXIgVFVMVVMsIHN0YW5kLXVwIGNvbWVkaWFuIERPRElUIE1VTFlBTlRPLCBtZW50YWxpc3QgUk9NWSBSQUZBRUwgYW5kIFVSWSBSQUZBRUwgYXQgSklFWFBPIENvbnZlbnRpb24gQ2VudGVyICZhbXA7IFRoZWF0cmUsIG9uZSBvZiB0aGUgZ3JhbmRlc3QgdGhlYXRyZSBpbiBJbmRvbmVzaWEgJmFtcDsgQXNpYSBQYWNpZmljLjwvcD48cD5Kb2luIHVzIGZvciBhIG5pZ2h0IG9mIGxpdmUgbXVzaWMgJmFtcDsgZW50ZXJ0YWlubWVudCwgZ2FtZXMmbmJzcDsgJmFtcDsgZG9vcnByaXplcywgYWxsIHlvdSBjYW4gZWF0IGRpbm5lcisgZnJlZSBmbG93IGRyaW5rcywgYW5kJm5ic3A7IGV4cGVyaWVuY2luZyB3ZWxsbmVzcyBmZXN0aXZhbCB0aHJvdWdoIGEgcGxlbnR5IG9mIGZ1biAmYW1wOyBoZWFsaW5nIGFjdGl2aXRpZXMgYXMgd2VsbCBhcyBwb3AtdXAgZXhoaWJpdGlvbiBmcm9tIHdlbGwta25vd24gYnJhbmRzIGluIEluZG9uZXNpYS48L3A+',	'PHA+WWVhciBFbmQgUGFydHkgd2l0aCBBbCBNY2theXMgRWFydGgsIFdpbmQgJmFtcDsgRmlyZSBFeHBlcmllbmNlPC9wPjxwPkhhcmdhIHRpa2V0IHN1ZGFoIHRlcm1hc3VrIHBhamFrIGRhbiBhZG1pbiBmZWU8L3A+PHA+VGlrZXQgeWFuZyBzdWRhaCBkaWJlbGkgdGlkYWsgZGFwYXQgZGl0dWthci8gZGlrZW1iYWxpa2FuPC9wPjxwPkUtdGlrZXQgYWthbiBkaWtpcmlta2FuIG1lbGFsdWkmbmJzcDsgZW1haWwmbmJzcDsgc2V0ZWxhaCBtZWxha3VrYW4gcGVtYmF5YXJhbiBwZXNhbmFuLjwvcD4=',	'[\"nama_lengkap\", \"email\", \"no_handphone\"]',	'2022-12-09 14:47:21',	'2022-12-10 01:38:52'),
(25,	'LatihanMenembakBersamaJendralcda9e98f-018c-42e9-b35b-bef24750f9b5',	21,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670597551/atxdx4wn3rkvu6gsepma.jpg\", \"etag\": \"0c9a52ddc73934595704262309c538e5\", \"tags\": [], \"type\": \"upload\", \"bytes\": 68804, \"width\": 700, \"folder\": \"\", \"format\": \"jpg\", \"height\": 343, \"api_key\": \"235439449818346\", \"version\": 1670597551, \"asset_id\": \"ee687f125e42f59969c6a99bb8e5e3da\", \"public_id\": \"atxdx4wn3rkvu6gsepma\", \"signature\": \"3a0feaf246a364cace2efac026f67c8bab6df4d8\", \"created_at\": \"2022-12-09T14:52:31Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670597551/atxdx4wn3rkvu6gsepma.jpg\", \"version_id\": \"5b7500782492c3fc2e797a86cd98e9f6\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"php8DE3\", \"original_extension\": \"tmp\"}',	'Latihan Menembak Bersama Jendral ',	'organizer 1',	16,	'2022-12-30',	'00:00:00',	'Jl. Panjang',	1,	'Tiket Exclusive',	1000000,	100,	100,	'Setiap sesi pelatihan akan menugaskan kamu untuk mengalahkan monster, mereka akan menjatuhkan kotak dalam tiga tingkat kelangkaan setelah mereka ditaklukkan.\r\n\r\nSemakin tinggi tingkat kelangkaannya, semakin tinggi kelangkaan hadiah itemnya. Eits, tapi nggak sembarang orang bisa menjadi murid Saitama, lho.',	'PHA+VGhpcyBpcyBzb21lIHNhbXBsZSBjb250ZW50LjwvcD4=',	'PHA+VGhpcyBpcyBzb21lIHNhbXBsZSBjb250ZW50LjwvcD4=',	'[\"nama_lengkap\", \"email\", \"no_handphone\", \"no_identitas\", \"tgl_lahir\", \"jenis_kelamin\"]',	'2022-12-09 14:52:32',	'2022-12-09 14:52:32'),
(26,	'EventMusicEvent1cf617fc-2c0f-49ae-9102-537382a767d61',	21,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670597189/fmgfuic3xxcyh3l3zadl.webp\", \"etag\": \"7727ca3d216131b6abd83afb13950254\", \"tags\": [], \"type\": \"upload\", \"bytes\": 30708, \"pages\": 1, \"width\": 600, \"folder\": \"\", \"format\": \"webp\", \"height\": 320, \"api_key\": \"235439449818346\", \"version\": 1670597189, \"asset_id\": \"aec333f03fa087eaf89d60eb25c06821\", \"public_id\": \"fmgfuic3xxcyh3l3zadl\", \"signature\": \"550022d16a9a6ea54e3fa9b3886348671c1c297a\", \"created_at\": \"2022-12-09T14:46:29Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670597189/fmgfuic3xxcyh3l3zadl.webp\", \"version_id\": \"44ae66282bb29d42bb1f3b230b3f14a0\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"phpC07\", \"original_extension\": \"tmp\"}',	'Event Music Event',	'organizer 1',	2,	'2022-12-31',	'21:46:00',	'Jl. Panjang',	1,	'Tiket Masuk',	250000,	100,	100,	'must follow the seats that have been provided by the organizer\r\n\r\nwajib mengikuti tempat duduk yang telah disediakan oleh penyelenggara\r\n\r\n \r\n\r\nTicket verification is carried out at the venue with the following schedule  16:30 - 18:45 WIB\r\n\r\nVerifikasi tiket dilakukan di venue dengan jadwal 16:30 – 18:45 WIB\r\n\r\n \r\n\r\nWe are NOT responsible for the lost of this e-voucher\r\n\r\nKami tidak bertanggung jawab atas kehilangan e-voucher ini\r\n\r\n \r\n\r\nNO WEAPON & NO DRUGS\r\n\r\nDILARANG MEMBAWA SENJATA ATAU OBAT-OBATAN\r\n\r\n',	'PHA+PHN0cm9uZz5OZXcgWWVhcidzIEV2ZSBDZWxlYnJhdGlvbjogInJlU09VTHV0aW9uIjwvc3Ryb25nPjwvcD48cD5DZWxlYnJhdGUgdGhlIHllYXIgdGhhdCBoYXMgYmVlbiBhbmQgc3RlcCBwb3dlcmZ1bGx5IGludG8gMjAyMywgbGV04oCZcyByaW5nIGluIHRoZSBuZXcgeWVhciB0b2dldGhlciE8L3A+PHA+V2UgcHJvdWRseSBwcmVzZW50ICJyZVNPVUx1dGlvbiIsIGEgd2VsbG5lc3MtdGhlbWVkIE5ZRSBjZWxlYnJhdGlvbiBmZWF0dXJpbmcgb25lIHN0b3AgZW50ZXJ0YWlubWVudCBhbmQgd2VsbG5lc3MgZmVzdGl2YWwuIEVuam95IHNwZWNpYWwgcGVyZm9ybWFuY2VzIGJ5IEluZG9uZXNpYSByZW5vd25lZCBzaW5nZXIgVFVMVVMsIHN0YW5kLXVwIGNvbWVkaWFuIERPRElUIE1VTFlBTlRPLCBtZW50YWxpc3QgUk9NWSBSQUZBRUwgYW5kIFVSWSBSQUZBRUwgYXQgSklFWFBPIENvbnZlbnRpb24gQ2VudGVyICZhbXA7IFRoZWF0cmUsIG9uZSBvZiB0aGUgZ3JhbmRlc3QgdGhlYXRyZSBpbiBJbmRvbmVzaWEgJmFtcDsgQXNpYSBQYWNpZmljLjwvcD48cD5Kb2luIHVzIGZvciBhIG5pZ2h0IG9mIGxpdmUgbXVzaWMgJmFtcDsgZW50ZXJ0YWlubWVudCwgZ2FtZXMmbmJzcDsgJmFtcDsgZG9vcnByaXplcywgYWxsIHlvdSBjYW4gZWF0IGRpbm5lcisgZnJlZSBmbG93IGRyaW5rcywgYW5kJm5ic3A7IGV4cGVyaWVuY2luZyB3ZWxsbmVzcyBmZXN0aXZhbCB0aHJvdWdoIGEgcGxlbnR5IG9mIGZ1biAmYW1wOyBoZWFsaW5nIGFjdGl2aXRpZXMgYXMgd2VsbCBhcyBwb3AtdXAgZXhoaWJpdGlvbiBmcm9tIHdlbGwta25vd24gYnJhbmRzIGluIEluZG9uZXNpYS48L3A+PHA+PGJyPiZuYnNwOzwvcD4=',	'PHA+WWVhciBFbmQgUGFydHkgd2l0aCBBbCBNY2theXMgRWFydGgsIFdpbmQgJmFtcDsgRmlyZSBFeHBlcmllbmNlPC9wPjxwPkhhcmdhIHRpa2V0IHN1ZGFoIHRlcm1hc3VrIHBhamFrIGRhbiBhZG1pbiBmZWU8L3A+PHA+VGlrZXQgeWFuZyBzdWRhaCBkaWJlbGkgdGlkYWsgZGFwYXQgZGl0dWthci8gZGlrZW1iYWxpa2FuPC9wPjxwPkUtdGlrZXQgYWthbiBkaWtpcmlta2FuIG1lbGFsdWkmbmJzcDsgZW1haWwmbmJzcDsgc2V0ZWxhaCBtZWxha3VrYW4gcGVtYmF5YXJhbiBwZXNhbmFuLjwvcD4=',	'[\"nama_lengkap\", \"email\", \"no_handphone\", \"no_identitas\"]',	'2022-12-09 14:46:31',	'2022-12-09 14:46:31'),
(27,	'EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb1',	20,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670597240/u2qnij6mvnjkxptwaite.jpg\", \"etag\": \"240e6336c8a2ad7ee3c5674d64b1f0fb\", \"tags\": [], \"type\": \"upload\", \"bytes\": 147681, \"width\": 1000, \"folder\": \"\", \"format\": \"jpg\", \"height\": 780, \"api_key\": \"235439449818346\", \"version\": 1670597240, \"asset_id\": \"3053bcfa7415cd277e12991cd0b2a6fc\", \"public_id\": \"u2qnij6mvnjkxptwaite\", \"signature\": \"f6e535d400a159e99737257e7e533c8ab6a49122\", \"created_at\": \"2022-12-09T14:47:20Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670597240/u2qnij6mvnjkxptwaite.jpg\", \"version_id\": \"b184b0a90f2d593f95c2385755850336\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"phpD004\", \"original_extension\": \"tmp\"}',	'Event Sport Bike',	'organizer 2',	19,	'2022-12-30',	'12:00:00',	'Jl. Panjang',	1,	'Tiket Sport 1',	100000,	100,	100,	'Year End Party with Al Mckays Earth, Wind & Fire Experience\r\n\r\nHarga tiket sudah termasuk pajak dan admin fee\r\n\r\nTiket yang sudah dibeli tidak dapat ditukar/ dikembalikan\r\n\r\nE-tiket akan dikirimkan melalui  email  setelah melakukan pembayaran pesanan.',	'PHA+PHN0cm9uZz5OZXcgWWVhcidzIEV2ZSBDZWxlYnJhdGlvbjogInJlU09VTHV0aW9uIjwvc3Ryb25nPjwvcD48cD5DZWxlYnJhdGUgdGhlIHllYXIgdGhhdCBoYXMgYmVlbiBhbmQgc3RlcCBwb3dlcmZ1bGx5IGludG8gMjAyMywgbGV04oCZcyByaW5nIGluIHRoZSBuZXcgeWVhciB0b2dldGhlciE8L3A+PHA+V2UgcHJvdWRseSBwcmVzZW50ICJyZVNPVUx1dGlvbiIsIGEgd2VsbG5lc3MtdGhlbWVkIE5ZRSBjZWxlYnJhdGlvbiBmZWF0dXJpbmcgb25lIHN0b3AgZW50ZXJ0YWlubWVudCBhbmQgd2VsbG5lc3MgZmVzdGl2YWwuIEVuam95IHNwZWNpYWwgcGVyZm9ybWFuY2VzIGJ5IEluZG9uZXNpYSByZW5vd25lZCBzaW5nZXIgVFVMVVMsIHN0YW5kLXVwIGNvbWVkaWFuIERPRElUIE1VTFlBTlRPLCBtZW50YWxpc3QgUk9NWSBSQUZBRUwgYW5kIFVSWSBSQUZBRUwgYXQgSklFWFBPIENvbnZlbnRpb24gQ2VudGVyICZhbXA7IFRoZWF0cmUsIG9uZSBvZiB0aGUgZ3JhbmRlc3QgdGhlYXRyZSBpbiBJbmRvbmVzaWEgJmFtcDsgQXNpYSBQYWNpZmljLjwvcD48cD5Kb2luIHVzIGZvciBhIG5pZ2h0IG9mIGxpdmUgbXVzaWMgJmFtcDsgZW50ZXJ0YWlubWVudCwgZ2FtZXMmbmJzcDsgJmFtcDsgZG9vcnByaXplcywgYWxsIHlvdSBjYW4gZWF0IGRpbm5lcisgZnJlZSBmbG93IGRyaW5rcywgYW5kJm5ic3A7IGV4cGVyaWVuY2luZyB3ZWxsbmVzcyBmZXN0aXZhbCB0aHJvdWdoIGEgcGxlbnR5IG9mIGZ1biAmYW1wOyBoZWFsaW5nIGFjdGl2aXRpZXMgYXMgd2VsbCBhcyBwb3AtdXAgZXhoaWJpdGlvbiBmcm9tIHdlbGwta25vd24gYnJhbmRzIGluIEluZG9uZXNpYS48L3A+',	'PHA+WWVhciBFbmQgUGFydHkgd2l0aCBBbCBNY2theXMgRWFydGgsIFdpbmQgJmFtcDsgRmlyZSBFeHBlcmllbmNlPC9wPjxwPkhhcmdhIHRpa2V0IHN1ZGFoIHRlcm1hc3VrIHBhamFrIGRhbiBhZG1pbiBmZWU8L3A+PHA+VGlrZXQgeWFuZyBzdWRhaCBkaWJlbGkgdGlkYWsgZGFwYXQgZGl0dWthci8gZGlrZW1iYWxpa2FuPC9wPjxwPkUtdGlrZXQgYWthbiBkaWtpcmlta2FuIG1lbGFsdWkmbmJzcDsgZW1haWwmbmJzcDsgc2V0ZWxhaCBtZWxha3VrYW4gcGVtYmF5YXJhbiBwZXNhbmFuLjwvcD4=',	'[\"nama_lengkap\", \"email\", \"no_handphone\"]',	'2022-12-09 14:47:21',	'2022-12-09 14:47:21'),
(28,	'LatihanMenembakBersamaJendralcda9e98f-018c-42e9-b35b-bef24750f9b51',	21,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670597551/atxdx4wn3rkvu6gsepma.jpg\", \"etag\": \"0c9a52ddc73934595704262309c538e5\", \"tags\": [], \"type\": \"upload\", \"bytes\": 68804, \"width\": 700, \"folder\": \"\", \"format\": \"jpg\", \"height\": 343, \"api_key\": \"235439449818346\", \"version\": 1670597551, \"asset_id\": \"ee687f125e42f59969c6a99bb8e5e3da\", \"public_id\": \"atxdx4wn3rkvu6gsepma\", \"signature\": \"3a0feaf246a364cace2efac026f67c8bab6df4d8\", \"created_at\": \"2022-12-09T14:52:31Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670597551/atxdx4wn3rkvu6gsepma.jpg\", \"version_id\": \"5b7500782492c3fc2e797a86cd98e9f6\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"php8DE3\", \"original_extension\": \"tmp\"}',	'Latihan Menembak Bersama Jendral ',	'organizer 1',	16,	'2022-12-30',	'00:00:00',	'Jl. Panjang',	1,	'Tiket Exclusive',	1000000,	100,	99,	'Setiap sesi pelatihan akan menugaskan kamu untuk mengalahkan monster, mereka akan menjatuhkan kotak dalam tiga tingkat kelangkaan setelah mereka ditaklukkan.\r\n\r\nSemakin tinggi tingkat kelangkaannya, semakin tinggi kelangkaan hadiah itemnya. Eits, tapi nggak sembarang orang bisa menjadi murid Saitama, lho.',	'PHA+VGhpcyBpcyBzb21lIHNhbXBsZSBjb250ZW50LjwvcD4=',	'PHA+VGhpcyBpcyBzb21lIHNhbXBsZSBjb250ZW50LjwvcD4=',	'[\"nama_lengkap\", \"email\", \"no_handphone\", \"no_identitas\", \"tgl_lahir\", \"jenis_kelamin\"]',	'2022-12-09 14:52:32',	'2022-12-09 15:43:02'),
(29,	'Test143439907-84df-4241-8d8e-150bf1a7fed6',	22,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670636532/wzj9ninxgq6rgz6ist7w.webp\", \"etag\": \"27de10308d014c30b8af657b279af1f2\", \"tags\": [], \"type\": \"upload\", \"bytes\": 40820, \"pages\": 1, \"width\": 900, \"folder\": \"\", \"format\": \"webp\", \"height\": 1015, \"api_key\": \"235439449818346\", \"version\": 1670636532, \"asset_id\": \"e8b9b31d4d13e294c91ea5589dfbd387\", \"public_id\": \"wzj9ninxgq6rgz6ist7w\", \"signature\": \"c3d88f9ffcbeb647fcba0d77c61c607c5c1c5b70\", \"created_at\": \"2022-12-10T01:42:12Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670636532/wzj9ninxgq6rgz6ist7w.webp\", \"version_id\": \"7fbf30767f84250f9fad356377cb7010\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"php5D6B\", \"original_extension\": \"tmp\"}',	'Test 1',	'Vladimir Makarov',	1,	'2022-12-30',	'08:41:00',	'Jl. Panjang',	1,	'Tiket 1',	10000,	100,	100,	'Test desc',	'PHA+PHN0cm9uZz5UaGlzIGlzIHNvbWUgc2FtcGxlIGNvbnRlbnQuPC9zdHJvbmc+PC9wPjxwPiZuYnNwOzwvcD4=',	'PHA+VGhpcyBpcyBzb21lIHNhbXBsZSBjb250ZW50LjwvcD4=',	'[\"nama_lengkap\", \"email\", \"no_handphone\"]',	'2022-12-10 01:42:11',	'2022-12-10 01:42:11');

DROP TABLE IF EXISTS `ms_kategori`;
CREATE TABLE `ms_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `ms_kategori` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1,	'Festival Fair Bazaar',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(2,	'Konser',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(3,	'Pertandingan',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(4,	'Exhibition Expo Pameran',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(6,	'Konferensi',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(7,	'Workshop',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(8,	'Pertunjukan',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(9,	'Atraksi dan Theme Park',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(13,	'Akomodasi',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(14,	'Seminar Talk Show',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(15,	'Social Gathering',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(16,	'Training Sertifikasi dan Ujian',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(17,	'Pensi Event Sekolah Kampus',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(18,	'Trip dan Tur',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(19,	'Turnamen Kompetisi',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44'),
(20,	'Lainnya',	'2021-10-20 07:01:44',	'2021-10-20 07:01:44');

DROP TABLE IF EXISTS `ms_pembeli`;
CREATE TABLE `ms_pembeli` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `attributes` text,
  `event_listener` text,
  `value` text,
  `options` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `ms_pembeli` (`id`, `label`, `name`, `type`, `attributes`, `event_listener`, `value`, `options`, `created_at`, `updated_at`) VALUES
(1,	'Nama Lengkap',	'nama_lengkap',	'text',	'checked=\"checked\"',	'onclick=\"return false;\"',	NULL,	NULL,	'2021-11-11 03:11:36',	'2021-11-11 03:11:36'),
(2,	'Email',	'email',	'email',	'checked=\"checked\"',	'onclick=\"return false;\"',	NULL,	NULL,	'2021-11-11 03:11:36',	'2021-11-11 03:11:36'),
(3,	'No. Handphone',	'no_handphone',	'number',	'checked=\"checked\"',	'onclick=\"return false;\"',	NULL,	NULL,	'2021-11-11 03:11:36',	'2021-11-11 03:11:36'),
(4,	'No. Identitas (KTP, SIM, Paspor, dll)',	'no_identitas',	'text',	NULL,	NULL,	NULL,	NULL,	'2021-11-11 03:11:36',	'2021-11-11 03:11:36'),
(5,	'Tanggal Lahir',	'tgl_lahir',	'date',	NULL,	NULL,	NULL,	NULL,	'2021-11-11 03:11:36',	'2021-11-11 03:11:36'),
(6,	'Jenis Kelamin',	'jenis_kelamin',	'select',	NULL,	NULL,	NULL,	'[{\"text\": \"Laki-laki\", \"value\": \"Laki-laki\"}, {\"text\": \"Perempuan\", \"value\": \"Perempuan\"}]',	'2021-11-11 03:11:36',	'2021-11-11 03:11:36');

DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `banner_image` text,
  `profile_image` json DEFAULT NULL,
  `nama_organizer` text NOT NULL,
  `no_handphone` varchar(255) NOT NULL,
  `alamat` longtext,
  `tentang_kami` longtext,
  `tags` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `profile` (`id`, `user_id`, `banner_image`, `profile_image`, `nama_organizer`, `no_handphone`, `alamat`, `tentang_kami`, `tags`, `created_at`, `updated_at`) VALUES
(8,	21,	NULL,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670596966/tdtn491lqmnpi437niyq.png\", \"etag\": \"329049962eddfd8e6a9f0d836d994919\", \"tags\": [], \"type\": \"upload\", \"bytes\": 4273, \"width\": 100, \"folder\": \"\", \"format\": \"png\", \"height\": 100, \"api_key\": \"235439449818346\", \"version\": 1670596966, \"asset_id\": \"6d9bc1113ad46ddce4f4a43fe66a3732\", \"public_id\": \"tdtn491lqmnpi437niyq\", \"signature\": \"464b9f52d834190895569614640ed06839b36816\", \"created_at\": \"2022-12-09T14:42:46Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670596966/tdtn491lqmnpi437niyq.png\", \"version_id\": \"eb5d369c7bf6ad61133e894f9a67429c\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"phpA078\", \"original_extension\": \"tmp\"}',	'organizer 1',	'08123123',	'Jl. Panjang',	'Hello World',	NULL,	'2022-12-09 14:42:47',	'2022-12-09 14:42:47'),
(9,	20,	NULL,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670596992/g2mqnzkhqfmtygzpgphf.png\", \"etag\": \"1a1c9247cc8e3eaaceea51ed6b4c2e3a\", \"tags\": [], \"type\": \"upload\", \"bytes\": 6308, \"width\": 100, \"folder\": \"\", \"format\": \"png\", \"height\": 100, \"api_key\": \"235439449818346\", \"version\": 1670596992, \"asset_id\": \"023c8ebad0df9ce730ffce02ce64529d\", \"public_id\": \"g2mqnzkhqfmtygzpgphf\", \"signature\": \"f3bb56a46006bdf54961612661c9c1c281490e23\", \"created_at\": \"2022-12-09T14:43:12Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670596992/g2mqnzkhqfmtygzpgphf.png\", \"version_id\": \"bad3c398dc26ca5bd3bb583e1d8b406a\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"php907\", \"original_extension\": \"tmp\"}',	'organizer 2',	'08112233',	'Jl. Panjang',	'Hello World',	NULL,	'2022-12-09 14:43:13',	'2022-12-09 14:43:13'),
(10,	16,	NULL,	'null',	'Admin',	'08123',	'Admin',	'admin',	NULL,	'2022-12-09 14:50:00',	'2022-12-09 14:50:00'),
(11,	22,	NULL,	'{\"url\": \"http://res.cloudinary.com/dwqwwil7k/image/upload/v1670597883/vdw4wcuvwvvsj3kdiumg.png\", \"etag\": \"98a5e59bbdf1b343e29edcc2e570ce5e\", \"tags\": [], \"type\": \"upload\", \"bytes\": 5546, \"width\": 100, \"folder\": \"\", \"format\": \"png\", \"height\": 100, \"api_key\": \"235439449818346\", \"version\": 1670597883, \"asset_id\": \"5ad4a9c3405b9c6805e23acda1d6ad7f\", \"public_id\": \"vdw4wcuvwvvsj3kdiumg\", \"signature\": \"fd5afe0500ff4c542206413c619f8335a56bb972\", \"created_at\": \"2022-12-09T14:58:03Z\", \"secure_url\": \"https://res.cloudinary.com/dwqwwil7k/image/upload/v1670597883/vdw4wcuvwvvsj3kdiumg.png\", \"version_id\": \"bbed9ae46288e2db7f6c38174c52bb34\", \"placeholder\": false, \"resource_type\": \"image\", \"original_filename\": \"phpA4A0\", \"original_extension\": \"tmp\"}',	'Vladimir Makarov',	'08123123',	'Jl. Panjang',	'Hello World...',	NULL,	'2022-12-09 14:58:03',	'2022-12-09 14:58:03');

DROP TABLE IF EXISTS `rekening`;
CREATE TABLE `rekening` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `nama_bank` varchar(255) NOT NULL,
  `pemilik_rekening` varchar(255) NOT NULL,
  `nomor_rekening` int(11) NOT NULL,
  `kantor_cabang` varchar(255) NOT NULL,
  `kota` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `rekening_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `sub_event`;
CREATE TABLE `sub_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `jenis_tiket` tinyint(4) NOT NULL,
  `nama_tiket` varchar(255) NOT NULL,
  `harga_tiket` bigint(20) NOT NULL,
  `jumlah_tiket` int(11) NOT NULL,
  `sisa_tiket` int(11) NOT NULL,
  `deskripsi_tiket` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  CONSTRAINT `sub_event_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `temp_transaction`;
CREATE TABLE `temp_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) DEFAULT NULL,
  `token` text,
  `data` json DEFAULT NULL,
  `expired` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `temp_transaction` (`id`, `order_id`, `token`, `data`, `expired`, `created_at`, `updated_at`) VALUES
(22,	'EventMusicEvent1cf617fc-2c0f-49ae-9102-537382a767d6',	'c27d700f-44ab-4344-9739-696c3bf9cdac',	'{\"token\": \"c27d700f-44ab-4344-9739-696c3bf9cdac\", \"order_id\": \"EventMusicEvent1cf617fc-2c0f-49ae-9102-537382a767d6\", \"harga_tiket\": \"250000\", \"jumlah_tiket\": \"1\"}',	'2022-12-09 15:10:21',	'2022-12-09 14:55:21',	'2022-12-09 14:55:21'),
(23,	'EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb',	'8fd6f516-2510-41b4-91df-a77daa17e833',	'{\"token\": \"8fd6f516-2510-41b4-91df-a77daa17e833\", \"order_id\": \"EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb\", \"harga_tiket\": \"100000\", \"jumlah_tiket\": \"1\"}',	'2022-12-09 15:21:00',	'2022-12-09 15:19:37',	'2022-12-09 15:19:37'),
(24,	'LatihanMenembakBersamaJendralcda9e98f-018c-42e9-b35b-bef24750f9b51',	'9ea45a66-1455-489b-bce4-9af9d22899c5',	'{\"token\": \"9ea45a66-1455-489b-bce4-9af9d22899c5\", \"order_id\": \"LatihanMenembakBersamaJendralcda9e98f-018c-42e9-b35b-bef24750f9b51\", \"harga_tiket\": \"1000000\", \"jumlah_tiket\": \"1\"}',	'2022-12-09 15:57:46',	'2022-12-09 15:42:46',	'2022-12-09 15:42:46'),
(25,	'EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb',	'e21ee4b5-4a27-4f68-851f-fa16a86164f4',	'{\"token\": \"e21ee4b5-4a27-4f68-851f-fa16a86164f4\", \"order_id\": \"EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb\", \"harga_tiket\": \"100000\", \"jumlah_tiket\": \"1\"}',	'2022-12-09 16:01:17',	'2022-12-09 15:46:17',	'2022-12-09 15:46:17'),
(26,	'EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb',	'2c3c4791-2c99-4691-bc25-d6fff8326c92',	'{\"token\": \"2c3c4791-2c99-4691-bc25-d6fff8326c92\", \"order_id\": \"EventSportBikec2b667f5-6955-406f-9319-d8b1d10ab5bb\", \"harga_tiket\": \"100000\", \"jumlah_tiket\": \"1\"}',	'2022-12-10 01:53:09',	'2022-12-10 01:38:09',	'2022-12-10 01:38:09');

DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `transaction_detail_id` int(11) NOT NULL,
  `kode_tiket` char(8) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `event_id` (`event_id`),
  KEY `transaction_detail_id` (`transaction_detail_id`),
  CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`transaction_detail_id`) REFERENCES `transaction_detail` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `ticket` (`id`, `event_id`, `transaction_detail_id`, `kode_tiket`, `created_at`, `updated_at`) VALUES
(21,	23,	34,	'IHJLT406',	'2022-12-09 14:56:14',	'2022-12-09 14:56:14'),
(22,	24,	35,	'WVDSF846',	'2022-12-09 15:06:40',	'2022-12-09 15:06:40'),
(23,	24,	36,	'BXLZR567',	'2022-12-09 15:06:46',	'2022-12-09 15:06:46'),
(24,	24,	37,	'KWBLW796',	'2022-12-09 15:20:26',	'2022-12-09 15:20:26'),
(25,	24,	40,	'STESI722',	'2022-12-10 01:39:45',	'2022-12-10 01:39:45');

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice` varchar(255) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0 = menunggu pembayaran 1 = sudah membayar 2 = expired',
  `email_notifikasi` varchar(255) DEFAULT NULL,
  `metode_pembayaran` varchar(255) NOT NULL,
  `jumlah_tiket` int(11) NOT NULL,
  `virtual_account` varchar(255) NOT NULL,
  `transaction_time` timestamp NULL DEFAULT NULL,
  `total` float NOT NULL,
  `fee` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `transactions` (`id`, `invoice`, `status`, `email_notifikasi`, `metode_pembayaran`, `jumlah_tiket`, `virtual_account`, `transaction_time`, `total`, `fee`, `created_at`, `updated_at`) VALUES
(30,	'INV/20221209/5275/TQRDT',	1,	'irwan_maulana42@yahoo.com',	'gopay',	1,	'775177',	'2022-12-09 14:56:10',	250000,	3000,	'2022-12-09 14:55:45',	'2022-12-09 14:56:10'),
(31,	'INV/20221209/6069/XQDUU',	1,	'irwan_maulana42@yahoo.com',	'gopay',	2,	'593753',	'2022-12-09 15:06:36',	200000,	3000,	'2022-12-09 15:06:21',	'2022-12-09 15:06:36'),
(32,	'INV/20221209/9990/YIHBN',	1,	'irwan_maulana42@yahoo.com',	'bca',	1,	'150258',	'2022-12-09 15:20:14',	100000,	3000,	'2022-12-09 15:19:58',	'2022-12-09 15:20:14'),
(33,	'INV/20221209/5417/SHAGD',	0,	'irwan_maulana42@yahoo.com',	'bank_transfer',	1,	'373001',	NULL,	1000000,	3000,	'2022-12-09 15:43:02',	'2022-12-09 15:43:02'),
(34,	'INV/20221209/6826/MSWNM',	0,	'irwan_maulana42@yahoo.com',	'gopay',	1,	'742317',	NULL,	100000,	3000,	'2022-12-09 15:46:31',	'2022-12-09 15:46:31'),
(35,	'INV/20221210/2978/OTLHO',	1,	'irwan_maulana42@yahoo.com',	'gopay',	1,	'521415',	'2022-12-10 01:39:41',	100000,	3000,	'2022-12-10 01:38:52',	'2022-12-10 01:39:41');

DROP TABLE IF EXISTS `transaction_detail`;
CREATE TABLE `transaction_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `data_pemesan` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_id` (`transaction_id`),
  KEY `event_id` (`event_id`),
  CONSTRAINT `transaction_detail_ibfk_3` FOREIGN KEY (`transaction_id`) REFERENCES `transactions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaction_detail_ibfk_4` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `transaction_detail` (`id`, `transaction_id`, `event_id`, `data_pemesan`, `created_at`, `updated_at`) VALUES
(34,	30,	23,	'{\"email\": \"irwan_maulana42@yahoo.com\", \"nama_lengkap\": \"Irwan M\", \"no_handphone\": \"08123123\", \"no_identitas\": \"0812301283\"}',	'2022-12-09 14:55:45',	'2022-12-09 14:55:45'),
(35,	31,	24,	'{\"email\": \"irwan_maulana42@yahoo.com\", \"nama_lengkap\": \"Irwan M\", \"no_handphone\": \"123123\"}',	'2022-12-09 15:06:21',	'2022-12-09 15:06:21'),
(36,	31,	24,	'{\"email\": \"irwan_maulana42@yahoo.com\", \"nama_lengkap\": \"Yaya\", \"no_handphone\": \"11001\"}',	'2022-12-09 15:06:21',	'2022-12-09 15:06:21'),
(37,	32,	24,	'{\"email\": \"irwan_maulana42@yahoo.com\", \"nama_lengkap\": \"Irwan M\", \"no_handphone\": \"123123\"}',	'2022-12-09 15:19:58',	'2022-12-09 15:19:58'),
(38,	33,	28,	'{\"email\": \"irwan_maulana42@yahoo.com\", \"tgl_lahir\": \"2022-12-10\", \"nama_lengkap\": \"IRwan\", \"no_handphone\": \"21123\", \"no_identitas\": \"123\", \"jenis_kelamin\": \"Laki-laki\"}',	'2022-12-09 15:43:02',	'2022-12-09 15:43:02'),
(39,	34,	24,	'{\"email\": \"irwan_maulana42@yahoo.com\", \"nama_lengkap\": \"irwan_maulana42@yahoo.com\", \"no_handphone\": \"123123\"}',	'2022-12-09 15:46:31',	'2022-12-09 15:46:31'),
(40,	35,	24,	'{\"email\": \"irwan_maulana42@yahoo.com\", \"nama_lengkap\": \"Irwan\", \"no_handphone\": \"08123123\"}',	'2022-12-10 01:38:52',	'2022-12-10 01:38:52');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `is_superadmin` tinyint(4) DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `email`, `password`, `is_superadmin`, `status`, `created_at`, `updated_at`) VALUES
(16,	'irwan@mail.com',	'$2y$10$u0Nu0XtjivJFGWnkpCqaSuEe/tFBYRVNJ73oODtGZHe42GD8Vw6sW',	1,	1,	'2022-09-10 03:17:14',	'2022-09-10 03:17:14'),
(20,	'irwan9764@gmail.com',	'$2y$10$tCGfV/Urp.vBJsop4kVFaOcTBqETLbySXXUtTnqfzLb7weLLCOeM2',	0,	1,	'2022-12-09 14:32:00',	'2022-12-09 14:32:00'),
(21,	'irwan_maulana248@yahoo.com',	'$2y$10$26.HTVij3HEDOxm0cvijZ.AsCi3f2BSghlLP8ef2zNOvHdp1RhWeO',	0,	1,	'2022-12-09 14:32:38',	'2022-12-09 14:32:38'),
(22,	'irwan_maulana42@yahoo.com',	'$2y$10$XmyS/C3meZf35/SiCZOjxuXc0rLmaKGbPbJ77uzMCS9GbXsS6ZKPW',	0,	1,	'2022-12-09 14:32:57',	'2022-12-09 14:32:57');

-- 2022-12-10 02:04:26
